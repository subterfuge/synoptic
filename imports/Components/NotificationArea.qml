import QtQuick
import Components as C

ListView {
    id: root
    property ListModel notificationModel: listModel
    model: notificationModel
    spacing: 2
    boundsBehavior: Flickable.StopAtBounds
    delegate: Notification {
        width: root.width
        title: model.title
        message: model.message
        type: model.type
        timeout: model.timeout
        onClosed: root.notificationModel.remove(model.index)
    }

    ListModel {
        id: listModel
    }
}
