import QtQuick
import Components as C

Item {
    id: spinBoxSlider

    property int startValue: 0
    property int value: 0
    property list<string> valueList: []
    property font font: { pixelSize: 13 }
    property bool rounded: false
    property bool readOnly: false
    property int labelWidth: 80
    property bool lightMode: false
    property alias labelVisible: valueLabel.visible
    property color minColor: Qt.hsva(0, 1, 1)
    property color maxColor: Qt.hsva(0.35, 1, 1)

    QtObject {
        // private properties
        id: internal
        property color colorValue: Qt.hsva(0, 1, 1)
    }

    implicitHeight: Math.max(minusBtn.height, sliderBtns.height, plusBtn.height, valueLabel.height)
    implicitWidth: minusBtn.width + sliderBtns.width + plusBtn.width + valueLabel.width
        + sliderBtns.spacing * btnModel.count + 2 * 3

    onValueChanged: {
        let maxValue = valueList.length - 1 + startValue
        let normalizedValue = (value - startValue) / (maxValue - startValue);
        let lerpedHue = minColor.hsvHue + normalizedValue * (maxColor.hsvHue - minColor.hsvHue)
            internal.colorValue = Qt.hsva(lerpedHue, internal.colorValue.hsvSaturation, internal.colorValue.hsvValue, 1)
        for(let i = 0; i < btnModel.count; i++)
        {
            if(btnModel.get(i).value <= value)
                btnModel.setProperty(i, "active", true)
            else
                btnModel.setProperty(i, "active", false)
        }
    }

    onReadOnlyChanged: {
        readOnly ? enabled = false : enabled = true
    }

    Rectangle {
        id: minusBtn
        width: readOnly ? 0 : 22
        height: 22
        color: "transparent"
        border { width: 1; color: "white" }
        radius: rounded ? width / 2 : 0
        visible: !readOnly

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
            onEntered: minusBtn.state = "hovered"
            onExited: minusBtn.state = ""
            onClicked: spinBoxSlider.value = Math.max(spinBoxSlider.value - 1, spinBoxSlider.startValue)
        }

        Text {
            id: minusBtnText
            anchors.fill: parent
            text: "-"
            color: "white"
            font: spinBoxSlider.font
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        states: [
            State {
                name: "hovered"
                PropertyChanges {
                    target: minusBtn
                    color: "white"
                }
                PropertyChanges {
                    target: minusBtnText
                    color: "black"
                }
            }
        ]
    }

    ListView {
        id: sliderBtns
        anchors.left: minusBtn.right
        anchors.leftMargin: readOnly ? 0 : 2
        width: btnModel.count * 17
        height: 22
        spacing: 2
        orientation: Qt.Horizontal
        model: btnModel
        delegate: Rectangle {
            id: valueBtn
            width: 15
            height: 22
            color: {
                if(spinBoxSlider.value === model.value)
                    internal.colorValue
                else
                    model.active ? Qt.darker(internal.colorValue, 1.8) : C.Palette.background
            }
            border {
                width: 1
                color: {
                    if(spinBoxSlider.lightMode)
                        C.Palette.background
                    else
                        model.active ? internal.colorValue : C.Palette.front
                }
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onEntered: if(!readOnly) model.hovered = true
                onExited: model.hovered = false
                onClicked: spinBoxSlider.value = model.value
            }

            states: [
                State {
                    name: "hovered"
                    when: hovered
                    PropertyChanges {
                        target: valueBtn
                        border.width: 2
                    }
                }
            ]
        }
    }

    Rectangle {
        id: plusBtn
        anchors.left: sliderBtns.right
        width: readOnly ? 0 : 22
        height: 22
        color: "transparent"
        border { width: 1; color: "white" }
        radius: rounded ? width / 2 : 0
        visible: !readOnly

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
            onEntered: plusBtn.state = "hovered"
            onExited: plusBtn.state = ""
            onClicked: spinBoxSlider.value = Math.min(spinBoxSlider.value + 1,
                spinBoxSlider.valueList.length - 1 + spinBoxSlider.startValue)
        }

        Text {
            id: plusBtnText
            anchors.fill: parent
            text: "+"
            color: "white"
            font: spinBoxSlider.font
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }

        states: [
            State {
                name: "hovered"
                PropertyChanges {
                    target: plusBtn
                    color: "white"
                }
                PropertyChanges {
                    target: plusBtnText
                    color: "black"
                }
            }
        ]
    }

    Rectangle {
        id: valueLabel
        anchors.left: plusBtn.right
        anchors.leftMargin: readOnly ? 0 : 2
        width: spinBoxSlider.labelWidth
        height: 22
        color: "#404040"

        Text {
            anchors.fill: parent
            text: spinBoxSlider.valueList[value - spinBoxSlider.startValue]
            color: "white"
            font: spinBoxSlider.font
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }
    }

    ListModel {
        id: btnModel
    }

    Component.onCompleted: {
        for(let i = startValue; i < spinBoxSlider.valueList.length + startValue; i++)
        {
            btnModel.append({"value": i, "active": i <= value, "hovered": false})
        }
        spinBoxSlider.value = spinBoxSlider.startValue
    }
}
