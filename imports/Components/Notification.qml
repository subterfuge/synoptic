import QtQuick
import QtQuick.Shapes
import Components as C

Item {
    id: root
    implicitWidth: 300
    implicitHeight: title.height + message.height + 5

    property int type: -1
    property color backgroundColor: C.Palette.front
    property color borderColor: C.Palette.front
    property double timeout: 0
    property alias title: title.text
    property alias message: message.text

    QtObject {
        id: internal
        property int boxHeight: root.height
    }

    signal closed

    function close() {
        closed()
        root.visible = false
    }

    function stopTimeout() {
        timeoutAnimation.stop()
        timeoutBar.width = root.width + 2
    }

    function startTimeout() {
        if(timeout > 0)
            timeoutAnimation.start()
    }

    onTypeChanged: {
        switch(type) {
        case NotificationSystem.TypeBasic:
            root.backgroundColor = C.Palette.front
            root.borderColor = C.Palette.front
            break
        case NotificationSystem.TypeImportant:
            root.backgroundColor = C.Palette.greenLight
            root.borderColor = C.Palette.greenLight
            break
        case NotificationSystem.TypeWarning:
            root.backgroundColor = C.Palette.orange
            root.borderColor = C.Palette.orange
            break
        case NotificationSystem.TypeError:
            root.backgroundColor = C.Palette.red
            root.borderColor = C.Palette.red
            break
        }
    }

    Component.onCompleted: {
        if(root.timeout > 0) {
            root.implicitHeight = title.contentHeight + message.contentHeight + 9
            internal.boxHeight = root.implicitHeight - 4
            timeoutBar.visible = true
        }
    }


    Row {
        spacing: 2
        anchors.fill: parent

        Rectangle {
            id: background
            width: root.width - 50
            height: internal.boxHeight
            color: C.Palette.background
            border.color: root.borderColor

            Rectangle {
                anchors.fill: parent
                anchors.margins: 2
                color: root.backgroundColor

                Column {
                    anchors.fill: parent
                    anchors.leftMargin: 5
                    anchors.rightMargin: 5

                    Text {
                        id: title
                        function update() {
                            title.height = title.text === "" ? 0 : title.contentHeight
                        }

                        width: parent.width
                        color: C.Palette.background
                        font: C.CustomFont.subTitleFont
                        wrapMode: Text.Wrap

                        onTextChanged: update()
                        Component.onCompleted: update()
                    }

                    Text {
                        id: message
                        function update() {
                            message.height = message.text === "" ? 0 : message.contentHeight
                        }
                        width: parent.width
                        color: C.Palette.background
                        font: C.CustomFont.smallTextFont
                        wrapMode: Text.Wrap

                        onTextChanged: update()
                        Component.onCompleted: update()
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                enabled: timeout > 0
                hoverEnabled: true
                onEntered: root.stopTimeout()
                onExited: root.startTimeout()
            }
        }

        MouseArea {
            id: closeBtn
            width: 50
            height: internal.boxHeight
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
            onEntered: {
                root.stopTimeout()
                state = "hovered"
            }
            onExited: {
                root.startTimeout()
                state = ""
            }
            onClicked: {
                root.close()
            }

            states: [
                State {
                    name: ""
                    PropertyChanges {
                        target: closeBtnBackground
                        width: 0
                        height: 0
                    }
                    PropertyChanges {
                        target: crossShape
                        color: C.Palette.front
                    }
                },

                State {
                    name: "hovered"
                    PropertyChanges {
                        target: closeBtnBackground
                        width: closeBtnBackground.parent.width - 4
                        height: closeBtnBackground.parent.height - 4
                    }
                    PropertyChanges {
                        target: crossShape
                        color: C.Palette.background
                    }
                }
            ]

            Rectangle {
                anchors.fill: parent
                color: C.Palette.background
                border.color: root.borderColor

                Rectangle {
                    id: closeBtnBackground
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: root.backgroundColor
                    width: 0
                    height: 0
                    Behavior on width {
                        NumberAnimation { duration: 50 }
                    }
                    Behavior on height {
                        NumberAnimation { duration: 50 }
                    }
                }
            }

            Shape {
                id: crossShape
                property color color: C.Palette.front
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                ShapePath {
                    startX: 0; startY: 0
                    strokeColor: crossShape.color
                    strokeWidth: 1

                    PathLine { x: 15; y: 15; }
                }

                ShapePath {
                    startX: 15; startY: 0
                    strokeColor: crossShape.color
                    strokeWidth: 1

                    PathLine { x: 0; y: 15; }
                }
            }
        }
    }

    Rectangle {
        id: timeoutBar
        visible: false
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        width: root.width + 2
        color: backgroundColor
        height: 3

        NumberAnimation on width {
            id: timeoutAnimation
            running: root.timeout > 0
            duration: root.timeout
            to: 0
            onFinished: root.close()
        }
    }
}
