pragma Singleton
import QtQml
import QtQuick

QtObject {
    readonly property color red: "#e80c30"
    readonly property color redLight: "#fc3c2b"
    readonly property color blueLight: "#00ddff"
    readonly property color green: "#00e626"
    readonly property color greenLight: "#00ffb7" //"#03fc6f"
    readonly property color yellow: "#e3fc03"
    readonly property color white: "#ffffff"
    readonly property color whiteDark: "#bcc5c5"
    readonly property color black: "#0a0a0a"
    readonly property color grey: "#43454a"
    readonly property color greyLight: "#9d9ea3"
    //readonly property color smokeGreen: "#002616"
    //readonly property color smokeGreen: "#2e3c3d"
    readonly property color smokeGreen: "#1b2a22"
    readonly property color smokeGreenLight: "#446b6b"
    readonly property color orange: "#ff6929"

    readonly property color primary: white
    readonly property color front: whiteDark
    readonly property color background: black
    readonly property color backgroundLight: grey
    readonly property color hoveredBackground: greyLight

    readonly property color highlight: smokeGreen
}
