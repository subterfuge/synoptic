pragma Singleton
import QtQuick
import Components as C

QtObject {
    enum NotificationType {
        TypeBasic,
        TypeImportant,
        TypeWarning,
        TypeError
    }

    property list<C.NotificationArea> areas

    signal areaAdded(index: int)

    function addArea(area) {
        areas.push(area)
        let index = areas.length - 1
        areaAdded(index)
        return index
    }

    function push(title, message, type = NotificationSystem.TypeBasic, timeout = 0, areaIndex = 0) {
        areas[areaIndex].notificationModel.append({"title": title, "message": message, "type": type, "timeout": timeout})
    }
}
