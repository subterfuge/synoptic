import QtQuick
import QtQuick.Shapes
import Components as C

Rectangle {
    id: closeRect
    signal clicked()
    width: 22
    height: width
    color: C.Palette.red

    MouseArea {
        width: parent.width
        height: parent.height
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true
        onClicked: closeRect.clicked()
        onEntered: parent.color = C.Palette.redLight
        onExited: parent.color = C.Palette.red
    }

    Shape {
        ShapePath {
            startX: 3; startY: closeRect.height - 12
            strokeColor: C.Palette.background
            strokeWidth: 1

            PathLine { x: closeRect.height - 10; y: closeRect.height - 3; }
        }

        ShapePath {
            startX: 3; startY: closeRect.height - 3
            strokeColor: C.Palette.background
            strokeWidth: 1

            PathLine { x: closeRect.height - 10; y: closeRect.height - 12; }
        }
    }
}