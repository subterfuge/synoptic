#include "Axis.h"

AAxis::AAxis(QObject *parent)
        : QObject{parent}, m_color(QColorConstants::White), m_labelColor(QColorConstants::White),
        m_width(1)
{}

QColor AAxis::getColor() const
{
    return m_color;
}

void AAxis::setColor(const QColor &newColor)
{
    if (m_color == newColor)
        return;

    m_color = newColor;
    emit colorChanged();
}

QColor AAxis::getLabelColor() const
{
    return m_labelColor;
}

void AAxis::setLabelColor(const QColor &newLabelColor)
{
    if (m_labelColor == newLabelColor)
        return;

    m_labelColor = newLabelColor;
    emit labelColorChanged();
}

int AAxis::getWidth() const
{
    return m_width;
}

void AAxis::setWidth(int newWidth)
{
    if(m_width == newWidth)
        return;

    m_width = newWidth;
    emit widthChanged();
}

ValueAxis::ValueAxis(QObject *parent)
    : AAxis{parent}, m_min(0), m_max(1), m_step(1), m_labelStart(0)
{}

int ValueAxis::getLabelCount()
{
    return static_cast<int>(std::floor((m_max - m_min) / m_step) + 1);
}

float ValueAxis::getMin() const
{
    return m_min;
}

void ValueAxis::setMin(float newMin)
{
    if (qFuzzyCompare(m_min, newMin))
        return;

    m_min = newMin;
    emit minChanged();
    emit labelCountChanged();
}

float ValueAxis::getMax() const
{
    return m_max;
}

void ValueAxis::setMax(float newMax)
{
    if (qFuzzyCompare(m_max, newMax))
        return;

    m_max = newMax;
    emit maxChanged();
    emit labelCountChanged();
}

float ValueAxis::getStep() const
{
    return m_step;
}

void ValueAxis::setStep(float newStep)
{
    if (qFuzzyCompare(m_step, newStep))
        return;

    m_step = newStep;
    emit stepChanged();
    emit labelCountChanged();
}

float ValueAxis::getLabelStart() const
{
    return m_labelStart;
}

void ValueAxis::setLabelStart(float newLabelStart)
{
    if (qFuzzyCompare(m_labelStart, newLabelStart))
        return;

    m_labelStart = newLabelStart;
    emit labelStartChanged();
}

CategoryAxis::CategoryAxis(QObject *parent)
    : AAxis{parent}
{}

int CategoryAxis::getLabelCount()
{
    return m_categories.count();
}

QStringList CategoryAxis::getCategories() const
{
    return m_categories;
}

void CategoryAxis::setCategories(const QStringList &newCategories)
{
    if(m_categories == newCategories)
        return;

    emit categoriesChanged();
    m_categories = newCategories;
}
