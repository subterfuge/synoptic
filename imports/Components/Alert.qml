import QtQuick
import Components as C
import Qt5Compat.GraphicalEffects

Item {
    enum AlertType {
        Info,
        Success,
        Warning,
        Danger
    }

    id: root
    property int type: Alert.Info
    property alias text: label.text

    implicitWidth: label.contentWidth + 15
    implicitHeight: label.contentHeight + 4

    Rectangle {
        id: background
        width: root.implicitWidth
        height: root.implicitHeight
        color: C.Palette.white
    }

    RectangularGlow {
        id: glow
        anchors.fill: background
        z: 0
        glowRadius: 2
        spread: 0
        color: C.Palette.white
        cornerRadius: 2
    }

    Text {
        id: label
        anchors.centerIn: background
        font { family: CustomFont.proggy; capitalization: Font.AllUppercase; pixelSize: 15; bold: false }
        color: C.Palette.background
    }

    function updateColor() {
        switch(root.type) {
            case Alert.Info: {
                background.color = C.Palette.white
                glow.color = C.Palette.white
                label.color = C.Palette.background
                break
            }
            case Alert.Success: {
                background.color = C.Palette.green
                glow.color = C.Palette.green
                label.color = C.Palette.background
                break
            }
            case Alert.Warning: {
                background.color = C.Palette.yellow
                glow.color = C.Palette.yellow
                label.color = C.Palette.background
                break
            }
            case Alert.Danger: {
                background.color = C.Palette.red
                glow.color = C.Palette.red
                label.color = C.Palette.white
                break
            }
            default: {
                background.color = C.Palette.white
                glow.color = C.Palette.white
                label.color = C.Palette.background
                break
            }
        }
    }

    onTypeChanged: {
        updateColor()
    }
}
