#include "Histogram.h"

#include <QAbstractItemModel>

AChartData::AChartData(QObject *parent)
        : QObject{parent}
{}

Bar::Bar(QObject *parent)
        : QObject{parent}, m_categoryIndex(0), m_value(0), m_color(QColorConstants::White), m_valueRole(-1)
{}

int Bar::getCategoryIndex() const
{
    return m_categoryIndex;
}

void Bar::setCategoryIndex(int newCategoryIndex)
{
    if (m_categoryIndex == newCategoryIndex)
        return;

    m_categoryIndex = newCategoryIndex;
    emit categoryIndexChanged();
}

float Bar::getValue() const
{
    QAbstractListModel *listModel = qvariant_cast<QAbstractListModel*>(m_model);
    if(listModel == nullptr || m_valueRole == -1)
        return m_value;

    float result = 0;
    for(int i = 0; i < listModel->rowCount(); ++i)
        result += listModel->data(listModel->index(i, 0), m_valueRole).toFloat();
    if(m_valueFormatting.isCallable())
        result = static_cast<float>(m_valueFormatting.call(QJSValueList{ result }).toNumber());
    return result;
}

void Bar::setValue(float newValue)
{
    if (qFuzzyCompare(m_value, newValue))
        return;

    m_value = newValue;
    emit valueChanged();
}

QColor Bar::getColor() const
{
    return m_color;
}

void Bar::setColor(const QColor &newColor)
{
    if (m_color == newColor)
        return;

    m_color = newColor;
    emit colorChanged();
}

QVariant Bar::getModel() const
{
    return m_model;
}

void Bar::setModel(const QVariant &newModel)
{
    if (m_model == newModel)
        return;

    m_model = newModel;
    emit modelChanged();
}

int Bar::getValueRole() const
{
    return m_valueRole;
}

void Bar::setValueRole(int newValueRole)
{
    if (m_valueRole == newValueRole)
        return;

    m_valueRole = newValueRole;
    emit valueRoleChanged();
}

QJSValue Bar::getValueFormatting() const
{
    return m_valueFormatting;
}

void Bar::setValueFormatting(QJSValue newValueFormatting)
{
    m_valueFormatting = newValueFormatting;
    emit valueFormattingChanged();
}

Histogram::Histogram(QObject *parent)
        : AChartData{parent}
{}

QQmlListProperty<Bar> Histogram::getBars()
{
    return QQmlListProperty<Bar>(this, nullptr, &Histogram::addBar, &Histogram::countBars, &Histogram::barAt,
        &Histogram::clearBars);
}

void Histogram::addBar(QQmlListProperty<Bar> *list, Bar *bar)
{
    auto *histogram = qobject_cast<Histogram*>(list->object);
    if(histogram == nullptr)
        return;

    bar->setParent(histogram);
    histogram->m_bars.append(bar);
}

qsizetype Histogram::countBars(QQmlListProperty<Bar> *list)
{
    auto *histogram = qobject_cast<Histogram*>(list->object);
    if(histogram == nullptr)
        return 0;

    return histogram->m_bars.size();
}

Bar *Histogram::barAt(QQmlListProperty<Bar> *list, qsizetype index)
{
    auto *histogram = qobject_cast<Histogram*>(list->object);
    if(histogram == nullptr)
        return nullptr;

    Q_ASSERT(index < histogram->m_bars.size());
    return histogram->m_bars[index];
}

void Histogram::clearBars(QQmlListProperty<Bar> *list)
{
    auto *histogram = qobject_cast<Histogram*>(list->object);
    if(histogram == nullptr)
        return;

    histogram->m_bars.clear();
}
