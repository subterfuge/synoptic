import QtQuick
import QtQuick.Controls

ComboBox {
    id: control

    popup: Popup {
        y: control.height - 1
        width: control.width
        implicitHeight: contentItem.implicitHeight
        padding: 1

        contentItem: Item {
            anchors.fill: parent

            TextArea {
                id: filterConditionText
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.bottom

                focus: false
                leftPadding: 12

                selectByMouse: true
                hoverEnabled: true

                wrapMode: TextEdit.WrapAnywhere
                placeholderText: qsTr("Filter")
                background: Rectangle {
                    color: "#1c2127"
                }

                //! Bottom indicator
                Rectangle {
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right

                    anchors.bottomMargin: 12
                    height: 1
                }
            }

            //! List view to show/select items
            ListView {
                clip: true
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: filterConditionText.bottom

                implicitHeight: contentHeight
                model: control.popup.visible ? control.delegateModel : null
                currentIndex: control.highlightedIndex

                ScrollIndicator.vertical: ScrollIndicator {
                }
            }


            ListView {
                clip: true
                implicitHeight: contentHeight
                model: control.popup.visible ? control.delegateModel : null
                currentIndex: control.highlightedIndex

                ScrollIndicator.vertical: ScrollIndicator {
                }
            }
        }

        background: Rectangle {
            border.color: "#21be2b"
            radius: 2
        }
    }
}