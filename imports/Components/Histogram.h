#pragma once

#include <QObject>
#include <QQmlEngine>
#include <QColor>

class AChartData : public QObject
{
    Q_OBJECT
    QML_UNCREATABLE("ChartData is abstract and shouldn't be created")
    QML_NAMED_ELEMENT(ChartData)

public:
    explicit AChartData(QObject *parent = nullptr);
};

class Bar : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int categoryIndex READ getCategoryIndex WRITE setCategoryIndex NOTIFY categoryIndexChanged FINAL)
    Q_PROPERTY(float value READ getValue WRITE setValue NOTIFY valueChanged FINAL)
    Q_PROPERTY(QColor color READ getColor WRITE setColor NOTIFY colorChanged FINAL)
    Q_PROPERTY(QVariant model READ getModel WRITE setModel NOTIFY modelChanged FINAL)
    Q_PROPERTY(int valueRole READ getValueRole WRITE setValueRole NOTIFY valueRoleChanged FINAL)
    /// Js function taking one parameter. This bar's value is passed as parameter of this function to format
    /// the output before calling getValue. If no function is set, the value is returned as is
    Q_PROPERTY(QJSValue valueFormatting READ getValueFormatting WRITE setValueFormatting NOTIFY valueFormattingChanged FINAL)
    QML_ELEMENT

public:
    explicit Bar(QObject *parent = nullptr);

    int getCategoryIndex() const;
    void setCategoryIndex(int newCategoryIndex);

    float getValue() const;
    void setValue(float newValue);

    QColor getColor() const;
    void setColor(const QColor &newColor);

    QVariant getModel() const;
    void setModel(const QVariant &newModel);

    int getValueRole() const;
    void setValueRole(int newValueRole);

    QJSValue getValueFormatting() const;
    void setValueFormatting(QJSValue newValueFormatting);

signals:
    void categoryIndexChanged();
    void valueChanged();
    void colorChanged();
    void modelChanged();
    void valueRoleChanged();
    void valueFormattingChanged();

private:
    int m_categoryIndex;
    float m_value;
    QColor m_color;
    QVariant m_model;
    int m_valueRole;
    QJSValue m_valueFormatting;

    void updateValue();
};

class Histogram : public AChartData
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Bar> bars READ getBars CONSTANT)
    Q_CLASSINFO("DefaultProperty", "bars")
    QML_ELEMENT

public:
    explicit Histogram(QObject *parent = nullptr);

    QQmlListProperty<Bar> getBars();

private:
    QList<Bar*> m_bars;

    static void addBar(QQmlListProperty<Bar> *list, Bar *bar);
    static qsizetype countBars(QQmlListProperty<Bar> *list);
    static Bar* barAt(QQmlListProperty<Bar> *list, qsizetype index);
    static void clearBars(QQmlListProperty<Bar> *list);
};
