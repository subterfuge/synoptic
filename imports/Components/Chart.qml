import QtQuick
import QtQuick.Layouts
import Components as C

Item {
    id: root

    property Axis verticalAxis: C.ValueAxis {
        width: 1
        color: "white"
        labelColor: "white"
        min: 0
        max: 1
        step: 1
        labelStart: 0
    }
    property Axis horizontalAxis: C.ValueAxis {
        width: 1
        color: "white"
        labelColor: "white"
        min: 0
        max: 1
        step: 1
        labelStart: 0
    }

    property int axisTickWidth: 10
    property int verticalAxisLabelWidth: 15
    property int horizontalAxisLabelHeight: 15
    default property ChartData chartData

    function lerp(aVal, bVal, progress) {
        return (1 - progress) * aVal + bVal * progress
    }

    function inverseLerp(aVal, bVal, value) {
        return (value - aVal) / (bVal - aVal)
    }

    function remap(oldAVal, oldBVal, newAVal, newBVal, value) {
        var progress = inverseLerp(oldAVal, oldBVal, value)
        var result = lerp(newAVal, newBVal, progress)
        return result
    }

    Loader {
        id: verticalAxisItem
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: horizontalAxisItem.top
        sourceComponent: {
            if(root.verticalAxis instanceof ValueAxis)
                return verticalValueAxisComponent
            else if(root.verticalAxis instanceof CategoryAxis)
                return verticalCategoryAxisComponent
        }
    }

    Component {
        id: verticalValueAxisComponent

        Item {
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            implicitWidth: verticalAxisLine.width + root.axisTickWidth + root.verticalAxisLabelWidth

            Rectangle {
                id: verticalAxisLine
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: root.verticalAxis.width
                color: root.verticalAxis.color
                border.width: 0
            }

            Repeater {
                id: verticalAxisRepeater
                model: root.verticalAxis.labelCount
                delegate: Rectangle {
                    id: verticalAxisRepeaterDelegate
                    required property int index

                    anchors.right: parent.right
                    width: root.axisTickWidth
                    height: root.verticalAxis.width
                    y: verticalAxisLine.height - root.remap(root.verticalAxis.labelStart, root.verticalAxis.max,
                            0, verticalAxisLine.height, root.verticalAxis.labelStart + (index * root.verticalAxis.step))
                    color: root.verticalAxis.color

                    Text {
                        id: test
                        anchors.right: parent.left
                        width: root.verticalAxisLabelWidth
                        y: -7
                        font: Qt.font({ family: CustomFont.proggy, pixelSize: 12 })
                        text: root.remap(0, root.verticalAxis.labelCount - 1, root.verticalAxis.min, root.verticalAxis.max,
                                verticalAxisRepeaterDelegate.index)
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                        color: root.verticalAxis.labelColor
                    }
                }
            }
        }
    }

    Component {
        id: verticalCategoryAxisComponent
        Item {
            // todo
        }
    }

    Item {
        id: horizontalAxisItem
        anchors.bottom: parent.bottom
        anchors.left: verticalAxisItem.right
        anchors.right: parent.right
        implicitHeight: horizontalAxisLine.height + root.axisTickWidth + root.horizontalAxisLabelHeight

        Rectangle {
            id: horizontalAxisLine
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.left: parent.left
            height: root.horizontalAxis.width
            color: root.horizontalAxis.color
            border.width: 0
        }

        Item {
            anchors.fill: parent

            Rectangle {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: -1
                width: root.horizontalAxis.width
                height: root.axisTickWidth
                color: root.horizontalAxis.color
                border.width: 0
            }

            RowLayout {
                anchors.fill: parent
                spacing: 0

                Repeater {
                    id: horizontalAxisRepeater
                    model: root.horizontalAxis.categories
                    delegate: Item {
                        required property int index
                        required property var modelData
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        Text {
                            anchors.fill: parent
                            font: Qt.font({ family: CustomFont.proggy, pixelSize: 12 })
                            text: modelData
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            color: root.verticalAxis.labelColor
                        }

                        Rectangle {
                            anchors.top: parent.top
                            anchors.right: parent.right
                            width: root.horizontalAxis.width
                            height: root.axisTickWidth
                            color: root.horizontalAxis.color
                            border.width: 0
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: chartDataLoader
        anchors { left: verticalAxisItem.right; right: parent.right; top: parent.top; bottom: horizontalAxisItem.top }
        sourceComponent: {
            if(root.chartData instanceof Histogram) {
                return histogramComponent
            }
        }
    }

    Component {
        id: histogramComponent

        Item {
            RowLayout {
                anchors.fill: parent
                spacing: 0

                Repeater {
                    model: root.horizontalAxis.categories
                    delegate: Item {
                        id: categoryItem
                        required property string modelData
                        required property int index
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        RowLayout {
                            id: categoryLayout
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom

                            Repeater {
                                id: barsRepeater
                                model: {
                                    let result = []
                                    for(let i = 0; i < root.chartData.bars.length; i++) {
                                        if(categoryItem.index === root.chartData.bars[i].categoryIndex)
                                            result.push(root.chartData.bars[i])
                                    }
                                    return result;
                                }
                                delegate: Rectangle {
                                    required property Bar modelData

                                    width: 20
                                    height: root.remap(root.verticalAxis.labelStart,
                                            root.verticalAxis.max, 0, root.height - horizontalAxisItem.height, modelData.value)
                                    y: -height
                                    color: C.Palette.greenLight
                                    border.width: 0
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
