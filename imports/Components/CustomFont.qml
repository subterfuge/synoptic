pragma Singleton
import QtQuick

QtObject {
    // readonly property FontLoader thedusCondensedFL: FontLoader {
    //     source: "/font/Thedus/ThedusCondensedLight-Regular.otf"
    // }
    //
    // readonly property FontLoader thedusWideFL: FontLoader {
    //     source: "/font/Thedus/ThedusCondensedLight-Regular.otf"
    // }
    //
    // readonly property FontLoader thedusWideBoldFL: FontLoader {
    //     source: "/font/Thedus/ThedusCondensedLight-Regular.otf"
    // }

    readonly property FontLoader proggyFL: FontLoader {
        source: "/font/Proggy/ProggyVector-Regular.otf"
    }

    readonly property FontLoader mordakFL: FontLoader {
        source: "/font/Mordak/Mordak Regular.otf"
    }

    readonly property FontLoader mordakMono1FL: FontLoader {
        source: "/font/Mordak/Mordak-mono1.otf"
    }


    // This is a variablefont, but it's not yet supported in QtQuick. Used as a static font for now
    readonly property FontLoader materialFL: FontLoader {
        source: "/font/Material/MaterialSymbolsOutlined[FILL,GRAD,opsz,wght].ttf"
    }

    // readonly property string thedusCondensed: thedusCondensedFL.name
    // readonly property string thedusWide: thedusWideFL.name
    // readonly property string thedusWideBold: thedusWideBoldFL.name
    readonly property string proggy: proggyFL.name
    readonly property string mordak: mordakFL.name
    readonly property string mordakMono1: mordakMono1FL.name
    readonly property string material: materialFL.name

    readonly property font defaultTextFont: Qt.font({ family: proggy, pixelSize: 18 })
    readonly property font smallTextFont: Qt.font({ family: proggy, pixelSize: 16 })
    readonly property font titleFont: Qt.font({family: mordak, pixelSize: 26 })
    readonly property font subTitleFont: Qt.font({ family: proggy, pixelSize: 22, bold: true })
}
