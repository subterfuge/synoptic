import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import style.SynopticStyle
import Components as C

TextField {
    id: timePicker

    enum TimeFormat {
        TwelveHoursTimeFormat,
        TwentyFourHoursTimeFormat
    }

    property int timeFormat: TimePicker.TwentyFourHoursTimeFormat
    property int hours: 0
    property int minutes: 0
    property alias model: timeListView.model
    property string textRole: ""
    property string valueRole: ""

    function checkInput() {
        let re
        switch(timePicker.timeFormat) {
            case TimePicker.TwelveHoursTimeFormat:
                re = new RegExp(/^(0?[1-9]|1[0-2]):([0-5][0-9]|00?) +([apAP][mM])$/)
                break
            case TimePicker.TwentyFourHoursTimeFormat:
                re = new RegExp(/^([01]?[0-9]|2[0-3]):([0-5][0-9]|00?)$/)
                break
        }
        let matches = re.exec(timePicker.text)
        if(matches === null)
            return { result: false, hours: 0, minutes: 0 }

        let time = timePicker.timeFormat === TimePicker.TwelveHoursTimeFormat
                ? timePicker.to24HoursFormat(matches[1], matches[2], matches[3]) : timePicker.to24HoursFormat(matches[1], matches[2])
        return { result: true, hours: time.hours, minutes: time.minutes }
    }

    function setTime(time) {
        let format = timePicker.timeFormat ===TimePicker.TwelveHoursTimeFormat ? "h:mm ap" : "hh:mm"
        timePicker.text = time.toLocaleTimeString(Qt.locale(), format)
    }

    function to24HoursFormat(hours, minutes, anteOrPostMeridiem) {
        if(timePicker.timeFormat === TimePicker.TwentyFourHoursTimeFormat || anteOrPostMeridiem !== "PM")
            return { hours: hours, minutes: minutes }

        return { hours: 12 + hours, minutes: minutes }
    }

    implicitWidth: 100
    placeholderText: qsTr("hh:mm")
    onTextRoleChanged: {
        if(timePicker.valueRole === "")
            timePicker.valueRole = timePicker.textRole
    }
    onPressed: timePopup.open()

    Text {
        anchors.right: timePicker.right
        anchors.rightMargin: 5
        anchors.verticalCenter: timePicker.verticalCenter
        // x: timePicker.width - width - timePicker.padding - 3
        // y: timePicker.topPadding + (timePicker.availableHeight - height) / 2
        color: C.Palette.front
        text: String.fromCodePoint(0xe8b5)
        font { family: C.CustomFont.material; pixelSize: 20 }
        opacity: enabled ? 1 : 0.3
        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: timePopup.open()
        }
    }

    Popup {
        id: timePopup
        y: timePicker.height
        width: timePicker.width
        height: timeListView.height

        ListView {
            id: timeListView
            property int delegateHeight: 30
            width: parent.width
            height: model.count === undefined ? delegateHeight * model.length : delegateHeight * model.count
            model: [ "9:00", "11:00", "13:00", "15:00", "17:00" ]
            delegate: MouseArea {
                width: timePopup.width
                height: timeListView.delegateHeight
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true

                onClicked: {
                    timePicker.text = timePicker.valueRole === "" ? modelData : model[timePicker.valueRole]
                    timePopup.close()
                }
                onEntered: {
                    delegateRect.color = C.Palette.front
                    delegateText.color = C.Palette.background
                }
                onExited: {
                    delegateRect.color = C.Palette.background
                    delegateText.color = C.Palette.front
                }

                Rectangle {
                    id: delegateRect
                    anchors.fill: parent
                    color: C.Palette.background
                }

                Text {
                    id: delegateText
                    anchors.fill: parent
                    anchors.leftMargin: 5
                    verticalAlignment: Text.AlignVCenter
                    color: C.Palette.front
                    font: C.CustomFont.smallTextFont
                    text: timePicker.textRole === "" ? modelData : model[timePicker.textRole]
                }
            }

            Rectangle {
                anchors.fill: parent
                color: "transparent"
                border.color: C.Palette.front
            }
        }
    }
}
