import QtQuick
import QtQuick.Controls
import Components as C

Item {
    property color normalColor: C.Palette.white
    property color hoveredColor: C.Palette.greenLight
    property color disabledColor: C.Palette.greyLight
    property alias text: buttonText.text
    property bool error: false
    property font font
    signal clicked()

    onErrorChanged : {
        if(error === true)
        {
            normalColor = C.Palette.red
            hoveredColor = C.Palette.orange
            disabledColor = C.Palette.orange
        }
        else
        {
            normalColor = C.Palette.white
            hoveredColor = C.Palette.greenLight
            disabledColor = C.Palette.greyLight
        }
    }

    QtObject {
        id: internal
        property bool pressed: false
        property bool hovered: false
    }

    id: button
    z: 1
    implicitWidth: buttonText.contentWidth + 4
    implicitHeight: 35
    font { family: CustomFont.proggy; pixelSize: 16 }

    Rectangle {
        id: rectangle
        color: button.normalColor
        y: 0
        //radius: 1
        width: button.width
        height: button.height - 3
    }

    Label {
        id: buttonText
        z: 1
        y: 0
        width: button.width
        height: button.height - 3
        color: C.Palette.background
        font: button.font
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor
        onEntered: internal.hovered = true
        onExited: internal.hovered = false
        onPressed: pressedAnimation.start()
        onReleased: releasedAnimation.start()
        onClicked: button.clicked()
    }

    Rectangle {
        id: line
        anchors {
            top: button.bottom
            topMargin: 0
        }
        x: 12
        height: 1
        width: button.width - 24
        color: button.normalColor
    }

    PropertyAnimation {
        id: pressedAnimation
        targets: [ rectangle, buttonText ]
        properties: "y"
        to: 4
        duration: 50
    }

    PropertyAnimation {
        id: releasedAnimation
        targets: [ rectangle, buttonText ]
        properties: "y"
        to: 0
        duration: 50
    }

    /*RectangularGlow {
        id: glow
        anchors.fill: button
        z: 0
        glowRadius: 2
        spread: 0
        color: normalColor
        cornerRadius: 2
    }*/

    states: [
        State {
            name: "hovered"
            when: internal.hovered
            PropertyChanges {
                target: rectangle
                color: hoveredColor
            }
            /*PropertyChanges {
                target: glow
                color: hoveredColor
            }*/
            PropertyChanges {
                target: line
                width: button.width
                x: 0
                color: hoveredColor
            }
        }
    ]

    transitions: [
        Transition {
            to: "hovered"
            SequentialAnimation {
                loops: 2
                ColorAnimation { from: normalColor; to: hoveredColor; duration: 0 }
                PauseAnimation { duration: 70 }
                ColorAnimation { from: hoveredColor; to: normalColor; duration: 0 }
                PauseAnimation { duration: 70 }
            }
            NumberAnimation { properties: "x,width"; duration: 190; easing.type: Easing.InOutQuad }
        },
        Transition {
            from: "hovered"
            NumberAnimation {
                properties: "x,width"
                duration: 140
                easing.type: Easing.InOutQuad
            }
            ColorAnimation { from: hoveredColor; to: normalColor; duration: 0 }
        }
    ]
}
