import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import style.SynopticStyle
import Components as C

TextField {
    id: datePicker
    property var selectedDate: undefined
    property date displayedDate: new Date()
    property string format: "dd/MM/yyyy"

    /// Convert text field's string to date
    /// \return type:object An object containing the result, true if date could be parsed, false otherwise,
    ///                     and the corresponding date
    function inputToDate() {
        let date = Date.fromLocaleDateString(Qt.locale(), datePicker.text.trim(), datePicker.format)
        let result = date instanceof Date && !isNaN(date)
        if(result)
            datePicker.selectedDate = date
        return { result: result, date: date }
    }

    //implicitHeight: dateTextField.implicitHeight
    implicitWidth: 140
    inputMethodHints: Qt.ImhDate
    placeholderText: qsTr("dd/mm/yyyy")

    onSelectedDateChanged: {
        datePicker.text = datePicker.selectedDate === undefined ? ""
            : datePicker.selectedDate.toLocaleDateString(Qt.locale(), datePicker.format)
    }
    onPressed: {
        if(enabled && !calendarPopup.visible) {
            calendarPopup.open()
        }
    }
    onFocusChanged: {
        if(focus)
            return

        let checkResult = inputToDate()
        if(checkResult.result === true)
            datePicker.selectedDate = checkResult.date
    }

    Text {
        anchors.right: datePicker.right
        anchors.verticalCenter: datePicker.verticalCenter
        anchors.rightMargin: 5
        color: C.Palette.front
        text: String.fromCodePoint(0xe935)
        font { family: C.CustomFont.material; pixelSize: 20 }
        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: {
                calendarPopup.open()
                datePicker.forceActiveFocus(Qt.MouseFocusReason)
            }
        }
    }

    Popup {
        id: calendarPopup

        y: datePicker.height
        // to keep the popup inside the window :
        width: calendarRect.width
        height: calendarRect.height
        margins: 0

        onVisibleChanged: {
            if(visible === true)
                datePicker.displayedDate = datePicker.selectedDate === undefined ? new Date() : datePicker.selectedDate
        }

        /// MouseArea preventing clicks through the Popup
        MouseArea {
            anchors.fill: parent
        }

        Rectangle {
            id: calendarRect
            width: calendarLayout.width + 10
            height: calendarLayout.height + 10
            color: C.Palette.background
            border.color: C.Palette.front

            ColumnLayout {
                id: calendarLayout
                anchors.centerIn: parent

                Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 30
                    color: "transparent"

                    Button {
                        id: previousMonthButton
                        anchors.left: parent.left
                        width: 20
                        height: 20
                        text: "◄"
                        onClicked: datePicker.displayedDate.setMonth(datePicker.displayedDate.getMonth() - 1)
                    }

                    Text {
                        anchors.left: previousMonthButton.right
                        anchors.right: nextMonthButton.left
                        text: datePicker.displayedDate.toLocaleDateString(Qt.locale(), "MMMM yyyy")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: C.Palette.front
                        font: C.CustomFont.defaultTextFont
                    }

                    Button {
                        id: nextMonthButton
                        anchors.right: parent.right
                        width: 20
                        height: 20
                        text: "►"
                        onClicked: datePicker.displayedDate.setMonth(datePicker.displayedDate.getMonth() + 1)
                    }
                }

                DayOfWeekRow {
                    id: dayOfWeekRow
                    Layout.fillWidth: true
                    Layout.preferredHeight: 20

                    delegate: Text {
                        text: model.narrowName
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        color: C.Palette.front
                        font: C.CustomFont.smallTextFont
                    }
                }

                MonthGrid {
                    id: grid

                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    month: datePicker.displayedDate.getMonth()
                    year: datePicker.displayedDate.getFullYear()
                    locale: Qt.locale("fr_FR")

                    delegate: Rectangle {
                        id: dayRect
                        width: 25
                        height: 25
                        border.color: C.Palette.front
                        color: C.Palette.background
                        opacity: model.month === grid.month ? 1.0 : 0.3

                        Text {
                            id: dayText
                            anchors.centerIn: parent
                            text: model.day
                            color: C.Palette.front
                            font: C.CustomFont.smallTextFont
                        }

                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true
                            onClicked: {
                                datePicker.selectedDate = model.date
                                if(model.month !== grid.month)
                                    datePicker.displayedDate = model.date
                                else
                                    calendarPopup.close()
                            }
                            onEntered: {
                                dayRect.color = C.Palette.front
                                dayText.color = C.Palette.background
                            }
                            onExited: {
                                dayRect.color = C.Palette.background
                                dayText.color = C.Palette.front
                            }
                        }
                    }
                }
            }
        }
    }
}
