#pragma once

#include <QQmlEngine>
#include <QColor>

class AAxis : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ getColor WRITE setColor NOTIFY colorChanged FINAL)
    Q_PROPERTY(QColor labelColor READ getLabelColor WRITE setLabelColor NOTIFY labelColorChanged FINAL)
    Q_PROPERTY(int width READ getWidth WRITE setWidth NOTIFY widthChanged FINAL)
    Q_PROPERTY(int labelCount READ getLabelCount NOTIFY labelCountChanged)
    QML_UNCREATABLE("Axis is an abstract class and shouldn't be created. Create a subclass like ValueAxis or"
            "CategoryAxis instead")
    QML_NAMED_ELEMENT(Axis)

public:
    explicit AAxis(QObject *parent = nullptr);

    QColor getColor() const;
    void setColor(const QColor &newColor);

    QColor getLabelColor() const;
    void setLabelColor(const QColor &newLabelColor);

    int getWidth() const;
    void setWidth(int newWidth);

    virtual int getLabelCount() = 0;

signals:
    void colorChanged();
    void labelColorChanged();
    void widthChanged();
    void labelCountChanged();

private:
    QColor m_color;
    QColor m_labelColor;
    int m_width;
};


class ValueAxis : public AAxis
{
    Q_OBJECT
    Q_PROPERTY(float min READ getMin WRITE setMin NOTIFY minChanged FINAL)
    Q_PROPERTY(float max READ getMax WRITE setMax NOTIFY maxChanged FINAL)
    Q_PROPERTY(float step READ getStep WRITE setStep NOTIFY stepChanged FINAL)
    Q_PROPERTY(float labelStart READ getLabelStart WRITE setLabelStart NOTIFY labelStartChanged FINAL)
    QML_ELEMENT

public:
    explicit ValueAxis(QObject *parent = nullptr);

    int getLabelCount() override;

    float getMin() const;
    void setMin(float newMin);

    float getMax() const;
    void setMax(float newMax);

    float getStep() const;
    void setStep(float newStep);

    float getLabelStart() const;
    void setLabelStart(float newLabelStart);

signals:
    void minChanged();
    void maxChanged();
    void stepChanged();
    void labelStartChanged();

private:
    float m_min;
    float m_max;
    float m_step;
    float m_labelStart;
};


class CategoryAxis : public AAxis
{
    Q_OBJECT
    Q_PROPERTY(QStringList categories READ getCategories WRITE setCategories NOTIFY categoriesChanged FINAL)
    QML_ELEMENT

public:
    explicit CategoryAxis(QObject *parent = nullptr);

    int getLabelCount() override;

    QStringList getCategories() const;
    void setCategories(const QStringList &newStringList);

signals:
    void categoriesChanged();

private:
    QStringList m_categories;
};
