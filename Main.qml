import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts
import Synoptic
import style.SynopticStyle
import Components as C

Window {
    enum View {
        None,
        ViewProject,
        ViewBoard,
        ViewKanban,
        ViewCalendar
    }

    property int currentView: Main.ViewBoard
    property Project project
    property int taskBarHeight: 30
    property string workspace
    property Item stopWatchItem

    id: mainWindow
    minimumWidth: 1100
    minimumHeight: 700
    visible: true
    visibility: Window.Maximized
    title: qsTr("Synoptic")
    flags: Qt.FramelessWindowHint | Qt.Window
    color: C.Palette.background

    Component.onCompleted: {
        mainWindow.stopWatchItem = stopWatch
    }

    TaskBar {
        id: taskBar
        target: mainWindow
        text: mainWindow.title
        height: taskBarHeight
    }

    WindowsFramelessHelper {
        titlebar: taskBar
        borderWidth: 7
    }

    SideBar {
        id: sideBar
        anchors.left: parent.left
        anchors.top: taskBar.bottom
        width: 35
        height: parent.height - taskBarHeight

        onProjectClicked: {
            currentView = Main.ViewProject
        }
        onBoardClicked: {
            currentView = Main.ViewBoard
        }
        onKanbanClicked: {
            currentView = Main.ViewKanban
        }
        onCalendarClicked: {
            currentView = Main.ViewCalendar
        }
        onSettingsClicked: {
            settingsPopup.open()
        }
    }

    ColumnLayout {
        anchors.top: taskBar.bottom
        anchors.bottom: parent.bottom
        anchors.left: sideBar.right
        anchors.right: parent.right

        Item {
            id: mainPanel

            Layout.fillWidth: true
            Layout.fillHeight: true

            BoardView {
                id: boardView
                anchors.fill: parent
                anchors.topMargin: 10
                anchors.leftMargin: 20
                anchors.rightMargin: 20
                visible: false
            }

            KanbanView {
                id: kanbanView
                anchors.fill: parent
                anchors.topMargin: 10
                anchors.leftMargin: 20
                anchors.rightMargin: 20
                visible: false
            }

            CalendarView {
                id: calendarView
                anchors.fill: parent
                anchors.margins: 50
                visible: false
            }

            ProjectView {
                id: projectView
                anchors.centerIn: parent
                width: Math.min(parent.width, 1000)
                height: Math.min(parent.height, 700)
                visible: false

                onProjectSelected: (project) => {
                    let id = project.dbId
                    mainWindow.project = project
                    utils.setProjectFilter(id)
                    mainWindow.currentView = Main.ViewBoard
                }
            }
        }

        Item {
            id: bottomBar
            Layout.fillWidth: true
            Layout.preferredHeight: 100

            ColumnLayout {
                id: sprintOverview

                property TicketInfo sprintInfo: TicketDataSource.getInfo()

                function updateText() {
                    let count
                    let estimation
                    let currentSprintId = Settings.currentSprint
                    if(currentSprintId <= 0) {
                        count = qsTr("%1 tickets in next sprint").arg(sprintInfo.sprintReadyTicketsCount)
                        estimation = qsTr("%1 story points").arg(sprintInfo.sprintReadyEstimate)
                        sprintButton.text = qsTr("Start sprint")
                    } else {
                        let sprint = SprintDataSource.get(currentSprintId)
                        count = qsTr("%1/%2 tickets done").arg(sprintInfo.sprintReadyDoneCount)
                                .arg(sprintInfo.sprintReadyTicketsCount)
                        estimation = qsTr("%1/%2 story points").arg(sprintInfo.sprintReadyDoneEstimate)
                                .arg(sprintInfo.sprintReadyEstimate)
                        sprintButton.text = qsTr("Stop sprint")
                    }
                    sprintTicketText.text = count
                    sprintEstimationText.text = estimation
                }

                Connections {
                    target: sprintOverview.sprintInfo
                    function onInfoChanged() {
                        sprintOverview.updateText()
                    }
                }

                Component.onCompleted: {
                    sprintOverview.updateText()
                }

                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 5

                Text {
                    id: sprintTicketText
                    font: C.CustomFont.subTitleFont
                    color: C.Palette.front
                }

                Text {
                    id: sprintEstimationText
                    Layout.topMargin: -10
                    font: C.CustomFont.smallTextFont
                    color: C.Palette.front
                }

                Button {
                    id: sprintButton
                    onClicked: {
                        if(Settings.currentSprint <= 0)
                            startSprintPopup.open()
                        else {
                            confirmPopup.open()
                        }
                    }
                }
            }

            StartSprintPopup {
                id: startSprintPopup
                onClosed: sprintOverview.updateText()
            }

            Popup {
                id: confirmPopup
                anchors.centerIn: Overlay.overlay
                padding: 0
                modal: true
                closePolicy: Popup.NoAutoClose
                implicitWidth: 300
                implicitHeight: 150

                Rectangle {
                    anchors.centerIn: parent
                    width: parent.width
                    height: parent.height
                    color: C.Palette.smokeGreen

                    Text {
                        anchors.top: parent.top
                        anchors.topMargin: -30
                        text: qsTr("Warning")
                        color: C.Palette.front
                        font { family: C.CustomFont.mordak; pixelSize: 26 }
                    }

                    Text {
                        anchors.fill: parent
                        anchors.margins: 10
                        text: qsTr("Are you sure you want to close the sprint?" +
                                   " Finished tickets on the Kanban board will be archived.")
                        wrapMode: Text.Wrap
                        color: C.Palette.front
                        font: C.CustomFont.defaultTextFont
                    }

                    Row {
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 5
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: 15
                        C.Button {
                            text: qsTr("Ok")
                            onClicked: {
                                SprintDataSource.closeSprint(Settings.currentSprint)
                                sprintOverview.updateText()
                                confirmPopup.close()
                            }
                        }
                        C.Button {
                            text: qsTr("Cancel")
                            onClicked: confirmPopup.close()
                        }
                    }
                }
            }

            RowLayout {
                id: stopWatchLayout
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter

                StopWatch {
                    id: stopWatch

                    property var ticketId
                    property var projectId

                    function saveTimeEntry(ticketId, projectId) {
                        TimeEntryDataSource.insert(ticketId, projectId, stopWatch.startTime,
                            Math.floor(stopWatch.elapsedTimeMs / 1000))
                        stopWatch.reset()
                        selectTicketPopup.close()
                        stopWatch.ticketId = undefined
                        stopWatch.projectId = undefined
                    }

                    Layout.fillHeight: true
                    onClicked: {
                        if(stopWatch.running)
                            return

                        if(stopWatch.ticketId === undefined || stopWatch.projectId === undefined)
                            selectTicketPopup.open()
                        else
                            stopWatch.saveTimeEntry(stopWatch.ticketId, stopWatch.projectId)
                    }

                    Popup {
                        id: selectTicketPopup
                        x: 5
                        y: -height
                        width: stopWatchLayout.width - 15
                        height: 370
                        padding: 0

                        Rectangle {
                            anchors.fill: parent
                            color: C.Palette.background
                            border.color: C.Palette.front
                        }

                        ListView {
                            id: ticketList
                            anchors.fill: parent
                            anchors.margins: 5
                            clip: true
                            spacing: 2
                            boundsBehavior: Flickable.StopAtBounds
                            interactive: true
                            ScrollBar.vertical: ScrollBar {}
                            model: QuickProxyModel {
                                sourceModel: TicketModel {
                                    dataSource: TicketDataSource
                                }
                                sortRole: TicketModel.PositionRole

                                RoleFilter {
                                    role: TicketModel.InNextSprintRole
                                    filter: true
                                }
                                RoleFilter {
                                    role: TicketModel.KanbanColumnRole
                                    filter: [ 1, 2 ]
                                }
                            }
                            delegate: MouseArea {
                                width: ticketList.width - 15
                                height: 30
                                hoverEnabled: true
                                cursorShape: Qt.PointingHandCursor

                                onClicked: {
                                    if(stopWatch.running || stopWatch.elapsedTimeMs === 0) {
                                        stopWatch.ticketId = model.id
                                        stopWatch.projectId = model.projectId
                                    }
                                    else
                                        stopWatch.saveTimeEntry(model.id, model.projectId)
                                    selectTicketPopup.close()
                                }
                                onEntered: {
                                    rectangle.color = C.Palette.front
                                    title.color = C.Palette.background
                                }
                                onExited: {
                                    rectangle.color = C.Palette.background
                                    title.color = C.Palette.front
                                }

                                Rectangle {
                                    id: rectangle
                                    anchors.fill: parent
                                    color: C.Palette.background
                                    border.color: C.Palette.front
                                }

                                Text {
                                    id: title
                                    anchors.fill: parent
                                    anchors.margins: 5
                                    color: C.Palette.front
                                    font: C.CustomFont.smallTextFont
                                    verticalAlignment: Text.AlignVCenter
                                    text: model.title
                                }
                            }
                        }
                    }
                }

                ColumnLayout {
                    Layout.leftMargin: -10
                    Layout.rightMargin: 10
                    Button {
                        Layout.preferredHeight: 40
                        Layout.preferredWidth: 40
                        text: String.fromCodePoint(0xe3c9)
                        font { family: C.CustomFont.material; pixelSize: 32 }
                        onClicked: selectTicketPopup.open()
                    }
                    Button {
                        Layout.preferredHeight: 40
                        Layout.preferredWidth: 40
                        text: String.fromCodePoint(0xe872)
                        font { family: C.CustomFont.material; pixelSize: 32 }
                        onClicked: {
                            stopWatch.ticketId = undefined
                            stopWatch.projectId = undefined
                            stopWatch.reset()
                        }
                    }
                }
            }
        }
    }

    C.NotificationArea {
        id: notificationArea
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: taskBar.bottom
        anchors.topMargin: 10
        width: 320
        height: Math.min(contentHeight, parent.height - taskBar.height - bottomBar.height - 20)

        Component.onCompleted: {
            C.NotificationSystem.addArea(notificationArea)
        }
    }

    Connections {
        target: C.NotificationSystem
        function onAreaAdded() {
            let sprintId = Settings.currentSprint
            if(sprintId > 0) {
                let sprint = SprintDataSource.get(sprintId)
                let now = new Date()
                if(now.setHours(0,0,0,0) === sprint.endDate.setHours(0,0,0,0))
                    C.NotificationSystem.push(qsTr("Sprint ending"), qsTr("The current sprint end is due today"),
                                              C.NotificationSystem.TypeImportant)
                else if(now.setHours(0,0,0,0) > sprint.endDate.setHours(0,0,0,0))
                    C.NotificationSystem.push(qsTr("Sprint ended"),
                                              qsTr("The current sprint is past due since %1")
                                              .arg(sprint.endDate.toLocaleDateString(Qt.locale(), "dd/MM/yyyy")),
                                              C.NotificationSystem.TypeImportant)
            }
        }
    }

    SettingsPopup {
        id: settingsPopup
        visible: false
    }

    onCurrentViewChanged: {
        boardView.visible = false
        kanbanView.visible = false
        projectView.visible = false
        calendarView.visible = false
        switch(currentView) {
            case Main.ViewCalendar:
                calendarView.visible = true
                break
            case Main.ViewBoard:
                boardView.open()
                break
            case Main.ViewKanban:
                kanbanView.visible = true
                break
            case Main.ViewProject:
                projectView.visible = true
                break
        }
    }
}
