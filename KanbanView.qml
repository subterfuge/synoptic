import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Synoptic
import style.SynopticStyle
import Components as C

Item {
    id: root

    property int spacing: 0
    property list<string> model: [ "ready", "doing", "validate", "complete" ]
    property int sprintId

    QtObject {
        id: internal

        property var draggedItem: undefined
    }

    function savePositioning() {
        console.trace()
        TicketDataSource.beginTransaction()
        for(let i = 0; i < delegateModel.items.count; i++) {
            let delegateItem = delegateModel.items.get(i)
            let result = TicketDataSource.setKanbanPosition(delegateItem.model.projectId, delegateItem.model.id,
                delegateItem.itemsIndex)
            if(!result) {
                // TODO: Display error message to user
                TicketDataSource.rollback()
                break
            }
        }
        TicketDataSource.endTransaction()
    }

    onVisibleChanged: {
        if(visible) {
            if(Settings.currentSprint <= 0)
                startSprintPopup.open()
            kanbanModel.update()
        } else {
            startSprintPopup.close()
        }
    }

    StartSprintPopup {
        id: startSprintPopup
        modal: true
        closePolicy: Popup.NoAutoClose
    }

    RowLayout {
        id: layout
        anchors.fill: parent

        Repeater {
            id: columnRepeater
            model: root.model
            delegate: ColumnLayout {
                required property var modelData
                required property int index
                id: columnLayout
                property string column: modelData
                property alias contentWidth: ticketList.contentWidth
                Layout.fillWidth: true
                Layout.fillHeight: true

                Label {
                    id: listTitle
                    Layout.preferredWidth: parent.width
                    Layout.fillWidth: true
                    text: modelData.toUpperCase()
                    font: C.CustomFont.defaultTextFont
                    color: C.Palette.background
                    horizontalAlignment: Text.AlignHCenter
                    background: Rectangle {
                        color: C.Palette.front
                    }
                }

                Rectangle {
                    id: rect
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    border.color: C.Palette.front
                    color: "transparent"

                    DropArea {
                        anchors.fill: parent
                        onEntered: {
                            if(internal.draggedItem === undefined || internal.draggedItem.model.kanbanColumn === columnLayout.index)
                                return

                            internal.draggedItem.model.kanbanColumn = columnLayout.index
                            delegateModel.items.move(internal.draggedItem.DelegateModel.itemsIndex, delegateModel.items.count - 1)
                        }
                    }

                    ListView {
                        id: ticketList
                        anchors.fill: parent
                        clip: true
                        spacing: root.spacing
                        boundsBehavior: Flickable.StopAtBounds
                        interactive: true
                        ScrollBar.vertical: ScrollBar {}
                        model: delegateModel.parts[columnLayout.column]
                    }
                }
            }
        }
    }

    DelegateModel {
        id: delegateModel
        model: QuickProxyModel {
            id: kanbanModel
            sourceModel: TicketModel {
                dataSource: TicketDataSource
            }
            sortRole: TicketModel.KanbanPositionRole
            dynamicSortFilter: false

            RoleFilter {
                role: TicketModel.InNextSprintRole
                filter: true
            }
        }
        delegate: KanbanPackage { width: layout.width / root.model.length; height: 120 }
    }

    component KanbanPackage: Package {
        id: pack
        required property var model
        property int width
        property int height
        property int visualIndex: DelegateModel.itemsIndex

        DropArea {
            id: dropAreaReady
            property string modelData: "ready"
            property int columnIndex: 0
            Package.name: modelData
            visible: pack.model.kanbanColumn === columnIndex
            width: pack.width
            height: pack.model.kanbanColumn === columnIndex ? pack.height : 0
            onEntered: function(drag) {
                if(drag.source.model.kanbanColumn !== pack.model.kanbanColumn) {
                    drag.source.model.kanbanColumn = columnIndex
                    let item = delegateModel.items.get(drag.source.DelegateModel.itemsIndex)
                    item.model.kanbanColumn = columnIndex
                    delegateModel.items.move(drag.source.DelegateModel.itemsIndex, delegateModel.items.count - 1)
                }
                delegateModel.items.move(drag.source.DelegateModel.itemsIndex, draggable.DelegateModel.itemsIndex)
            }
        }

        DropArea {
            id: dropAreaDoing
            property string modelData: "doing"
            property int columnIndex: 1
            Package.name: modelData
            visible: pack.model.kanbanColumn === columnIndex
            width: pack.width
            height: pack.model.kanbanColumn === columnIndex ? pack.height : 0
            onEntered: function(drag) {
                if(drag.source.model.kanbanColumn !== pack.model.kanbanColumn) {
                    drag.source.model.kanbanColumn = columnIndex
                    let item = delegateModel.items.get(drag.source.DelegateModel.itemsIndex)
                    item.model.kanbanColumn = columnIndex
                    delegateModel.items.move(drag.source.DelegateModel.itemsIndex, delegateModel.items.count - 1)
                }
                delegateModel.items.move(drag.source.DelegateModel.itemsIndex, draggable.DelegateModel.itemsIndex)
            }
        }

        DropArea {
            id: dropAreaValidate
            property string modelData: "validate"
            property int columnIndex: 2
            Package.name: modelData
            visible: pack.model.kanbanColumn === columnIndex
            width: pack.width
            height: pack.model.kanbanColumn === columnIndex ? pack.height : 0
            onEntered: function(drag) {
                if(drag.source.model.kanbanColumn !== pack.model.kanbanColumn) {
                    drag.source.model.kanbanColumn = columnIndex
                    let item = delegateModel.items.get(drag.source.DelegateModel.itemsIndex)
                    item.model.kanbanColumn = columnIndex
                    delegateModel.items.move(drag.source.DelegateModel.itemsIndex, delegateModel.items.count - 1)
                }
                delegateModel.items.move(drag.source.DelegateModel.itemsIndex, draggable.DelegateModel.itemsIndex)
            }
        }

        DropArea {
            id: dropAreaComplete
            property string modelData: "complete"
            property int columnIndex: 3
            Package.name: modelData
            visible: pack.model.kanbanColumn === columnIndex
            width: pack.width
            height: pack.model.kanbanColumn === columnIndex ? pack.height : 0
            onEntered: function(drag) {
                if(drag.source.model.kanbanColumn !== pack.model.kanbanColumn) {
                    drag.source.model.kanbanColumn = columnIndex
                    let item = delegateModel.items.get(drag.source.DelegateModel.itemsIndex)
                    item.model.kanbanColumn = columnIndex
                    delegateModel.items.move(drag.source.DelegateModel.itemsIndex, delegateModel.items.count - 1)
                }
                delegateModel.items.move(drag.source.DelegateModel.itemsIndex, draggable.DelegateModel.itemsIndex)
            }
        }

        MouseArea {
            id: draggable
            property var model: pack.model
            property int visualIndex: pack.visualIndex
            property alias startTimerState: startTimerButton.state
            property int previousColumn: draggable.model.kanbanColumn

            function onDropped() {
                if(root.model[draggable.previousColumn] === "validate"
                        && root.model[draggable.model.kanbanColumn] !== "validate"
                        && root.model[draggable.model.kanbanColumn] !== "complete"
                        && dodModel.anyChecked()) {
                    resetDod.visible = true
                } else if(root.model[draggable.model.kanbanColumn] === "complete"
                        || root.model[draggable.model.kanbanColumn] === "validate") {
                    resetDod.visible = false
                }
            }

            anchors.leftMargin: 3
            anchors.rightMargin: 15
            anchors.topMargin: 3

            Drag.active: dragHandler.active
            Drag.source: draggable
            Drag.hotSpot.x: width / 2
            Drag.hotSpot.y: height / 2

            state: root.model[draggable.model.kanbanColumn]
            states: [
                State {
                    name: "dragged"
                    ParentChange {
                        target: draggable
                        parent: root
                    }

                    AnchorChanges {
                        target: draggable
                        anchors {
                            horizontalCenter: undefined
                            verticalCenter: undefined
                        }
                    }
                }
            ]

            DragHandler {
                id: dragHandler
                target: draggable
                onActiveChanged: {
                    if(active) {
                        draggable.state = "dragged"
                        internal.draggedItem = draggable
                        draggable.previousColumn = draggable.model.kanbanColumn
                    } else {
                        draggable.state = root.model[draggable.model.kanbanColumn]
                        internal.draggedItem = undefined
                        root.savePositioning()
                        startTimerButton.updateState()
                        draggable.onDropped()
                        draggable.Drag.drop()
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                color: C.Palette.background
                border.color: C.Palette.front
            }

            Rectangle {
                id: title
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: 20
                color: C.Palette.front

                Text {
                    anchors.fill: parent
                    anchors.leftMargin: 2
                    color: C.Palette.background
                    font: C.CustomFont.defaultTextFont
                    verticalAlignment: Text.AlignVCenter
                    text: model.title
                }
            }

            Text {
                id: elapsedText
                anchors.bottom: draggable.bottom
                anchors.left: draggable.left
                anchors.leftMargin: 5
                anchors.bottomMargin: 2
                visible: root.model[draggable.model.kanbanColumn] === "doing" ||
                         root.model[draggable.model.kanbanColumn] === "complete"
                color: C.Palette.front
                font { family: C.CustomFont.mordakMono1; pixelSize: 40 }
                text: qsTr("%1h %2m").arg(Math.floor(timeEntryModel.totalTime / 60 / 60))
                      .arg(Math.floor(timeEntryModel.totalTime / 60) % 60)

                TimeEntryModel {
                    id: timeEntryModel
                    dataSource: TimeEntryDataSource
                    ticketId: model.id
                    ticketProjectId: model.projectId
                    Component.onCompleted: update()
                }
            }

            Text {
                anchors.left: elapsedText.right
                anchors.bottom: draggable.bottom
                anchors.bottomMargin: 4
                visible: root.model[draggable.model.kanbanColumn] === "doing"
                color: C.Palette.front
                font { family: C.CustomFont.proggy; pixelSize: 22; bold: true }
                text: "/"
            }

            Text {
                anchors.left: elapsedText.right
                anchors.leftMargin: 12
                anchors.bottom: draggable.bottom
                anchors.bottomMargin: 7
                visible: root.model[draggable.model.kanbanColumn] === "doing"
                color: C.Palette.front
                font { family: C.CustomFont.mordakMono1; pixelSize: 22; }
                text: {
                    let time = model.storyPoints * WorkspaceDataSource.get(Settings.getWorkspace()).storyPointEstimation
                    return qsTr("%1h %2m").arg(Math.floor(time / 60)).arg(time % 60)
                }
            }

            ScrollView {
                anchors.top: title.bottom
                anchors.bottom: draggable.bottom
                anchors.left: draggable.left
                anchors.right: startTimerButton.left
                anchors.margins: 5
                visible: root.model[draggable.model.kanbanColumn] === "validate"

                ListView {
                    id: dodList
                    anchors.fill: parent
                    spacing: 2
                    clip: true
                    boundsBehavior: Flickable.StopAtBounds
                    model: TicketDefinitionOfDoneModel {
                        id: dodModel
                        dataSource: DefinitionOfDoneDataSource
                        ticketId: model.id
                        projectId: model.projectId
                    }
                    delegate: Row {
                        spacing: 2

                        CheckBox {
                            width: 20
                            height: 20
                            checked: model.checked
                            onClicked: model.checked = checked
                        }

                        Text {
                            text: model.description
                            font: C.CustomFont.smallTextFont
                            color: C.Palette.front
                        }
                    }
                }
            }

            Text {
                id: storyPointsText
                anchors.top: title.bottom
                anchors.left: draggable.left
                anchors.margins: 5
                visible: root.model[draggable.model.kanbanColumn] === "complete"
                color: C.Palette.front
                font { family: C.CustomFont.mordakMono1; pixelSize: 40 }
                text: qsTr("%1 SP").arg(draggable.model.storyPoints)
            }

            Rectangle {
                id: resetDod
                anchors { fill: parent; topMargin: title.height; leftMargin: 1; rightMargin: 1; bottomMargin: 1 }
                visible: false
                color: C.Palette.background

                Text {
                    x: 10
                    y: 15
                    text: qsTr("Uncheck definition of done?")
                    color: C.Palette.front
                    font { family: C.CustomFont.mordak; pixelSize: 22; bold: true }
                }

                Button {
                    id: yesBtn
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.margins: 15
                    text: qsTr("Yes")
                    onClicked: {
                        DefinitionOfDoneDataSource.uncheckAll(model.id, model.projectId)
                        resetDod.visible = false
                    }
                }

                Button {
                    anchors.bottom: parent.bottom
                    anchors.left: yesBtn.right
                    anchors.bottomMargin: 15
                    anchors.leftMargin: 45
                    text: qsTr("No")
                    onClicked: resetDod.visible = false
                }
            }

            MouseArea {
                id: startTimerButton
                property real glowBlur: 0.0
                property color color: baseColor
                readonly property color baseColor: Qt.rgba(1, 0.471, 0.212, 0.631)
                readonly property color runningColor: "#fe3550"
                readonly property color pausedColor: "#00c0a4"
                readonly property color disabledColor: "#4a4a4a"

                function updateStopWatchControl() {
                    if(startTimerButton.state === "disabled")
                        return

                    if(mainWindow.stopWatchItem.projectId === model.projectId
                            && mainWindow.stopWatchItem.ticketId === model.id) {
                        if(mainWindow.stopWatchItem.running)
                            startTimerButton.state = "running"
                        else // the stopwatch is only paused if no ticket is selected, so we disregard the "paused" state
                            startTimerButton.state = ""
                    }
                    else
                        startTimerButton.state = ""
                }

                function updateState() {
                    if(root.model[draggable.model.kanbanColumn] === "ready"
                            || root.model[draggable.model.kanbanColumn] === "complete")
                    {
                        if(startTimerButton.state === "running")
                            mainWindow.stopWatchItem.saveTimeEntry(mainWindow.stopWatchItem.ticketId,
                                                                   mainWindow.stopWatchItem.projectId)
                        startTimerButton.state = "disabled"
                    } else {
                        if(startTimerButton.state === "disabled")
                            startTimerButton.state = ""
                    }
                }

                anchors.right: parent.right
                anchors.rightMargin: 2
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 2
                width: 52
                height: 52
                cursorShape: enabled ? Qt.PointingHandCursor : Qt.ArrowCursor
                enabled: state !== "disabled"

                onClicked: {
                    if(startTimerButton.state === "running") {
                        mainWindow.stopWatchItem.saveTimeEntry(mainWindow.stopWatchItem.ticketId,
                                                               mainWindow.stopWatchItem.projectId)
                    } else {
                        if(mainWindow.stopWatchItem.running)
                            mainWindow.stopWatchItem.saveTimeEntry(mainWindow.stopWatchItem.ticketId,
                                                                   mainWindow.stopWatchItem.projectId)
                        mainWindow.stopWatchItem.ticketId = model.id
                        mainWindow.stopWatchItem.projectId = model.projectId
                        mainWindow.stopWatchItem.start()
                    }
                }

                states: [
                    State {
                        name: ""
                        PropertyChanges {
                            target: startTimerButton
                            color: startTimerButton.baseColor
                            glowBlur: 0.0
                        }
                        PropertyChanges {
                            target: stopIcon
                            visible: false
                        }
                        PropertyChanges {
                            target: playIcon
                            visible: true
                        }
                    },
                    State {
                        name: "disabled"
                        PropertyChanges {
                            target: startTimerButton
                            color: startTimerButton.disabledColor
                            glowBlur: 0.0
                        }
                        PropertyChanges {
                            target: stopIcon
                            visible: false
                        }
                        PropertyChanges {
                            target: playIcon
                            visible: true
                        }
                    },
                    State {
                        name: "running"
                        PropertyChanges {
                            target: startTimerButton
                            color: startTimerButton.runningColor
                            glowBlur: 0.2
                        }
                        PropertyChanges {
                            target: stopIcon
                            visible: true
                        }
                        PropertyChanges {
                            target: playIcon
                            visible: false
                        }
                    },
                    State {
                        name: "paused"
                        PropertyChanges {
                            target: startTimerButton
                            color: startTimerButton.pausedColor
                            glowBlur: 0.2
                        }
                        PropertyChanges {
                            target: stopIcon
                            visible: false
                        }
                        PropertyChanges {
                            target: playIcon
                            visible: true
                        }
                    }
                ]
                Component.onCompleted: updateState()

                layer.enabled: true
                layer.effect: ShaderEffect {
                    readonly property Item iSource: startTimerButton
                    property real iTime: frameAnimation.elapsedTime
                    readonly property Item iSourceBlur1: blurHelper.blurSrc1
                    readonly property Item iSourceBlur2: blurHelper.blurSrc2
                    readonly property Item iSourceBlur3: blurHelper.blurSrc3
                    readonly property Item iSourceBlur4: blurHelper.blurSrc4
                    readonly property Item iSourceBlur5: blurHelper.blurSrc5
                    readonly property real blurMultiplier: 0.0
                    property real fastBlurAmount: startTimerButton.glowBlur
                    property real speed: 0.0
                    property color colorFilter: startTimerButton.color

                    onSpeedChanged: {
                        frameAnimation.running = speed !== 0.0;
                    }

                    vertexShader: '/shaders/glow.vert.qsb'
                    fragmentShader: '/shaders/glow.frag.qsb'
                    implicitHeight: startTimerButton.implicitHeight
                    implicitWidth: startTimerButton.implicitWidth

                    FrameAnimation {
                        id: frameAnimation
                        running: true
                    }
                }

                Connections {
                    target: mainWindow.stopWatchItem
                    function onRunningChanged() {
                        startTimerButton.updateStopWatchControl()
                    }
                    function onTicketIdChanged() {
                        startTimerButton.updateStopWatchControl()
                    }
                    function onProjectIdChanged() {
                        startTimerButton.updateStopWatchControl()
                    }
                }

                BlurHelper {
                    id: blurHelper
                    anchors.fill: parent
                    source: startTimerButton
                    property int blurMax: 64
                    property real blurMultiplier: 0.0
                }

                Image {
                    anchors.fill: parent
                    anchors.margins: 2
                    source: "/image/warning-lines-button.png"
                }

                Rectangle {
                    id: stopIcon
                    anchors.fill: parent
                    anchors.margins: 12
                    visible: false
                    // width: 40
                    // height: 40
                    color: "white"
                }

                Text {
                    id: playIcon
                    anchors.fill: parent
                    anchors.topMargin: -4
                    text: String.fromCodePoint(0xe037)
                    color: "white"
                    font { family: C.CustomFont.material; pixelSize: 50 }
                }
            }

            Instantiator {
                id: statesInstantiator
                model: root.model
                delegate: State {
                    required property var modelData
                    required property int index
                    name: modelData
                    ParentChange {
                        id: parentChange
                        target: draggable
                        parent: {
                            switch(draggable.model.kanbanColumn) {
                            case 0:
                                dropAreaReady
                                break
                            case 1:
                                dropAreaDoing
                                break
                            case 2:
                                dropAreaValidate
                                break
                            case 3:
                                dropAreaComplete
                                break
                            }
                        }
                    }
                    AnchorChanges {
                        target: draggable
                        anchors {
                            left: draggable.parent.left
                            top: draggable.parent.top
                            right: draggable.parent.right
                            bottom: draggable.parent.bottom
                        }
                    }

                    Component.onCompleted: {
                        draggable.states.push(this)
                    }
                }
            }
        }
    }
}
