import QtQuick
import QtQuick.Layouts
import Synoptic
import style.SynopticStyle
import Components as C

Popup {
    id: root

    function submit() {
        let workspaceId = workspaceComboBox.currentValue
        if(Settings.setWorkspace(workspaceId))
            utils.resetDataSources()
    }

    anchors.centerIn: Overlay.overlay
    width: 600
    height: 370
    padding: 0
    closePolicy: Popup.NoAutoClose
    modal: true

    onAboutToShow: {
        let index = workspaceComboBox.indexOfValue(Settings.getWorkspace())
        workspaceComboBox.currentIndex = index
    }

    Rectangle {
        id: background
        anchors.fill: parent
        color: C.Palette.background
        border.color: C.Palette.front

        Text {
            anchors.top: parent.top
            anchors.topMargin: -30
            text: qsTr("Settings")
            color: C.Palette.front
            font { family: C.CustomFont.mordak; pixelSize: 26 }
        }
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.topMargin: 10
        spacing: 20

        GroupBox {
            title: qsTr("Workspace")

            RowLayout {
                spacing: 10

                ComboBox {
                    id: workspaceComboBox
                    model: WorkspaceModel {
                        dataSource: WorkspaceDataSource
                    }
                    textRole: "title"
                    valueRole: "id"
                }

                Button {
                    id: newWorkspaceBtn
                    Layout.leftMargin: 10
                    text: qsTr("New workspace")
                    icon.name: "add"
                    onClicked: newWorkspacePopup.open()
                }

                Button {
                    id: editWorkspaceBtn
                    text: qsTr("Edit workspace")
                    icon.name: "edit"
                    onClicked: editWorkspacePopup.open()
                }
            }
        }

        GroupBox {
            title: qsTr("Language")

            RowLayout {
                spacing: 10

                LangButton {
                    id: frBtn
                    iconSource: "/image/fr.svg"
                    text: "Français"
                    onClicked: {
                        let changed = Settings.setLocale("fr")
                        if(changed)
                        {
                            Settings.reloadTranslation("fr")
                            parent.updateLangBtnDisplay("fr")
                        }
                    }
                }

                LangButton {
                    id: enBtn
                    iconSource: "/image/en.svg"
                    text: "English"
                    onClicked: {
                        let changed = Settings.setLocale("en")
                        if(changed) {
                            Settings.reloadTranslation("en")
                            parent.updateLangBtnDisplay("en")
                        }
                    }
                }

                component LangButton: MouseArea {
                    id: langBtnRoot
                    property alias iconSource: flagIcon.source
                    property alias text: label.text
                    property bool activated: false
                    width: 150
                    height: 40
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor

                    Rectangle {
                        anchors.fill: parent
                        border.width: 1
                        border.color: C.Palette.front
                        color: parent.activated ? C.Palette.front : "transparent"
                    }

                    Image {
                        id: flagIcon
                        x: 5
                        anchors.verticalCenter: parent.verticalCenter
                        height: 20
                        fillMode: Image.PreserveAspectFit

                        Rectangle {
                            anchors.fill: parent
                            border.width: 1
                            border.color: langBtnRoot.activated ? C.Palette.background : C.Palette.front
                            color: "transparent"
                        }
                    }

                    Text {
                        id: label
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: flagIcon.right
                        anchors.leftMargin: 4
                        color: parent.activated ? C.Palette.background : C.Palette.front
                        font: C.CustomFont.defaultTextFont
                    }
                }

                Component.onCompleted: {
                    let locale = Settings.getLocale()
                    if(locale === "")
                        locale = "en"
                    updateLangBtnDisplay(locale)
                }

                function updateLangBtnDisplay(locale) {
                    switch(locale)
                    {
                        case "fr":
                            frBtn.activated = true
                            enBtn.activated = false
                            break
                        case "en":
                            frBtn.activated = false
                            enBtn.activated = true
                            break
                    }
                }
            }
        }
    }

    C.CrossButton {
        id: closeRect
        anchors.right: background.right
        anchors.top: background.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        onClicked: root.close()
    }

    Row {
        anchors { bottom: parent.bottom; right: parent.right; bottomMargin: 10; rightMargin: 10 }
        spacing: 15

        C.Button {
            text: qsTr("Ok")
            onClicked: {
                submit()
                close()
            }
        }

        C.Button {
            text: qsTr("Apply")
            onClicked: submit()
        }

        C.Button {
            text: qsTr("Cancel")
            onClicked: close()
        }
    }

    NewWorkspacePopup {
        id: newWorkspacePopup
        onClosed: {
            if(workspaceComboBox.currentIndex === -1 && WorkspaceDataSource.any())
                workspaceComboBox.currentIndex = 0
        }
    }

    Popup {
        id: editWorkspacePopup
        anchors.centerIn: Overlay.overlay
        modal: true
        closePolicy: Popup.NoAutoClose
        onOpened: {
            editWorkspaceView.index = workspaceComboBox.currentIndex
            editWorkspaceView.workspace = WorkspaceDataSource.get(workspaceComboBox.currentValue)
        }

        EditWorkspace {
            id: editWorkspaceView
            onClose: editWorkspacePopup.close()
            workspace: WorkspaceDataSource.get(workspaceComboBox.currentValue)
            index: workspaceComboBox.currentIndex
        }
    }
}
