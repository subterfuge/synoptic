import QtQuick
import QtQuick.Controls
import Components as C

Rectangle {
    color: C.Palette.black
    opacity: 0.5
    x: sideBar.width
    y: taskBar.height
    width: Window.width - sideBar.width
    height: Window.height - taskBar.height
}