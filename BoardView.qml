import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Layouts
import Synoptic
import style.SynopticStyle
import Components as C

Item {
    id: boardView

    property Project project
    readonly property int itemHeight: 60
    readonly property int grabBtnWidth: 34

    function open() {
        if(project === null)
            projectPopup.open()
        visible = true
    }

    function setSprintReady(index, sprintReady) {
        let begin
        let end
        if(sprintReady === true) {
            begin = 0
            end = index
        } else {
            begin = index
            end = proxyTicketModel.rowCount() - 1
        }
        for(let i = begin; i <= end; i++) {
            proxyTicketModel.setData(proxyTicketModel.index(i, 0), sprintReady, TicketModel.InNextSprintRole)
        }
    }

    onVisibleChanged: {
        if(visible === false)
            projectPopup.close()
    }

    onProjectChanged: {
        projectFilterCb.setProject(project)
    }

    QuickProxyModel {
        id: proxyTicketModel
        property string projectId
        sourceModel: ticketModel
        sortRole: TicketModel.PositionRole

        RoleFilter {
            role: TicketModel.ProjectIdRole
            filter: proxyTicketModel.projectId
        }
    }

    TicketModel {
        id: ticketModel
        dataSource: TicketDataSource
    }


    Popup {
        id: projectPopup
        anchors.centerIn: Overlay.overlay
        width: Math.min(parent.width, 1000)
        height: Math.min(parent.height, 700)
        padding: 0
        closePolicy: Popup.NoAutoClose
        modal: true

        ProjectView {
            anchors.centerIn: parent
            width: parent.width
            height: parent.height

            onProjectSelected: (project) => {
                let id = project.dbId
                boardView.project = project
                proxyTicketModel.projectId = id
                projectPopup.close()
            }
        }

        Overlay.modal: AppModalRectangle {}
    }

    RowLayout {
        height: parent.height
        width: parent.width

        ColumnLayout {
            id: leftPanel
            Layout.horizontalStretchFactor: 1
            Layout.fillWidth: true
            Layout.fillHeight: true

            Item {
                id: filters
                Layout.preferredHeight: filterLabel.height + filterButtons.height
                Layout.fillWidth: true

                MouseArea {
                    id: filterLabel
                    cursorShape: Qt.PointingHandCursor
                    hoverEnabled: true
                    height: Math.max(filterLabelIcon.contentHeight, filterLabelText.contentHeight)
                    width: filterLabelIcon.contentWidth + filterLabelText.contentWidth

                    onEntered: {
                        filterLabelIcon.color = C.Palette.greenLight
                        filterLabelText.color = C.Palette.greenLight
                    }
                    onExited: {
                        filterLabelIcon.color = C.Palette.front
                        filterLabelText.color = C.Palette.front
                    }
                    onClicked: {
                            filterButtons.state === "visible" ? filterButtons.state = "" : filterButtons.state = "visible"
                    }

                    Text {
                        id: filterLabelIcon
                        anchors.left: parent.left
                        color: C.Palette.front
                        font { family: C.CustomFont.material; pixelSize: 18 }
                        text: String.fromCodePoint(0xe152) // Material icon "Filter"
                    }

                    Text {
                        id: filterLabelText
                        anchors.left: filterLabelIcon.right
                        color: C.Palette.front
                        font: C.CustomFont.smallTextFont
                        text: qsTr("Filters")
                    }
                }

                RowLayout {
                    id: filterButtons
                    anchors.top: filterLabel.bottom
                    height: 0
                    visible: false
                    spacing: 5

                    states: [
                        State {
                            name: "visible"
                            PropertyChanges { target: filterButtons; height: projectFilter.height + 5; visible: true }
                        }
                    ]

                    transitions: Transition {
                        NumberAnimation { properties: "height"; easing.type: Easing.Linear; duration: 50 }
                    }

                    ColumnLayout {
                        id: projectFilter
                        Text {
                            text: qsTr("Project")
                            font: C.CustomFont.smallTextFont
                            color: C.Palette.front
                        }

                        ComboBox {
                            id: projectFilterCb

                            function setProject(project) {
                                currentIndex = indexOfValue(project.dbId)
                            }

                            model: projectModel
                            Layout.minimumWidth: 200
                            currentIndex: -1
                            textRole: "title"
                            valueRole: "id"
                            onCurrentValueChanged: {
                                boardView.project = projectDataSource.get(currentValue)
                                proxyTicketModel.projectId = project.dbId
                            }
                        }
                    }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                Layout.preferredHeight: 45
                spacing: 5

                Rectangle {
                    Layout.preferredHeight: parent.height - 4
                    Layout.preferredWidth: boardView.itemHeight
                    color: C.Palette.front

                    Text {
                        anchors.fill: parent
                        font: C.CustomFont.defaultTextFont
                        color: C.Palette.background
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("Sprint\nready")
                    }
                }

                Rectangle {
                    Layout.preferredHeight: parent.height - 4
                    Layout.fillWidth: true
                    color: C.Palette.front

                    Text {
                        anchors.fill: parent
                        font: C.CustomFont.defaultTextFont
                        color: C.Palette.background
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("Tickets")
                    }
                }
            }

            ListView {
                id: productBacklog
                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true
                spacing: 8
                boundsBehavior: Flickable.StopAtBounds // TODO: change for mobile

                ScrollBar.vertical: ScrollBar {}
                model: ticketDelegateModel
            }

            C.Button {
                id: newTicketBtn
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 200
                normalColor: C.Palette.white
                hoveredColor: C.Palette.greenLight
                text: qsTr("New ticket")
                font.family: C.CustomFont.proggy
                font.pixelSize: 16
                onClicked: {
                    ticketView.setTicketNew(boardView.project, proxyTicketModel.rowCount())
                    ticketView.visible = true
                    ticketModel.resetSelected()
                }
            }
        }

        TicketView {
            id: ticketView

            onVisibleChanged: {
                state = visible ? "visible" : ""
            }

            Layout.preferredWidth: 0
            Layout.fillHeight: true
            visible: false
            states: [
                State {
                    name: "visible"
                    PropertyChanges { target: ticketView; Layout.preferredWidth: 800 }
                }
            ]
            transitions: [
                Transition {
                    reversible: true
                    NumberAnimation { properties: "Layout.preferredWidth"; easing.type: Easing.Linear; duration: 50 }
                }
            ]
        }
    }

    DelegateModel {
        id: ticketDelegateModel
        model: proxyTicketModel

        delegate: Item {
            id: draggable

            width: productBacklog.width - 12
            height: boardView.itemHeight

            Item {
                id: ticket
                property int previousIndex: -1

                width: productBacklog.width
                height: draggable.height
                Drag.active: dragArea.dragging
                Drag.source: dragArea
                Drag.hotSpot.x: width / 2
                Drag.hotSpot.y: height / 2

                RowLayout {
                    anchors.fill: parent
                    anchors.rightMargin: 15

                    CheckBox {
                        Layout.fillHeight: true
                        Layout.preferredWidth: height
                        checked: model.inNextSprint
                        enabled: Settings.currentSprint <= 0
                        onReleased: {
                            boardView.setSprintReady(DelegateModel.itemsIndex, !model.inNextSprint)
                        }

                        MouseArea {
                            anchors.fill: parent
                            enabled: !parent.enabled
                            hoverEnabled: !parent.enabled
                            ToolTip.visible: hoverEnabled && containsMouse
                            ToolTip.text: qsTr("You can't change sprint ready status during a sprint")
                        }
                    }

                    Rectangle {
                        id: dataRect

                        property color frontColor: C.Palette.front
                        property bool selected: false

                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        onSelectedChanged: {
                            if(selected)
                                border.color = C.Palette.greenLight
                            else
                                border.color = C.Palette.front
                        }

                        color: C.Palette.background
                        border.color: model.selected ? C.Palette.greenLight : C.Palette.front

                        RowLayout {
                            anchors {
                                left: parent.left
                                leftMargin: 5
                                right: parent.right
                                rightMargin: 2
                            }

                            Text {
                                id: title
                                Layout.fillWidth: true
                                text: model.title
                                color: dataRect.frontColor
                                elide: Text.ElideRight
                                font {
                                    family: C.CustomFont.proggy; pixelSize: 20; bold: true
                                }
                            }

                            Text {
                                id: days
                                Layout.minimumWidth: daysBackground.contentWidth
                                horizontalAlignment: Text.AlignRight
                                text: {
                                    let d = model.dueDate
                                    return Math.min(Math.max(Math.round(((d.setHours(0,0,0,0) - (new Date()).setHours(0,0,0,0)) / 86400000)), -999), 999)
                                }
                                color: {
                                    let d = model.dueDate
                                    let diff = Math.round((d.setHours(0,0,0,0) - (new Date()).setHours(0,0,0,0)) / 86400000)
                                    if(diff > 0)
                                        return C.Palette.greenLight
                                    else if(diff === 0)
                                        return C.Palette.yellow
                                    else
                                        return C.Palette.red
                                }
                                font {
                                    family: C.CustomFont.mordakMono1; bold: true; pixelSize: 24
                                }

                                Text {
                                    id: daysBackground
                                    z: -1
                                    text: "-000"
                                    color: C.Palette.smokeGreen
                                    font {
                                        family: C.CustomFont.mordakMono1; bold: true; pixelSize: 24
                                    }
                                }
                            }
                        }

                        RowLayout {
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 5
                            x: 5

                            KanoCategorySlider {
                                id: kanoCategorySlider
                                functionnalValue: model.presentKanoFeel
                                dysfunctionnalValue: model.absentKanoFeel
                                labelVisible: false
                            }
                        }

                        RowLayout {
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            anchors.rightMargin: 2

                            Text {
                                id: priorityText
                                font {
                                    family: C.CustomFont.mordak; bold: true; pixelSize: 24
                                }
                                horizontalAlignment: Text.AlignRight
                                color: {
                                    switch(model.priority) {
                                        case 0:
                                            return Qt.hsva(0.6, 1, 1)
                                        case 1:
                                            return Qt.hsva(0.4, 1, 1)
                                        case 2:
                                            return Qt.hsva(0.2, 1, 1)
                                        case 3:
                                            return Qt.hsva(0, 1, 1)
                                    }
                                }
                                text: {
                                    switch(model.priority) {
                                        case 0:
                                            return qsTr("Low")
                                        case 1:
                                            return qsTr("Medium")
                                        case 2:
                                            return qsTr("High")
                                        case 3:
                                            return qsTr("Urgent")
                                    }
                                }
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true

                            onClicked: {
                                if(model.selected) {
                                    model.selected = false
                                    ticketView.visible = false
                                }
                                else {
                                    model.selected = true
                                    ticketView.setTicket(model.id, model.projectId)
                                    ticketView.visible = true
                                }
                            }
                            onEntered: {
                                dataRect.state = "hovered"
                            }
                            onExited: {
                                dataRect.state = ""
                            }
                        }

                        states: [
                            State {
                                name: "hovered"
                                PropertyChanges { target: dataRect; color: C.Palette.front; frontColor: C.Palette.background }
                                PropertyChanges { target: days; color: C.Palette.background }
                                PropertyChanges { target: daysBackground; visible: false }
                                PropertyChanges { target: kanoCategorySlider; lightMode: true }
                                PropertyChanges { target: priorityText; color: C.Palette.background }
                            }
                        ]
                    }

                    Rectangle {
                        id: grabBtn
                        Layout.preferredHeight: draggable.height
                        Layout.preferredWidth: boardView.grabBtnWidth
                        enabled: !model.inNextSprint
                        color: C.Palette.grey

                        Image {
                            x: Math.floor(parent.width / 2 - width / 2)
                            y: Math.floor(parent.height / 2 - height / 2)
                            source: enabled ? "/image/grab.png" : "/image/grab-disabled.png"
                        }

                        MouseArea {
                            id: dragArea

                            property bool dragging: false

                            anchors.fill: parent
                            hoverEnabled: parent.enabled ? false : true
                            ToolTip.visible: hoverEnabled && containsMouse
                            ToolTip.text: qsTr("Sprint-ready tickets can't be rearranged")

                            cursorShape: {
                                if(parent.enabled)
                                    dragging ? Qt.ClosedHandCursor : Qt.OpenHandCursor
                            }
                            drag.target: dragging ? ticket : undefined
                            drag.axis: Drag.YAxis

                            onPressed: {
                                ticket.previousIndex = DelegateModel.itemsIndex
                                dragging = true
                            }
                            onReleased: {
                                dragging = false
                                ticket.Drag.drop()
                                ticketModel.move(ticket.previousIndex, DelegateModel.itemsIndex, project)
                            }
                        }
                    }
                }

                states: State {
                    when: dragArea.dragging

                    ParentChange {
                        target: ticket
                        parent: boardView
                    }
                    AnchorChanges {
                        target: ticket
                        anchors {
                            horizontalCenter: undefined
                            verticalCenter: undefined
                        }
                    }
                }
            }

            DropArea {
                anchors {
                    fill: parent
                    margins: 10
                }
                enabled: !model.inNextSprint

                onEntered: (drag) => {
                    ticketDelegateModel.items.move(
                        drag.source.DelegateModel.itemsIndex,
                        dragArea.DelegateModel.itemsIndex
                    )
                }
            }
        }
    }
}
