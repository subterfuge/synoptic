import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Shapes
import QtQuick.Layouts
import Synoptic
import style.SynopticStyle
import Components as C

Item {
    id: projectView
    signal projectSelected(Project project)

    onVisibleChanged: {
        if(!visible)
            newProjectPopup.close()
    }

    Column {
        spacing: 5

        Text {
            text: qsTr("Project list")
            font: C.CustomFont.titleFont
            color: C.Palette.front
        }

        GridView {
            id: projectGrid
            width: projectView.width
            height: projectView.height - 80
            cellWidth: 210
            cellHeight: 110
            boundsBehavior: Flickable.StopAtBounds // TODO: change for mobile
            interactive: true
            clip: true

            model: projectModel
            delegate: projectCard

            ScrollBar.vertical: ScrollBar {}
        }

        C.Button {
            id: newProjectBtn
            anchors.horizontalCenter: parent.horizontalCenter
            //anchors.top: projectGrid.bottom
            width: 120
            height: 35
            text: qsTr("New project")
            font.family: C.CustomFont.proggy
            font.pixelSize: 16
            onClicked: newProjectPopup.open()
        }
    }

    NewProjectPopup {
        id: newProjectPopup
    }

    Component {
        id: projectCard

        Item {
            width: projectGrid.cellWidth
            height: projectGrid.cellHeight

            MouseArea {
                anchors.fill: parent
                anchors.margins: 5
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onClicked: {
                    projectSelected(projectDataSource.get(model.id))
                }
                onEntered: {
                    projectCardRect.color = C.Palette.front
                    projectId.color = C.Palette.background
                    projectTitle.color = C.Palette.background
                    projectLastTimeUsed.color = C.Palette.background
                }
                onExited: {
                    projectCardRect.color = C.Palette.background
                    projectId.color = C.Palette.front
                    projectTitle.color = C.Palette.front
                    projectLastTimeUsed.color = C.Palette.front
                }

                Rectangle {
                    id: projectCardRect
                    anchors.fill: parent
                    color: C.Palette.background
                    border.color: C.Palette.front
                }

                Column {
                    anchors.fill: parent
                    anchors.margins: 2
                    Text {
                        id: projectId
                        text: model.id
                        font {
                            family: C.CustomFont.mordak; pixelSize: 20; bold: true
                        }
                        color: C.Palette.front
                    }

                    Text {
                        id: projectTitle
                        width: parent.width
                        text: model.title
                        font: C.CustomFont.defaultTextFont
                        color: C.Palette.front
                        wrapMode: Text.Wrap
                        maximumLineCount: 2
                        elide: Text.ElideRight
                    }
                }

                Text {
                    x: 2
                    id: projectLastTimeUsed
                    anchors.bottom: parent.bottom
                    text: qsTr("Used last: ") + model.lastTimeUsed.toLocaleString(
                        Qt.locale("en_GB"), Locale.ShortFormat)
                    font { family: C.CustomFont.proggy; pixelSize: 12 }
                    color: C.Palette.front
                }
            }
        }
    }
}
