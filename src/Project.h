#pragma once

#include <QObject>
#include <QtQml/qqmlregistration.h>
#include <QDateTime>
#include "Workspace.h"

namespace Synoptic
{
    class Project : public QObject
    {
        Q_OBJECT
        QML_ELEMENT

        Q_PROPERTY(QString dbId READ getId WRITE setId NOTIFY idChanged FINAL)
        Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged FINAL)
        Q_PROPERTY(QDateTime creationDate READ getCreationDate WRITE setCreationDate NOTIFY creationDateChanged FINAL)
        Q_PROPERTY(QDateTime lastTimeUsed READ getLastTimeUsed WRITE setLastTimeUsed NOTIFY lastTimeUsedChanged FINAL)
        Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged FINAL)
        Q_PROPERTY(Workspace* workspace READ getWorkspace WRITE setWorkspace NOTIFY workspaceChanged FINAL)

    public:
        explicit Project(QObject *parent = nullptr);

        QString getId() const;
        void setId(const QString &newId);

        QString getTitle() const;
        void setTitle(const QString &newTitle);

        QDateTime getCreationDate() const;
        void setCreationDate(const QDateTime &newCreationDate);

        QDateTime getLastTimeUsed() const;
        void setLastTimeUsed(const QDateTime &newLastTimeUsed);

        QString getDescription() const;
        void setDescription(const QString &newDescription);

        Workspace* getWorkspace() const;
        void setWorkspace(Workspace *newWorkspace);

    signals:
        void idChanged();
        void titleChanged();
        void creationDateChanged();
        void lastTimeUsedChanged();
        void descriptionChanged();
        void workspaceChanged();

    private:
        QString m_id;
        QString m_title;
        QDateTime m_creationDate;
        QDateTime m_lastTimeUsed;
        QString m_description;
        Workspace *m_workspace;
    };

} // Synoptic
