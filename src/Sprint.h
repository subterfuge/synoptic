#pragma once

#include <QObject>
#include <QDateTime>

namespace Synoptic
{
    class Workspace;

    class Sprint : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(int id READ getId WRITE setId NOTIFY idChanged FINAL)
        Q_PROPERTY(bool closed READ getClosed WRITE setClosed NOTIFY closedChanged FINAL)
        Q_PROPERTY(QString sprintGoal READ getSprintGoal WRITE setSprintGoal NOTIFY sprintGoalChanged FINAL)
        Q_PROPERTY(QDateTime startDate READ getStartDate WRITE setStartDate NOTIFY startDateChanged FINAL)
        Q_PROPERTY(QDateTime endDate READ getEndDate WRITE setEndDate NOTIFY endDateChanged FINAL)
        Q_PROPERTY(QString productGoal READ getProductGoal WRITE setProductGoal NOTIFY productGoalChanged FINAL)
        Q_PROPERTY(Workspace* workspace READ getWorkspace WRITE setWorkspace NOTIFY workspaceChanged FINAL)

    public:
        explicit Sprint(QObject *parent = nullptr);

        int getId() const;
        void setId(int newId);

        bool getClosed() const;
        void setClosed(bool newClosed);

        QString getSprintGoal() const;
        void setSprintGoal(const QString &newSprintGoal);

        QDateTime getStartDate() const;
        void setStartDate(const QDateTime &newStartDate);

        QDateTime getEndDate() const;
        void setEndDate(const QDateTime &newEndDate);

        QString getProductGoal() const;
        void setProductGoal(const QString &newProductGoal);

        Workspace* getWorkspace() const;
        void setWorkspace(Workspace *newWorkspace);

    signals:
        void idChanged();
        void closedChanged();
        void sprintGoalChanged();
        void startDateChanged();
        void endDateChanged();
        void productGoalChanged();
        void workspaceChanged();

    private:
        int m_id;
        bool m_closed;
        QString m_sprintGoal;
        QDateTime m_startDate;
        QDateTime m_endDate;
        QString m_productGoal;
        Workspace *m_workspace;
    };

} // namespace Synoptic
