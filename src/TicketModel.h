#pragma once

#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include <QtQml/qqmlregistration.h>
#include "src/TicketDataSource.h"

namespace Synoptic
{
    class TicketModel : public QAbstractListModel
    {
        Q_OBJECT

        Q_PROPERTY(TicketDataSource *dataSource READ getDataSource WRITE setDataSource CONSTANT)
        QML_ELEMENT

    public:
        enum TicketRoles {
            IdRole = Qt::UserRole,
            ProjectIdRole,
            TitleRole,
            DescriptionRole,
            PositionRole,
            PresentKanoFeelRole,
            AbsentKanoFeelRole,
            StoryPointsRole,
            DueDateRole,
            PriorityRole,
            KanbanColumnRole,
            KanbanPositionRole,
            InNextSprintRole,
            SprintIdRole,
            ParentIdRole,
            SelectedRole,
        };
        Q_ENUM(TicketRoles)

        explicit TicketModel(QObject *parent = nullptr);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        bool setData(const QModelIndex &index, const QVariant &value,
                     int role = Qt::EditRole) override;

        Qt::ItemFlags flags(const QModelIndex& index) const override;

        QHash<int, QByteArray> roleNames() const override;

        Q_INVOKABLE void move(int from, int to, Synoptic::Project *project);

        Q_INVOKABLE void resetSelected();

        TicketDataSource *getDataSource() const;
        void setDataSource(TicketDataSource *newDataSource);

    private:
        TicketDataSource *m_dataSource;
    };
} // namespace Synoptic
