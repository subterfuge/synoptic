#include "SprintModel.h"

#include "SprintDataSource.h"

Synoptic::SprintModel::SprintModel(QObject *parent)
    : QAbstractListModel(parent)
{}

int Synoptic::SprintModel::rowCount(const QModelIndex &parent) const
{

}

QVariant Synoptic::SprintModel::data(const QModelIndex &index, int role) const
{

}

QHash<int, QByteArray> Synoptic::SprintModel::roleNames() const
{

}

void Synoptic::SprintModel::update()
{

}

Synoptic::SprintDataSource *Synoptic::SprintModel::getDataSource() const
{

}

void Synoptic::SprintModel::setDataSource(Synoptic::SprintDataSource *newDataSource)
{
    if(m_dataSource == newDataSource)
        return;

    beginResetModel();
    if(m_dataSource != nullptr)
        m_dataSource->disconnect(this);
    m_dataSource = newDataSource;
    if(m_dataSource != nullptr)
    {
        // todo
    }
    update();
    endResetModel();
}
