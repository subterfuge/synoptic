#pragma once

#include <QObject>
#include <QTranslator>
#include <QSettings>
#include <QtQml/qqmlregistration.h>
#include <QQmlEngine>

namespace Synoptic
{
    class Settings : public QObject
    {
        Q_OBJECT
        Q_PROPERTY(int currentSprint READ getCurrentSprint WRITE setCurrentSprint NOTIFY currentSprintChanged)
    public:
        friend class Utils;

        /// Set the workspace in settings.
        /// \returns true if the workspace has changed, false if the new value is the same as the old.
        Q_INVOKABLE bool setWorkspace(int workspaceId) const;
        Q_INVOKABLE int getWorkspace() const;

        /// Sets the locale in
        Q_INVOKABLE bool setLocale(const QString &locale);
        Q_INVOKABLE QString getLocale() const;

        Q_INVOKABLE bool reloadTranslation(QString locale);

        bool setCurrentSprint(int sprintId);
        int getCurrentSprint() const;

    signals:
        void currentSprintChanged();

    private:
        explicit Settings(QObject *parent = nullptr);

        static QTranslator m_translator;

        template<typename T>
        static bool setSetting(const QString &key, const T &value)
        {
            QSettings settings;
            T previousValue;
            bool previousValueSet = settings.contains(key);
            if(previousValueSet)
                previousValue = settings.value(key).value<T>();
            else
                previousValue = T();
            if(previousValueSet && previousValue == value)
            {
                qDebug() << "Settings:" << key << "value unchanged (" << value << ")";
                return false;
            }

            settings.setValue(key, value);
            qDebug() << "Settings:" << key << "value changed (old:" << previousValue << "; new:" << value << ")";
            return true;
        }
    };

    struct QmlSettingsFactory
    {
        Q_GADGET
        QML_FOREIGN(Synoptic::Settings)
        QML_SINGLETON
        QML_NAMED_ELEMENT(Settings)

    public:
        static Synoptic::Settings* create(QQmlEngine*, QJSEngine *engine);

    private:
        inline static QJSEngine *m_engine = nullptr;
    };
} // namespace Synoptic
