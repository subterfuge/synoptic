#include "TicketDataSource.h"

#include <QSqlQuery>
#include <QDate>
#include "Exception.h"
#include "DbManager.h"
#include "ProjectDataSource.h"
#include "Utils.h"

namespace Synoptic
{
    int TicketInfo::getSprintReadyTicketsCount() const
    {
        return m_sprintReadyTicketsCount;
    }

    void TicketInfo::setSprintReadyTicketsCount(int newSprintReadyTicketsCount)
    {
        if(newSprintReadyTicketsCount == m_sprintReadyTicketsCount)
            return;

        m_sprintReadyTicketsCount = newSprintReadyTicketsCount;
        emit sprintReadyTicketsCountChanged();
    }

    float TicketInfo::getSprintReadyEstimate() const
    {
        return m_sprintReadyEstimate;
    }

    void TicketInfo::setSprintReadyEstimate(float newSprintReadyEstimate)
    {
        if(newSprintReadyEstimate == m_sprintReadyEstimate)
            return;

        m_sprintReadyEstimate = newSprintReadyEstimate;
        emit sprintReadyEstimateChanged();
    }

    int TicketInfo::getSprintReadyDoneCount() const
    {
        return m_sprintReadyDoneCount;
    }

    void TicketInfo::setSprintReadyDoneCount(int newSprintReadyDoneCount)
    {
        if(newSprintReadyDoneCount == m_sprintReadyDoneCount)
            return;

        m_sprintReadyDoneCount = newSprintReadyDoneCount;
        emit sprintReadyDoneCountChanged();
    }

    float TicketInfo::getSprintReadyDoneEstimate() const
    {
        return m_sprintReadyDoneEstimate;
    }

    void TicketInfo::setSprintReadyDoneEstimate(float newSprintReadyDoneEstimate)
    {
        if(newSprintReadyDoneEstimate == m_sprintReadyDoneEstimate)
            return;

        m_sprintReadyDoneEstimate = newSprintReadyDoneEstimate;
        emit sprintReadyDoneEstimateChanged();
    }

    TicketDataSource::TicketDataSource(QObject *parent)
        : QObject{parent}
    {
        if(!m_init)
        {
            resetFromDb(Utils::getInstance().getSettings().getWorkspace());
            m_init = true;
        }
    }

    QList<Ticket*>& TicketDataSource::getAll()
    {
        return m_list;
    }

    bool TicketDataSource::update(Synoptic::Ticket *newTicket)
    {
        Q_ASSERT(!m_list.empty());
        if(newTicket->getProject() == nullptr)
            throw ArgumentException("newTicket's project shouldn't be null");

        Ticket *ticket = get(newTicket->getId(), newTicket->getProject()->getId());
        if(ticket == nullptr)
        {
            qCritical() << "Ticket with id =" << newTicket->getId() << "and project id ="
                    << newTicket->getProject()->getId() << "not found in datasource";
            return false;
        }

        QVariant newSprintId;
        if(ticket->getSprint() == nullptr)
            newSprintId = QVariant(QMetaType::fromType<int>());
        else
            newSprintId = QVariant(ticket->getSprint()->getId());
        QVariant newParentTicketId;
        if(ticket->getParent() == nullptr)
            newParentTicketId = QVariant(QMetaType::fromType<int>());
        else
            newParentTicketId = QVariant(ticket->getParent()->getId());
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare(DbManager::UPDATE_TICKET_SQL);
        query.bindValue(":id", ticket->getId());
        query.bindValue(":projectId", ticket->getProject()->getId());
        query.bindValue(":title", ticket->getTitle());
        query.bindValue(":position", ticket->getPosition());
        query.bindValue(":description", ticket->getDescription());
        query.bindValue(":presentKanoFeel", static_cast<int>(ticket->getPresentKanoFeel()));
        query.bindValue(":absentKanoFeel", static_cast<int>(ticket->getAbsentKanoFeel()));
        query.bindValue(":storyPoints", ticket->getStoryPoints());
        query.bindValue(":dueDate", ticket->getDueDate().toSecsSinceEpoch());
        query.bindValue(":priority", static_cast<int>(ticket->getPriority()));
        query.bindValue(":kanbanColumn", ticket->getKanbanColumn());
        query.bindValue(":kanbanPosition", ticket->getKanbanPosition());
        query.bindValue(":inNextSprint", ticket->getInNextSprint());
        query.bindValue(":parentTicketId", newParentTicketId);
        query.bindValue(":sprintId", newSprintId);
        query.exec();
        auto result = query.lastError();
        if(result.type() != QSqlError::NoError)
        {
            qCritical() << "Error updating ticket: " << result;
            return false;
        }

        if(ticket != newTicket)
        {
            ticket->setTitle(newTicket->getTitle());
            ticket->setPosition(newTicket->getPosition());
            ticket->setDescription(newTicket->getDescription());
            ticket->setPresentKanoFeel(newTicket->getPresentKanoFeel());
            ticket->setAbsentKanoFeel(newTicket->getAbsentKanoFeel());
            ticket->setStoryPoints(newTicket->getStoryPoints());
            ticket->setDueDate(newTicket->getDueDate());
            ticket->setPriority(newTicket->getPriority());
            ticket->setKanbanColumn(newTicket->getKanbanColumn());
            ticket->setKanbanPosition(newTicket->getKanbanPosition());
            ticket->setSprint(newTicket->getSprint());
            ticket->setParent(newTicket->getParent());
        }
        emit getSignals().itemChanged(getIndex(ticket));
        updateInfo();
        return true;
    }

    Ticket *TicketDataSource::get(int id, const QString &projectId) const
    {
        auto isTicketPredicate = [=](Ticket* t) {
            return t->getId() == id && t->getProject() != nullptr && t->getProject()->getId() == projectId;
        };
        return *std::find_if(m_list.begin(), m_list.end(), isTicketPredicate);
    }

    TicketInfo* TicketDataSource::getInfo()
    {
        static TicketInfo* info = new TicketInfo();
        return info;
    }

    void TicketDataSource::updateInfo()
    {
        int count = 0;
        float estimate = 0.f;
        int doneCount = 0;
        float doneEstimate = 0.f;
        for(auto* ticket : m_list)
        {
            if(ticket->getInNextSprint())
            {
                ++count;
                estimate += ticket->getStoryPoints();
                if(ticket->getKanbanColumn() == 3)
                {
                    ++doneCount;
                    doneEstimate += ticket->getStoryPoints();
                }
            }
        }
        getInfo()->setSprintReadyTicketsCount(count);
        getInfo()->setSprintReadyEstimate(estimate);
        getInfo()->setSprintReadyDoneCount(doneCount);
        getInfo()->setSprintReadyDoneEstimate(doneEstimate);
        emit getInfo()->infoChanged();
    }

    int TicketDataSource::getIndex(Synoptic::Ticket *ticket) const
    {
        if(ticket->getProject() == nullptr)
        {
            qWarning() << "Ticket have no project";
            return 0;
        }

        auto isTicketPredicate = [=](Ticket* t) {
            return t->getId() == ticket->getId() && t->getProject() != nullptr && t->getProject()->getId() == ticket->getProject()->getId();
        };
        auto result = std::find_if(m_list.begin(), m_list.end(), isTicketPredicate);
        if(result == m_list.end())
            return -1;

        return result - m_list.begin();
    }

    Ticket *TicketDataSource::getSelected() const
    {
        return m_selectedTicket;
    }

    QModelIndex TicketDataSource::getSelectedIndex() const
    {
        return m_selectedTicketIndex;
    }

    void TicketDataSource::setSelected(Ticket* ticket, const QModelIndex& index)
    {
        m_selectedTicket = ticket;
        m_selectedTicketIndex = index;
    }

    int TicketDataSource::insert(const QString &projectId, const QString &title, int position,
           const QString &description, Ticket::KanoFeel presentKanoFeel, Ticket::KanoFeel absentKanoFeel,
           float storyPoints, const QDateTime &dueDate, Ticket::Priority priority, int kanbanColumn,
           int kanbanPosition, bool inNextSprint)
    {
        Ticket *ticket = new Ticket(this);
        auto *project = Synoptic::ProjectDataSource().get(projectId);
        if(project == nullptr)
            throw ArgumentException(("project not found for id " + projectId + " shouldn't be null").toStdString());

        ticket->setId(getNextId(project));
        ticket->setProject(project);
        ticket->setTitle(title);
        ticket->setPosition(position);
        ticket->setDescription(description);
        ticket->setPresentKanoFeel(presentKanoFeel);
        ticket->setAbsentKanoFeel(absentKanoFeel);
        ticket->setStoryPoints(storyPoints);
        ticket->setDueDate(dueDate);
        ticket->setPriority(priority);
        ticket->setKanbanColumn(kanbanColumn);
        ticket->setKanbanPosition(kanbanPosition);
        ticket->setInNextSprint(inNextSprint);
        QSqlQuery query;
        query.prepare(DbManager::INSERT_TICKET_SQL);
        query.bindValue(":id", ticket->getId());
        query.bindValue(":projectId", ticket->getProject()->getId());
        query.bindValue(":title", ticket->getTitle());
        query.bindValue(":position", ticket->getPosition());
        query.bindValue(":description", ticket->getDescription());
        query.bindValue(":presentKanoFeel", static_cast<int>(ticket->getPresentKanoFeel()));
        query.bindValue(":absentKanoFeel", static_cast<int>(ticket->getAbsentKanoFeel()));
        query.bindValue(":storyPoints", ticket->getStoryPoints());
        query.bindValue(":dueDate", ticket->getDueDate().toSecsSinceEpoch());
        query.bindValue(":priority", static_cast<int>(ticket->getPriority()));
        query.bindValue(":kanbanColumn", ticket->getKanbanColumn());
        query.bindValue(":kanbanPosition", ticket->getKanbanPosition());
        query.bindValue(":inNextSprint", ticket->getInNextSprint());
        if(ticket->getSprint() == nullptr)
            query.bindValue(":sprintId", QVariant());
        else
            query.bindValue(":sprintId", ticket->getSprint()->getId());
        if(ticket->getParent() == nullptr)
            query.bindValue(":parentTicketId", QVariant());
        else
            query.bindValue(":parentTicketId", ticket->getParent()->getId());
        query.exec();
        int result = -1;
        if(query.lastError().type() == QSqlError::NoError)
        {
            result = ticket->getId();
            qDebug() << "Inserted ticket with id: " << ticket->getId();
            emit getSignals().preItemAppended();
            m_list.append(ticket);
            emit getSignals().postItemAppended();
        }
        else
            qCritical() << "Error inserting ticket: " << result;
        return result;
    }

    void TicketDataSource::move(int from, int to, Project *project)
    {
        Q_ASSERT(!m_list.empty());
        if(project == nullptr)
        {
            qWarning() << "Project shouldn't be null";
            return;
        }

        QList<Ticket*> ticketsToMove;
        for(auto *ticket : m_list)
        {
            if(ticket->getProject()->getId() == project->getId())
                ticketsToMove.push_back(ticket);
        }
        auto compare = [](Ticket *t1, Ticket *t2) {
            return t1->getPosition() < t2->getPosition();
        };
        std::sort(ticketsToMove.begin(), ticketsToMove.end(), compare);
        ticketsToMove.move(from, to);

        auto db = QSqlDatabase::database();
        db.transaction();
        QSqlQuery query(db);
        query.prepare("UPDATE Ticket SET position = ? WHERE id = ? AND projectId = ?");
        int index = 0;
        bool selectionChanged = false;
        for(auto* ticket : ticketsToMove)
        {
            if(!selectionChanged && m_selectedTicketIndex.row() == ticket->getPosition())
            {
                m_selectedTicketIndex = m_selectedTicketIndex.model()->index(index, m_selectedTicketIndex.column());
                selectionChanged = true;
            }
            ticket->setPosition(index);
            query.bindValue(0, index);
            query.bindValue(1, ticket->getId());
            query.bindValue(2, ticket->getProject()->getId());
            query.exec();
            ++index;
        }
        if(!db.commit()) {
            qCritical() << "Error updating ticket position: " << db.lastError();
            db.rollback();
        }
        auto compareWithProject = [](Ticket *t1, Ticket *t2) {
            if(t1->getProject()->getId() == t2->getProject()->getId())
                return t1->getPosition() < t2->getPosition();
            return t1->getProject()->getId() < t2->getProject()->getId();
        };
        std::sort(m_list.begin(), m_list.end(), compareWithProject);
        //m_list.move(from, to);
    }

    int TicketDataSource::getNextId(Project *project)
    {
        Q_ASSERT(project != nullptr);
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare("SELECT id FROM Ticket WHERE projectId = :projectId ORDER BY id DESC LIMIT 1;");
        query.bindValue(":projectId", project->getId());
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error fetching tickets for next id:" << error;
            return -1;
        }

        if(query.first())
            return query.value(0).toInt() + 1;

        return 1;
    }

    void TicketDataSource::resetFromDb(const int workspaceId)
    {
        emit getSignals().preItemsReset();
        m_list.clear();
        QSqlQuery query;
        if(workspaceId >= 0)
        {
            query.prepare("SELECT t.id, projectId, t.title, position, t.description, presentKanoFeel, "
                              "absentKanoFeel, storyPoints, dueDate, priority, kanbanColumn, kanbanPosition, "
                              "inNextSprint, sprintId FROM Ticket t "
                              "INNER JOIN Project p ON t.projectId = p.id "
                              "WHERE p.workspaceId = ? AND t.archived = 0 "
                              "ORDER BY projectId, position");
            query.bindValue(0, workspaceId);
        }
        else
            query.prepare("SELECT id, projectId, title, position, description, presentKanoFeel, absentKanoFeel, "
                               "storyPoints, dueDate, priority, kanbanColumn, kanbanPosition, inNextSprint, sprintId "
                               "FROM Ticket "
                               "WHERE archived = 0 ORDER BY projectId, position");
        query.exec();
        while(query.next())
        {
            int id = query.value(0).toInt();
            auto projectId = query.value(1).toString();
            Project* project = Synoptic::ProjectDataSource().get(projectId);
            QString title = query.value(2).toString();
            int position = query.value(3).toInt();
            QString description = query.value(4).toString();
            Ticket::KanoFeel presentKanoFeel = static_cast<Ticket::KanoFeel>(query.value(5).toInt());
            Ticket::KanoFeel absentKanoFeel = static_cast<Ticket::KanoFeel>(query.value(6).toInt());
            float storyPoint = query.value(7).toFloat();
            QDateTime dueDate = QDateTime::fromSecsSinceEpoch(query.value(8).toInt());
            Ticket::Priority priority = static_cast<Ticket::Priority>(query.value(9).toInt());
            int kanbanColumn = query.value(10).toInt();
            int kanbanPosition = query.value(11).toInt();
            bool inNextSprint = query.value(12).toBool();
            Ticket *ticket = new Ticket(this);
            ticket->setId(id);
            ticket->setProject(project);
            ticket->setTitle(title);
            ticket->setPosition(position);
            ticket->setDescription(description);
            ticket->setPresentKanoFeel(presentKanoFeel);
            ticket->setAbsentKanoFeel(absentKanoFeel);
            ticket->setStoryPoints(storyPoint);
            ticket->setDueDate(dueDate);
            ticket->setPriority(priority);
            ticket->setKanbanColumn(kanbanColumn);
            ticket->setKanbanPosition(kanbanPosition);
            ticket->setInNextSprint(inNextSprint);
            // TODO
//            if(!query.value(8).isNull)
//                ticket->setSprint();
            m_list.push_back(ticket);
        }
        updateInfo();
        emit getSignals().postItemsReset();
    }

    QSqlError TicketDataSource::archive(Synoptic::Ticket *ticket)
    {
        Q_ASSERT(!m_list.empty());
        Q_ASSERT(ticket->getProject() != nullptr);
        Ticket *ticketInList = *std::find_if(m_list.begin(), m_list.end(), [=](Ticket* t) {
            return t->getId() == ticket->getId() && t->getProject()->getId() == ticket->getProject()->getId();
        });
        Q_ASSERT(ticketInList != nullptr);

        int index = m_list.indexOf(ticketInList);
        emit getSignals().preItemRemoved(index);
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare(DbManager::ARCHIVE_TICKET_SQL);
        query.bindValue(":archived", 1);
        query.bindValue(":ticketId", ticketInList->getId());
        query.bindValue(":projectId", ticketInList->getProject()->getId());
        query.exec();
        auto result = query.lastError();
        if(result.type() != QSqlError::NoError)
            qCritical() << "Error archiving ticket:" << result;
        else
        {
            m_list.remove(index);
            emit getSignals().postItemRemoved();
        }
        return result;
    }

    QSqlError TicketDataSource::deleteTicket(Synoptic::Ticket *ticket)
    {
        Q_ASSERT(!m_list.empty());
        Q_ASSERT(ticket->getProject() != nullptr);
        Ticket *ticketInList = *std::find_if(m_list.begin(), m_list.end(), [=](Ticket* t) {
            return t->getId() == ticket->getId() && t->getProject()->getId() == ticket->getProject()->getId();
        });
        Q_ASSERT(ticketInList != nullptr);
        int index = m_list.indexOf(ticketInList);
        emit getSignals().preItemRemoved(index);
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare(DbManager::DELETE_TICKET_SQL);
        query.bindValue(":ticketId", ticketInList->getId());
        query.bindValue(":projectId", ticketInList->getProject()->getId());
        query.exec();
        auto result = query.lastError();
        if(result.type() != QSqlError::NoError)
            qCritical() << "Error deleting ticket:" << result;
        else
        {
            m_list.remove(index);
            emit getSignals().postItemRemoved();
        }
        return result;
    }

    bool TicketDataSource::setKanbanPosition(const QString &projectId, int ticketId, int position)
    {
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare("UPDATE Ticket SET kanbanPosition = :kanbanPosition WHERE projectId = :projectId AND id = :ticketId;");
        query.bindValue(":kanbanPosition", position);
        query.bindValue(":projectId", projectId);
        query.bindValue(":ticketId", ticketId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
            qCritical() << "Error setting kanban position:" << error;
        else
        {
            auto it = std::find_if(m_list.begin(), m_list.end(), [=](Ticket *t) {
                return t->getId() == ticketId && t->getProject()->getId() == projectId;
            });
            m_list[it - m_list.begin()]->setKanbanPosition(position);
        }
        return error.type() == QSqlError::NoError;
    }

    int TicketDataSource::removeCompletedFromList()
    {
        int result = 0;
        for(int i = 0; i < m_list.count();)
        {
            Ticket* ticket = m_list[i];
            if(ticket->getKanbanColumn() != 3)
            {
                ++i;
                continue;
            }

            emit getSignals().preItemRemoved(i);
            m_list.removeAt(i);
            ++result;
            emit getSignals().postItemRemoved();
        }
        return result;
    }

    void TicketDataSource::beginTransaction()
    {
        auto db = QSqlDatabase::database();
        if(!db.transaction())
            qCritical() << "Error starting transaction: " << db.lastError();
    }

    void TicketDataSource::endTransaction()
    {
        auto db = QSqlDatabase::database();
        if(!db.commit())
        {
            qCritical() << "Error commiting transaction: " << db.lastError();
            db.rollback();
        }
    }

    void TicketDataSource::rollback()
    {
        auto db = QSqlDatabase::database();
        db.rollback();
    }

    TicketDataSourceSignals &TicketDataSource::getSignals()
    {
        static TicketDataSourceSignals sig {};
        return sig;
    }
} // namespace Synoptic
