#pragma once

#include <QObject>
#include <QtQml/qqmlregistration.h>
#include <QAbstractListModel>
#include "TimeEntryDataSource.h"

namespace Synoptic
{
    class TimeEntryModel : public QAbstractListModel
    {
        Q_OBJECT

        Q_PROPERTY(TimeEntryDataSource *dataSource READ getDataSource WRITE setDataSource NOTIFY dataSourceChanged)
        Q_PROPERTY(int ticketId READ getTicketId WRITE setTicketId NOTIFY ticketIdChanged)
        Q_PROPERTY(QString ticketProjectId READ getTicketProjectId WRITE setTicketProjectId NOTIFY ticketProjectIdChanged)
        Q_PROPERTY(int totalTime READ getTotalTime NOTIFY totalTimeChanged)
        QML_ELEMENT

    public:
        enum TimeEntryRoles
        {
            IdRole = Qt::UserRole,
            TicketIdRole,
            TicketProjectIdRole,
            BeginDateRole,
            DurationRole,
        };
        Q_ENUM(TimeEntryRoles)

        explicit TimeEntryModel(QObject *parent = nullptr);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        QHash<int, QByteArray> roleNames() const override;

        Q_INVOKABLE virtual void update();

        TimeEntryDataSource* getDataSource() const;
        void setDataSource(TimeEntryDataSource *newDataSource);

        int getTicketId() const;
        void setTicketId(int newTicketId);

        QString getTicketProjectId() const;
        void setTicketProjectId(const QString &newTicketProjectId);

        int getTotalTime() const;

        QList<TimeEntry>& getItems();
        void setItems(const QList<TimeEntry> &newList);

    signals:
        void dataSourceChanged();
        void ticketIdChanged();
        void ticketProjectIdChanged();
        void totalTimeChanged();

    private:
        TimeEntryDataSource *m_dataSource;
        int m_ticketId;
        QString m_ticketProjectId;
        QList<TimeEntry> m_items;
    };

    class TimeEntryByDateModel : public TimeEntryModel
    {
        Q_OBJECT
        Q_PROPERTY(QDateTime date READ getDate WRITE setDate NOTIFY dateChanged)
        QML_ELEMENT

    public:
        explicit TimeEntryByDateModel(QObject *parent = nullptr);

        Q_INVOKABLE void update() override;

        QDateTime getDate() const;
        void setDate(const QDateTime &newDate);

    signals:
        void dateChanged();

    private:
        QDateTime m_date;
    };
} // Synoptic
