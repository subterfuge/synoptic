#include "DefinitionOfDoneModel.h"

namespace Synoptic
{
    int TicketDefinitionOfDoneModel::rowCount(const QModelIndex &parent) const
    {
        if(parent.isValid() || (m_dataSource == nullptr))
            return 0;

        return m_items.count();
    }

    QVariant TicketDefinitionOfDoneModel::data(const QModelIndex &index, int role) const
    {
        if(!checkIndex(index, CheckIndexOption::IndexIsValid))
            return QVariant();

        switch(role)
        {
            case IdRole:
                return m_items[index.row()].id;
            case DescriptionRole:
                return m_items[index.row()].description;
            case CheckedRole:
                return m_items[index.row()].checked;
            default:
                return QVariant();
        }
    }

    bool TicketDefinitionOfDoneModel::setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if(m_dataSource == nullptr)
            return false;

        if(data(index, role) != value)
        {
            bool changed = false;
            auto& item = m_items[index.row()];
            switch(role)
            {
                case DescriptionRole:
                    m_items[index.row()].description = value.toString();
                    changed = true;
                    break;
                case CheckedRole:
                    m_items[index.row()].checked = value.toBool();
                    changed = true;
                    break;
            }

            if(changed)
            {
                m_dataSource->update(item.id, item.description, item.checked);
                emit dataChanged(index, index, {role});
            }
            return changed;
        }
        return false;
    }

    QHash<int, QByteArray> TicketDefinitionOfDoneModel::roleNames() const
    {
        static const QHash<int, QByteArray> roles{
                {IdRole,          "id"},
                {DescriptionRole, "description"},
                {CheckedRole,     "checked"},
        };
        return roles;
    }

    bool TicketDefinitionOfDoneModel::anyChecked() const
    {
        return std::find_if(m_items.begin(), m_items.end(), [](DefinitionOfDoneItem it) { return it.checked; })
                != m_items.end();
    }

    void TicketDefinitionOfDoneModel::update()
    {
        if(m_dataSource == nullptr || m_projectId.isEmpty() || m_ticketId == -1)
            return;

        m_items = m_dataSource->get(m_projectId, m_ticketId);
    }

    int TicketDefinitionOfDoneModel::getTicketId() const
    {
        return m_ticketId;
    }

    void TicketDefinitionOfDoneModel::setTicketId(int newTicketId)
    {
        if(newTicketId == m_ticketId)
            return;

        m_ticketId = newTicketId;
        update();
        emit ticketIdChanged();
    }

    const QString& TicketDefinitionOfDoneModel::getProjectId() const
    {
        return m_projectId;
    }

    void TicketDefinitionOfDoneModel::setProjectId(const QString &newProjectId)
    {
        if(newProjectId == m_projectId)
            return;

        m_projectId = newProjectId;
        update();
        emit projectIdChanged();
    }

    DefinitionOfDoneDataSource *TicketDefinitionOfDoneModel::getDataSource() const
    {
        return m_dataSource;
    }

    void TicketDefinitionOfDoneModel::setDataSource(DefinitionOfDoneDataSource *newDataSource)
    {
        if(m_dataSource == newDataSource)
            return;

        beginResetModel();
        m_dataSource = newDataSource;
        update();
        if(m_dataSource != nullptr)
        {
            connect(m_dataSource->getSignals(), &DefinitionOfDoneSignals::ticketDodItemAppended, this, [this](int
                    ticketId, const QString& projectId)
            {
                if(m_ticketId != ticketId || m_projectId != projectId)
                    return;

                beginResetModel();
                update();
                endResetModel();
            });
            connect(m_dataSource->getSignals(), &DefinitionOfDoneSignals::ticketDodItemChanged, this, [this](int id)
            {
                auto it = std::find_if(m_items.begin(), m_items.end(), [=](DefinitionOfDoneItem& item)
                        { return id == item.id; });
                if(it == m_items.end())
                    return;

                auto item = Synoptic::DefinitionOfDoneDataSource::getItem(it->id);
                it->description = item.description;
                it->checked = item.checked;
                int row = std::distance(m_items.begin(), it);
                auto modelIndex = index(row, 0);
                emit dataChanged(modelIndex, modelIndex, {DescriptionRole, CheckedRole});
            });
            connect(m_dataSource->getSignals(), &DefinitionOfDoneSignals::ticketDodUpdated, this,
                    [this](int ticketId, const QString& projectId)
            {
                if(m_ticketId != ticketId || m_projectId != projectId)
                    return;

                beginResetModel();
                update();
                endResetModel();
            });
        }
        emit dataSourceChanged();
        endResetModel();
    }

} // Synoptic
