#include "SprintDataSource.h"

#include <QSqlQuery>
#include <QSqlDriver>
#include "WorkspaceDataSource.h"
#include "DbManager.h"
#include "Utils.h"

namespace Synoptic
{
    SprintDataSource::SprintDataSource(QObject *parent)
            : QObject{parent}
    {}

    Sprint *SprintDataSource::get(const int id)
    {
        QSqlQuery query;
        query.prepare(DbManager::SELECT_SPRINT_SQL);
        query.bindValue(0, id);
        query.exec();

        if(!query.first())
            return nullptr;
        Workspace *workspace = getWorkspaceDataSource().get(query.value(6).toInt());
        if(workspace == nullptr)
            return nullptr;

        bool closed = query.value(1).toBool();
        QString sprintGoal = query.value(2).toString();
        QDateTime startDate = QDateTime::fromSecsSinceEpoch(query.value(3).toLongLong());
        QDateTime endDate = QDateTime::fromSecsSinceEpoch(query.value(4).toLongLong());
        QString productGoal = query.value(5).toString();
        Sprint *sprint = new Sprint(this);
        sprint->setId(id);
        sprint->setClosed(closed);
        sprint->setSprintGoal(sprintGoal);
        sprint->setStartDate(startDate);
        sprint->setEndDate(endDate);
        sprint->setProductGoal(productGoal);
        sprint->setWorkspace(workspace);
        return sprint;
    }

    QList<Sprint *> SprintDataSource::getAll(int workspaceId, bool onlyOpen)
    {
        QSqlQuery query;
        if(onlyOpen)
            query.prepare(DbManager::SELECT_ALL_OPEN_SPRINT_SQL);
        else
            query.prepare(DbManager::SELECT_ALL_SPRINT_SQL);
        query.bindValue(":workspaceId", workspaceId);
        query.exec();

        QList<Sprint *> sprints;
        while(query.next())
        {
            Workspace *workspace = getWorkspaceDataSource().get(query.value(6).toInt());
            if(workspace == nullptr)
                continue;
            Sprint *sprint = new Sprint(this);
            sprint->setId(query.value(0).toInt());
            sprint->setClosed(query.value(1).toBool());
            sprint->setSprintGoal(query.value(2).toString());
            sprint->setStartDate(QDateTime::fromSecsSinceEpoch(query.value(3).toLongLong()));
            sprint->setEndDate(QDateTime::fromSecsSinceEpoch(query.value(4).toLongLong()));
            sprint->setProductGoal(query.value(5).toString());
            sprint->setWorkspace(workspace);
            sprints.append(sprint);
        }
        return sprints;
    }

    int SprintDataSource::insert(const QString &sprintGoal, const QDateTime &startDate, const QDateTime &endDate,
            const QString &productGoal, int workspaceId)
    {
        QSqlQuery query;
        query.prepare(DbManager::INSERT_SPRINT_SQL);
        query.bindValue(":closed", false);
        query.bindValue(":sprintGoal", sprintGoal);
        query.bindValue(":startDate", startDate.toSecsSinceEpoch());
        query.bindValue(":endDate", endDate.toSecsSinceEpoch());
        query.bindValue(":productGoal", productGoal);
        query.bindValue(":workspaceId", workspaceId);
        emit preItemAppended();
        query.exec();
        int result = -1;
        emit postItemAppended();
        if(query.lastError().type() != QSqlError::NoError)
            qCritical() << query.lastError().text();
        else
        {
            qDebug() << "Sprint inserted"
                    << (query.driver()->hasFeature(QSqlDriver::LastInsertId) ?
                            "(" + query.lastInsertId().toString() + ")" : "");
            result = query.lastInsertId().toInt();
        }
        return result;
    }

    bool SprintDataSource::update(int id, bool closed,
            const QString &sprintGoal, const QDateTime &startDate,
            const QDateTime &endDate, const QString &productGoal, int workspaceId)
    {
        QSqlQuery query;
        query.prepare(DbManager::UPDATE_SPRINT_SQL);
        query.bindValue(":closed", closed);
        query.bindValue(":sprintGoal", sprintGoal);
        query.bindValue(":startDate", startDate.toSecsSinceEpoch());
        query.bindValue(":endDate", endDate.toSecsSinceEpoch());
        query.bindValue(":productGoal", productGoal);
        query.bindValue(":workspaceId", workspaceId);
        query.bindValue(":id", id);
        bool result = query.exec();
        if(query.lastError().type() != QSqlError::NoError)
            qCritical() << query.lastError().text();
        else
            qDebug() << "Sprint updated (" << id << ")";
        return result;
    }

    WorkspaceDataSource &SprintDataSource::getWorkspaceDataSource()
    {
        static WorkspaceDataSource workspaceDataSource;
        return workspaceDataSource;
    }

    bool SprintDataSource::closeSprint(int id)
    {
        QSqlDatabase db = QSqlDatabase::database();
        if(!db.transaction())
            return false;

        QSqlQuery query(db);
        query.prepare(DbManager::CLOSE_SPRINT_SQL);
        query.bindValue(":id", id);
        query.exec();
        if(query.lastError().type() != QSqlError::NoError)
        {
            db.rollback();
            qCritical() << query.lastError().text();
            return false;
        }

        query.prepare(DbManager::ARCHIVE_COMPLETED_TICKETS_SQL);
        query.exec();
        if(query.lastError().type() != QSqlError::NoError)
        {
            db.rollback();
            qCritical() << query.lastError().text();
            return false;
        }

        if(!db.commit())
        {
            db.rollback();
            qCritical() << "Couldn't commit sprint closing:" << db.lastError();
            return false;
        }

        Synoptic::TicketDataSource tds {};
        tds.removeCompletedFromList();
        Utils::getInstance().getSettings().setCurrentSprint(0);
        qDebug() << "Sprint closed (" << id << ")";
        return true;
    }

} // namespace Synoptic
