#include "Project.h"

namespace Synoptic
{
    Project::Project(QObject *parent)
        : QObject{parent}
    {}

    QString Project::getId() const
    {
        return m_id;
    }

    void Project::setId(const QString &newId)
    {
        if(m_id == newId)
            return;
        m_id = newId;
        emit idChanged();
    }

    QString Project::getTitle() const
    {
        return m_title;
    }

    void Project::setTitle(const QString &newTitle)
    {
        if(m_title == newTitle)
            return;
        m_title = newTitle;
        emit titleChanged();
    }

    QDateTime Project::getCreationDate() const
    {
        return m_creationDate;
    }

    void Project::setCreationDate(const QDateTime &newCreationDate)
    {
        if(m_creationDate == newCreationDate)
            return;
        m_creationDate = newCreationDate;
        emit creationDateChanged();
    }

    QDateTime Project::getLastTimeUsed() const
    {
        return m_lastTimeUsed;
    }

    void Project::setLastTimeUsed(const QDateTime &newLastTimeUsed)
    {
        if(m_lastTimeUsed == newLastTimeUsed)
            return;
        m_lastTimeUsed = newLastTimeUsed;
        emit lastTimeUsedChanged();
    }

    QString Project::getDescription() const
    {
        return m_description;
    }

    void Project::setDescription(const QString &newDescription)
    {
        if (m_description == newDescription)
            return;
        m_description = newDescription;
        emit descriptionChanged();
    }

    Workspace *Project::getWorkspace() const
    {
        return m_workspace;
    }

    void Project::setWorkspace(Workspace *newWorkspace)
    {
        if(m_workspace == newWorkspace)
            return;
        m_workspace = newWorkspace;
        emit workspaceChanged();
    }

} // namespace Synoptic
