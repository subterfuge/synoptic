

#include "WindowsFramelessHelper.h"

#include <QQuickWindow>

#ifdef Q_OS_WIN

#include <QPoint>
#include <QScreen>
#include <QSize>
#include <QCoreApplication>
#include <windowsx.h>
#include <windows.h>

#endif

namespace Synoptic
{

    WindowsFramelessHelper::WindowsFramelessHelper(QObject *parent)
        : QObject(parent)
    {}

    WindowsFramelessHelper::~WindowsFramelessHelper()
    {
        NativeEventFilter::helpers.remove(mQuickWindow->winId());
    }

    QQuickItem *WindowsFramelessHelper::titlebar() const
    {
        return mTitlebar;
    }

    void WindowsFramelessHelper::setTitlebar(QQuickItem *titlebar)
    {
        if (mTitlebar != titlebar) {
            mTitlebar = titlebar;
            emit titlebarChanged();
        }
    }

    qreal WindowsFramelessHelper::borderWidth() const
    {
        return mBorderWidth;
    }

    void WindowsFramelessHelper::setBorderWidth(qreal borderWidth)
    {
        if (mBorderWidth != borderWidth) {
            mBorderWidth = borderWidth;
            emit borderWidthChanged();
        }
    }

    bool WindowsFramelessHelper::containAcceptMouseButtons(QQuickItem *item, qreal x, qreal y)
    {
        if (item->acceptedMouseButtons() != Qt::NoButton)
            return true;
        QPointF pos = item->mapToGlobal(QPoint(0, 0));
        int rx = x - pos.x();
        int ry = y - pos.y();

        QQuickItem *child = item->childAt(rx, ry);
        return child ? containAcceptMouseButtons(child, x, y) : false;
    }

#ifdef Q_OS_WIN

    bool WindowsFramelessHelper::nativeEventFilter(const QByteArray &eventType, void *message, qintptr *result)
    {
        Q_UNUSED(eventType)
    #if (QT_VERSION == QT_VERSION_CHECK(5, 11, 1))
        MSG* msg = *reinterpret_cast<MSG**>(message);
    #else
        MSG* msg = reinterpret_cast<MSG*>(message);
    #endif
        HWND hwnd = reinterpret_cast<HWND>(mQuickWindow->winId());

        switch (msg->message) {
            case WM_NCHITTEST: {
                *result = 0;

                const LONG border_width = mBorderWidth;
                RECT winrect;
                GetWindowRect(hwnd, &winrect);

                long x = GET_X_LPARAM(msg->lParam);
                long y = GET_Y_LPARAM(msg->lParam);

                if (mQuickWindow->visibility() == QWindow::Windowed) {
                    bool resizeWidth = mQuickWindow->minimumWidth() != mQuickWindow->maximumWidth();
                    bool resizeHeight = mQuickWindow->minimumHeight() != mQuickWindow->maximumHeight();

                    if(resizeWidth)
                    {
                        //left border
                        if (x >= winrect.left && x < winrect.left + border_width)
                        {
                            *result = HTLEFT;
                        }
                        //right border
                        if (x < winrect.right && x >= winrect.right - border_width)
                        {
                            *result = HTRIGHT;
                        }
                    }
                    if(resizeHeight)
                    {
                        //bottom border
                        if (y < winrect.bottom && y >= winrect.bottom - border_width)
                        {
                            *result = HTBOTTOM;
                        }
                        //top border
                        if (y >= winrect.top && y < winrect.top + border_width)
                        {
                            *result = HTTOP;
                        }
                    }
                    if(resizeWidth && resizeHeight)
                    {
                        //bottom left corner
                        if (x >= winrect.left && x < winrect.left + border_width &&
                            y < winrect.bottom && y >= winrect.bottom - border_width)
                        {
                            *result = HTBOTTOMLEFT;
                        }
                        //bottom right corner
                        if (x < winrect.right && x >= winrect.right - border_width &&
                            y < winrect.bottom && y >= winrect.bottom - border_width)
                        {
                            *result = HTBOTTOMRIGHT;
                        }
                        //top left corner
                        if (x >= winrect.left && x < winrect.left + border_width &&
                            y >= winrect.top && y < winrect.top + border_width)
                        {
                            *result = HTTOPLEFT;
                        }
                        //top right corner
                        if (x < winrect.right && x >= winrect.right - border_width &&
                            y >= winrect.top && y < winrect.top + border_width)
                        {
                            *result = HTTOPRIGHT;
                        }
                    }
                    if (0!=*result)
                        return true;
                }

                if (mTitlebar == nullptr)
                    return false;

                QPointF tbPos = mTitlebar->mapToGlobal(QPoint(0, 0));
                if (mQuickWindow->visibility() != QWindow::FullScreen &&
                    tbPos.y() <= y && tbPos.y() + mTitlebar->height() >= y &&
                    tbPos.x() <= x && tbPos.x() + mTitlebar->width() >= x &&
                    !containAcceptMouseButtons(mTitlebar, x, y))
                {
                    *result = HTCAPTION;
                    return true;
                }

                break;
            }
            case WM_NCCALCSIZE:
            {
                NCCALCSIZE_PARAMS& params = *reinterpret_cast<NCCALCSIZE_PARAMS*>(msg->lParam);
                WINDOWPLACEMENT wp;
                GetWindowPlacement(hwnd, &wp);
                if (params.rgrc[0].top != 0)
                    params.rgrc[0].top -= 1;
                if(wp.showCmd == SW_SHOWMAXIMIZED)
                {
                    params.rgrc[0].top += 8;
                    params.rgrc[0].left += 8;
                    params.rgrc[0].right -= 8;
                    params.rgrc[0].bottom -= 8;
                }
                *result = WVR_REDRAW;
                return true;
            }
        }

        return false;
    }

    void WindowsFramelessHelper::classBegin()
    {

    }

    void WindowsFramelessHelper::componentComplete()
    {
        auto obj = parent();
        while (obj != nullptr) {
            if (obj->inherits("QQuickRootItem")) {
                auto rootItem = qobject_cast<QQuickItem *>(obj);
                mQuickWindow = qobject_cast<QQuickWindow *>(rootItem->window());
                if (mQuickWindow != nullptr) {
                    NativeEventFilter::deliver(mQuickWindow, this);
                    connect(mQuickWindow, &QWindow::screenChanged, this, [this](){
                        SetWindowPos((HWND) mQuickWindow->winId(), NULL, 0, 0, 0, 0,
                                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                                     SWP_NOOWNERZORDER | SWP_FRAMECHANGED | SWP_NOACTIVATE);
                    });
                    break;
                }
            }
            obj = obj->parent();
        }
    }

    bool WindowsFramelessHelper::NativeEventFilter::nativeEventFilter(const QByteArray &eventType, void *message, qintptr *result)
    {
        Q_UNUSED(eventType)
        auto lpMsg = (LPMSG)message;

        auto wid = reinterpret_cast<WId>(lpMsg->hwnd);

        if (auto helper = helpers.value(wid)) {
            return helper->nativeEventFilter(eventType, message, result);
        }

        return false;
    }

    void WindowsFramelessHelper::NativeEventFilter::deliver(QQuickWindow *window, WindowsFramelessHelper *helper)
    {
        if (instance == nullptr) {
            instance = new NativeEventFilter;
            if (instance) qApp->installNativeEventFilter(instance);
        }
        if (window != nullptr && helper != nullptr) {
            auto wid = window->winId();
            if (!helpers.contains(wid)) {
                auto hwnd = reinterpret_cast<HWND>(wid);
                // set new window style
                DWORD oldStyle = GetWindowLong(hwnd, GWL_STYLE);
                SetWindowLong(hwnd, GWL_STYLE, oldStyle | WS_CAPTION | WS_POPUP
                                               | WS_MINIMIZEBOX |  WS_MAXIMIZEBOX | WS_THICKFRAME);
                helpers.insert(wid, helper);
            }
        }

        if (window != nullptr && helper == nullptr) {
            helpers.remove(window->winId());
        }
    }

    WindowsFramelessHelper::NativeEventFilter *WindowsFramelessHelper::NativeEventFilter::instance = nullptr;
    QHash<WId, WindowsFramelessHelper*> WindowsFramelessHelper::NativeEventFilter::helpers;

    #else
    void FramelessHelper::classBegin()
    {

    }

    void FramelessHelper::componentComplete()
    {

    }

    void FramelessHelper::NativeEventFilter::deliver(QQuickWindow *window, FramelessHelper *helper)
    {
        Q_UNUSED(window)
        Q_UNUSED(helper)
    }

    bool FramelessHelper::NativeEventFilter::nativeEventFilter(const QByteArray &eventType, void *message, long *result)
    {
        Q_UNUSED(eventType)
        Q_UNUSED(message)
        Q_UNUSED(result)
        return false;
    }

    bool FramelessHelper::nativeEventFilter(const QByteArray &eventType, void *message, long *result)
    {
        Q_UNUSED(eventType)
        Q_UNUSED(message)
        Q_UNUSED(result)
        return false;
    }

    #endif
}
