#include "Utils.h"

namespace Synoptic
{

    Utils::Utils(QObject *parent)
        : QObject(parent)
    {}

    Utils& Utils::getInstance()
    {
        static Utils instance;
        return instance;
    }

    WorkspaceModel& Utils::getWorkspaceModel()
    {
        return m_workspaceModel;
    }

    ProjectModel &Utils::getProjectModel()
    {
        return m_projectModel;
    }

    CalendarEventDataSource& Utils::getCalendarEventDataSource()
    {
        return m_calendarEventDataSource;
    }

    void Utils::resetDataSources()
    {
        const int workspaceId = getSettings().getWorkspace();
        getProjectModel().getDataSource()->resetFromDb(workspaceId);
        TicketDataSource tds {};
        tds.resetFromDb(workspaceId);
    }

    Synoptic::Settings& Utils::getSettings()
    {
        return m_settings;
    }

    QQmlApplicationEngine* Utils::getEngine() const
    {
        return m_engine;
    }

    void Utils::setEngine(QQmlApplicationEngine *engine)
    {
        m_engine = engine;
    }

} // Synoptic
