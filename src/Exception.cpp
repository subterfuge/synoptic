#include "Exception.h"

namespace Synoptic
{
    BaseException::BaseException(std::string message)
        : m_message(std::move(message))
    {}

    const char* BaseException::what() const noexcept
    {
        return m_message.c_str();
    }

    ArgumentException::ArgumentException(std::string message)
        : BaseException(std::move(message))
    {}

} // namespace Synoptic


