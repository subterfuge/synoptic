#include "TimeEntryModel.h"

namespace Synoptic
{
    TimeEntryModel::TimeEntryModel(QObject *parent)
        : QAbstractListModel(parent), m_dataSource(nullptr)
    {}

    int TimeEntryModel::rowCount(const QModelIndex &parent) const
    {
        if(parent.isValid() || (m_dataSource == nullptr))
            return 0;

        return m_items.count();
    }

    QVariant TimeEntryModel::data(const QModelIndex &index, int role) const
    {
        if(!checkIndex(index, CheckIndexOption::IndexIsValid))
            return QVariant();

        switch(role)
        {
            case IdRole:
                return m_items[index.row()].id;
            case TicketIdRole:
                return m_items[index.row()].ticketId;
            case TicketProjectIdRole:
                return m_items[index.row()].ticketProjectId;
            case BeginDateRole:
                return m_items[index.row()].beginDate;
            case DurationRole:
                return m_items[index.row()].duration;
            default:
                return QVariant();
        }
    }

    QHash<int, QByteArray> TimeEntryModel::roleNames() const
    {
        static const QHash<int, QByteArray> roles {
                { IdRole, "id" },
                { TicketIdRole, "ticketId" },
                { TicketProjectIdRole, "ticketProjectId" },
                { BeginDateRole, "beginDate" },
                { DurationRole, "duration" },
        };
        return roles;
    }

    void TimeEntryModel::update()
    {
        if(m_dataSource == nullptr)
        {
            m_items.clear();
            return;
        }

        beginResetModel();
        m_items = m_dataSource->get(m_ticketId, m_ticketProjectId);
        emit totalTimeChanged();
        endResetModel();
    }

    TimeEntryDataSource *TimeEntryModel::getDataSource() const
    {
        return m_dataSource;
    }

    void TimeEntryModel::setDataSource(TimeEntryDataSource *newDataSource)
    {
        if(m_dataSource == newDataSource)
            return;

        beginResetModel();
        m_dataSource = newDataSource;
        connect(TimeEntryDataSource::getSignals(), &TimeEntrySignals::preReset, this, [this]()
        {
            beginResetModel();
        });
        connect(TimeEntryDataSource::getSignals(), &TimeEntrySignals::postReset, this, [this]()
        {
            update();
            endResetModel();
        });
        endResetModel();
    }

    int TimeEntryModel::getTicketId() const
    {
        return m_ticketId;
    }

    void TimeEntryModel::setTicketId(int newTicketId)
    {
        if(m_ticketId == newTicketId)
            return;

        m_ticketId = newTicketId;
        emit ticketIdChanged();
    }

    QString TimeEntryModel::getTicketProjectId() const
    {
        return m_ticketProjectId;
    }

    void TimeEntryModel::setTicketProjectId(const QString &newTicketProjectId)
    {
        if(m_ticketProjectId == newTicketProjectId)
            return;

        m_ticketProjectId = newTicketProjectId;
        emit ticketProjectIdChanged();
    }

    int TimeEntryModel::getTotalTime() const
    {
        int result = 0;
        for(const auto &item : m_items)
        {
            result += item.duration;
        }
        return result;
    }

    QList<TimeEntry>& TimeEntryModel::getItems()
    {
        return m_items;
    }

    void TimeEntryModel::setItems(const QList<TimeEntry> &newList)
    {
        m_items = newList;
    }

    TimeEntryByDateModel::TimeEntryByDateModel(QObject *parent)
        : TimeEntryModel(parent)
    {}

    void TimeEntryByDateModel::update()
    {
        if(getDataSource() == nullptr)
        {
            getItems().clear();
            return;
        }

        beginResetModel();
        setItems(getDataSource()->getByDay(m_date));
        emit totalTimeChanged();
        endResetModel();
    }

    QDateTime TimeEntryByDateModel::getDate() const
    {
        return m_date;
    }

    void TimeEntryByDateModel::setDate(const QDateTime &newDate)
    {
        if(m_date == newDate)
            return;

        m_date = newDate;
        update();
        emit dateChanged();
    }
} // Synoptic
