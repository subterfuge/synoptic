#include "ProjectDataSource.h"

#include <QSqlQuery>
#include <QRegularExpression>
#include "DbManager.h"
#include "Utils.h"

namespace Synoptic
{

    ProjectDataSource::ProjectDataSource(QObject *parent)
        : QObject{parent}
    {
        if(!m_init)
        {
            init();
            m_init = true;
        }
    }

    void ProjectDataSource::init()
    {
        resetFromDb(Utils::getInstance().getSettings().getWorkspace());
    }

    void ProjectDataSource::resetFromDb(const int workspaceId)
    {
        emit preItemsReset();
        m_list.clear();
        QSqlQuery query;
        if(workspaceId >= 0)
        {
            query.prepare("SELECT p.id, p.title, creationDate, lastTimeUsed, "
                              "description "
                              "FROM Project p "
                              "INNER JOIN Workspace w ON p.workspaceId = w.id "
                              "WHERE w.id = ? "
                              "ORDER BY lastTimeUsed DESC");
            query.bindValue(0, workspaceId);
        }
        else
            query.prepare("SELECT id, title, creationDate, lastTimeUsed, "
                               "description FROM Project ORDER BY lastTimeUsed DESC");
        query.exec();
        while(query.next())
        {
            QString id = query.value(0).toString();
            QString title = query.value(1).toString();
            QDateTime creationDate = QDateTime::fromSecsSinceEpoch(query.value(2).toInt());
            QDateTime lastTimeUsed = QDateTime::fromSecsSinceEpoch(query.value(3).toInt());
            QString description = query.value(4).toString();
            Project *project = new Project(this);
            project->setId(id);
            project->setTitle(title);
            project->setCreationDate(creationDate);
            project->setLastTimeUsed(lastTimeUsed);
            project->setDescription(description);
            m_list.push_back(project);
        }
        emit postItemsReset();
    }

    QList<Project*>& ProjectDataSource::getAll()
    {
        return m_list;
    }

    Project* ProjectDataSource::get(const QString &id)
    {
        for(auto* project : getAll())
        {
            if(project == nullptr)
                continue;
            if(project->getId() == id)
                return project;
        }
        return nullptr;
    }

    ProjectDataSource::ProjectInsertError ProjectDataSource::insert(
            const QString &id, const QString &title, const QString &description,
            const int workspaceId)
    {
        QRegularExpression idCheckRe("^[A-Z0-9][A-Z0-9_]{0,3}$");
        if(!idCheckRe.match(id).hasMatch())
            return ProjectInsertError::IdMalformed;

        auto idEqualPredicate = [=](Project* p) {
            return p->getId() ==  id;
        };
        if(std::find_if(m_list.begin(), m_list.end(), idEqualPredicate) != m_list.end())
            return ProjectInsertError::IdAlreadyExists;

        QDateTime now = QDateTime::currentDateTime();
        QSqlQuery query;
        query.prepare(DbManager::INSERT_PROJECT_SQL);
        query.bindValue(":id", id);
        query.bindValue(":title", title);
        query.bindValue(":creationDate", now.toSecsSinceEpoch());
        query.bindValue(":lastTimeUsed", now.toSecsSinceEpoch());
        query.bindValue(":description", description);
        query.bindValue(":workspaceId", workspaceId);
        query.exec();
        auto result = query.lastError();
        if(result.type() != QSqlError::NoError)
        {
            qCritical() << "Error inserting project: " << result;
            return ProjectInsertError::DbError;
        }

        qDebug() << "Inserted project with id: " << id;
        Project *p = new Project(this);
        p->setId(id);
        p->setTitle(title);
        p->setCreationDate(now);
        p->setLastTimeUsed(now);
        p->setDescription(description);
        emit preItemAppended();
        m_list.append(p);
        emit postItemAppended();
        return ProjectInsertError::Ok;
    }

    QSqlError ProjectDataSource::update(const int index, Project *project)
    {
        QSqlQuery query;
        query.prepare(DbManager::UPDATE_PROJECT_SQL);
        query.bindValue(":id", project->getId());
        query.bindValue(":title", project->getTitle());
        query.bindValue(":lastTimeUsed", project->getLastTimeUsed().toSecsSinceEpoch());
        query.bindValue(":description", project->getDescription());
        m_list[index] = project;
        query.exec();
        return query.lastError();
    }

} // namespace Synoptic
