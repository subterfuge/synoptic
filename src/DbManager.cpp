#include "DbManager.h"

#include <QtSql>
#include "Utils.h"

namespace Synoptic {
    DbManager::DbManager()
            : m_db(QSqlDatabase::addDatabase("QSQLITE")) {
        m_db.setDatabaseName(dbName);
    }

    QSqlError DbManager::init() {
        if (!m_db.open())
            return m_db.lastError();

        QStringList tablesNames = m_db.tables();
        QSqlQuery query = QSqlQuery(m_db);
        if(!tablesNames.contains(WORKSPACE_TABLE_NAME))
        {
            if(!query.exec(CREATE_WORKSPACE_SQL))
                return query.lastError();
            if(!query.exec(INSERT_DEFAULT_WORKSPACE_SQL))
                return query.lastError();
            Utils::getInstance().getSettings().setWorkspace(1);
        }
        if (!tablesNames.contains(SPRINT_TABLE_NAME))
        {
            if(!query.exec(CREATE_SPRINT_SQL))
                return query.lastError();
        }
        if (!tablesNames.contains(PROJECT_TABLE_NAME))
        {
            if(!query.exec(CREATE_PROJECT_SQL))
                return query.lastError();
        }
        if (!tablesNames.contains(TICKET_TABLE_NAME))
        {
            if(!query.exec(CREATE_TICKET_SQL))
                return query.lastError();
        }
        if(!tablesNames.contains(TIME_ENTRY_TABLE_NAME))
        {
            if(!query.exec(CREATE_TIME_ENTRY_SQL))
                return query.lastError();
        }
        if (!tablesNames.contains(HISTORY_TABLE_NAME))
        {
            if(!query.exec(CREATE_HISTORY_SQL))
                return query.lastError();
        }
        if(!tablesNames.contains(PROJECT_DEFINITION_OF_DONE_TABLE_NAME))
        {
            if(!query.exec(CREATE_PROJECT_DEFINITION_OF_DONE_SQL))
                return query.lastError();
        }
        if(!tablesNames.contains(TICKET_DEFINITION_OF_DONE_TABLE_NAME))
        {
            if(!query.exec(CREATE_TICKET_DEFINITION_OF_DONE_SQL))
                return query.lastError();
        }

        return QSqlError();
    }
}
