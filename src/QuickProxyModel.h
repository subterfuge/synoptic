#pragma once

#include <QObject>
#include <QQmlEngine>
#include <QSortFilterProxyModel>

namespace Synoptic
{
    class RoleFilter : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(int role READ getRole WRITE setRole NOTIFY roleChanged)
        Q_PROPERTY(QVariant filter READ getFilter WRITE setFilter NOTIFY filterChanged)
        QML_ELEMENT
    public:
        explicit RoleFilter(QObject *parent = nullptr);

        int getRole() const;
        void setRole(int newRole);

        QVariant getFilter() const;
        void setFilter(const QVariant &newFilter);

    signals:
        void roleChanged();
        void filterChanged();

    private:
        int m_role;
        QVariant m_filter;
    };


    class QuickProxyModel : public QSortFilterProxyModel
    {
        Q_OBJECT

        Q_PROPERTY(QQmlListProperty<Synoptic::RoleFilter> roleFilters READ getRoleFilters CONSTANT)
        Q_PROPERTY(QVariant sortRole READ getQuickSortRole WRITE setQuickSortRole NOTIFY quickSortRoleChanged)
        Q_PROPERTY(SortOrder sortOrder READ getSortOrder WRITE setSortOrder NOTIFY sortOrderChanged)
        Q_PROPERTY(QAbstractItemModel *sourceModel READ getQuickSourceModel WRITE setQuickSourceModel NOTIFY quickSourceModelChanged)
        Q_CLASSINFO("DefaultProperty", "roleFilters")
        QML_ELEMENT
    public:
        enum SortOrder {
            AscendingOrder,
            DescendingOrder
        };
        Q_ENUM(SortOrder)

        explicit QuickProxyModel(QObject *parent = nullptr);

        QQmlListProperty<Synoptic::RoleFilter> getRoleFilters();

        QAbstractItemModel *getQuickSourceModel() const;
        void setQuickSourceModel(QAbstractItemModel *newSourceModel);

        QVariant getQuickSortRole() const;
        void setQuickSortRole(const QVariant &newSortRole);

        SortOrder getSortOrder() const;
        void setSortOrder(SortOrder newSortOrder);

        Q_INVOKABLE void update();

    signals:
        void quickSourceModelChanged();
        void quickSortRoleChanged();
        void sortOrderChanged();

    protected:
        bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
        bool lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const override;

    private:
        QList<Synoptic::RoleFilter*> m_roleFilters;
        QVariant m_sortRole;
        SortOrder m_sortOrder;

        static void addRoleFilter(QQmlListProperty<Synoptic::RoleFilter> *list, Synoptic::RoleFilter *filter);
    };
} // namespace Synoptic
