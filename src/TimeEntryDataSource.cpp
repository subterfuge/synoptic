#include "TimeEntryDataSource.h"

#include <QSqlQuery>
#include "DbManager.h"

namespace Synoptic
{
    TimeEntryDataSource::TimeEntryDataSource(QObject *parent)
            : QObject(parent)
    {}

    QList<TimeEntry> TimeEntryDataSource::get(int ticketId, const QString &projectId)
    {
        QList<TimeEntry> result;
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare(DbManager::SELECT_TIME_ENTRY_SQL);
        query.bindValue(":ticketId", ticketId);
        query.bindValue(":ticketProjectId", projectId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error querying TimeEntry table:" << error;
            return result;
        }

        while(query.next())
        {
            TimeEntry timeEntry;
            timeEntry.id = query.value(0).toInt();
            timeEntry.ticketId = query.value(1).toInt();
            timeEntry.ticketProjectId = query.value(2).toString();
            timeEntry.beginDate = QDateTime::fromSecsSinceEpoch(query.value(3).toInt());
            timeEntry.duration = query.value(4).toInt();
            result.append(timeEntry);
        }
        return result;
    }

    QList<TimeEntry> TimeEntryDataSource::getByDay(QDateTime date)
    {
        QList<TimeEntry> result;
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare(DbManager::SELECT_TIME_ENTRY_BY_DATE_SQL);
        date.setTime(QTime(0, 0, 0));
        query.bindValue(":date", date);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error querying TimeEntry table:" << error;
            return result;
        }

        while(query.next())
        {
            TimeEntry timeEntry;
            timeEntry.id = query.value(0).toInt();
            timeEntry.ticketId = query.value(1).toInt();
            timeEntry.ticketProjectId = query.value(2).toString();
            timeEntry.beginDate = QDateTime::fromSecsSinceEpoch(query.value(3).toInt());
            timeEntry.duration = query.value(4).toInt();
            result.append(timeEntry);
        }
        return result;
    }

    QSqlError TimeEntryDataSource::insert(int ticketId, const QString &ticketProjectId,
                                          const QDateTime &beginDate, int duration)
    {
        emit getSignals()->preReset();
        auto db = QSqlDatabase::database();
        QSqlQuery query(db);
        query.prepare(DbManager::INSERT_TIME_ENTRY_SQL);
        query.bindValue(":ticketId", ticketId);
        query.bindValue(":ticketProjectId", ticketProjectId);
        query.bindValue(":beginDate", beginDate);
        query.bindValue(":duration", duration);
        query.exec();
        emit getSignals()->postReset();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
            qCritical() << "Error inserting TimeEntry:" << error;
        return error;
    }

    TimeEntrySignals *TimeEntryDataSource::getSignals()
    {
        static TimeEntrySignals sig {};
        return &sig;
    }
} // Synoptic
