// Source: https://stackoverflow.com/questions/68032837/qt5-aero-snap-during-title-bar-customization-on-windows-10

#pragma once

#include <QObject>
#include <QQmlParserStatus>
#include <QQuickItem>
#include <QAbstractNativeEventFilter>

class QQuickWindow;

namespace Synoptic
{
    class WindowsFramelessHelper : public QObject, public QQmlParserStatus
    {
        Q_OBJECT

        Q_INTERFACES(QQmlParserStatus)
        Q_PROPERTY(QQuickItem *titlebar READ titlebar WRITE setTitlebar NOTIFY titlebarChanged)
        Q_PROPERTY(qreal borderWidth READ borderWidth WRITE setBorderWidth NOTIFY borderWidthChanged)
        QML_ELEMENT

    public:
        explicit WindowsFramelessHelper(QObject *parent = nullptr);

        ~WindowsFramelessHelper();

        virtual bool nativeEventFilter(const QByteArray &eventType, void *message, qintptr *result);

        virtual void classBegin() override;

        virtual void componentComplete() override;

        QQuickItem *titlebar() const;

        void setTitlebar(QQuickItem *titlebar);

        qreal borderWidth() const;

        void setBorderWidth(qreal borderWidth);

    signals:

        void borderWidthChanged();

        void titlebarChanged();

    private:
        bool containAcceptMouseButtons(QQuickItem *, qreal x, qreal y);

        QQuickItem *mTitlebar = nullptr;
        QQuickWindow *mQuickWindow = nullptr;

        qreal mBorderWidth = 7;

        class NativeEventFilter : public QAbstractNativeEventFilter
        {
        public:
            virtual bool nativeEventFilter(const QByteArray &eventType, void *message, qintptr *result) override;

            static void deliver(QQuickWindow *window, WindowsFramelessHelper *helper);

            static NativeEventFilter *instance;
            static QHash<WId, WindowsFramelessHelper *> helpers;
        };
    };
}
