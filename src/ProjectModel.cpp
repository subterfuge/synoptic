#include "ProjectModel.h"


namespace Synoptic
{
    ProjectModel::ProjectModel(QObject *parent)
        : QAbstractListModel(parent), m_dataSource(nullptr)
    {}

    int ProjectModel::rowCount(const QModelIndex &parent) const
    {
        if (parent.isValid() || (m_dataSource == nullptr))
            return 0;

        return m_dataSource->getAll().count();
    }

    QVariant ProjectModel::data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        Project *project = m_dataSource->getAll()[index.row()];
        switch(role)
        {
            case IdRole:
                return QVariant(project->getId());
            case TitleRole:
                return QVariant(project->getTitle());
            case CreationDateRole:
                return QVariant(project->getCreationDate());
            case LastTimeUsedRole:
                return QVariant(project->getLastTimeUsed());
            case DescriptionRole:
                return QVariant(project->getDescription());
            case WorkspaceIdRole:
                if(project->getWorkspace() == nullptr)
                    return QVariant(QMetaType::fromType<int>());
                else
                    return QVariant(project->getWorkspace()->getId());
            default:
                return QVariant();
        }
    }

    bool ProjectModel::setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if(m_dataSource == nullptr)
            return false;

        if(data(index, role) != value)
        {
            bool changed = false;
            Project *project = m_dataSource->getAll()[index.row()];
            switch(role)
            {
                case IdRole:
                    if(project->getId() != value.toString())
                    {
                        project->setId(value.toString());
                        changed = true;
                    }
                    break;
                case TitleRole:
                    if(project->getTitle() != value.toString())
                    {
                        project->setTitle(value.toString());
                        changed = true;
                    }
                    break;
                case CreationDateRole:
                    if(project->getCreationDate() != value.toDateTime())
                    {
                        project->setCreationDate(value.toDateTime());
                        changed = true;
                    }
                case LastTimeUsedRole:
                    if(project->getLastTimeUsed() != value)
                    {
                        project->setLastTimeUsed(value.toDateTime());
                        changed = true;
                    }
                case DescriptionRole:
                    if(project->getDescription() != value.toString())
                    {
                        project->setDescription(value.toString());
                        changed = true;
                    }
                    break;
                case WorkspaceIdRole:
                    changed = value.isNull() && project->getWorkspace() != nullptr
                            || project->getWorkspace() == nullptr && !value.isNull()
                            || project->getWorkspace() != nullptr && project->getWorkspace()->getId() != value.toInt();
                    if(value.isNull())
                        project->setWorkspace(nullptr);
                    else
                    {
                        auto *workspace = Synoptic::WorkspaceDataSource().get(value.toInt());
                        project->setWorkspace(workspace);
                    }
                    break;
            }

            if(changed)
            {
                auto error = m_dataSource->update(index.row(), project);
                emit dataChanged(index, index, {role});
                if(error.type() != QSqlError::NoError)
                {
                    qWarning() << error;
                }
                return true;
            }
        }
        return false;
    }

    Qt::ItemFlags ProjectModel::flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return Qt::NoItemFlags;

        return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
    }

    QHash<int, QByteArray> ProjectModel::roleNames() const
    {
        QHash<int, QByteArray> roles;
        roles[IdRole] = "id";
        roles[TitleRole] = "title";
        roles[CreationDateRole] = "creationDate";
        roles[LastTimeUsedRole] = "lastTimeUsed";
        roles[DescriptionRole] = "description";
        roles[WorkspaceIdRole] = "workspaceId";
        return roles;
    }

    ProjectDataSource *ProjectModel::getDataSource() const
    {
        return m_dataSource;
    }

    void ProjectModel::setDataSource(ProjectDataSource *newDataSource)
    {
        beginResetModel();
        if(m_dataSource != nullptr)
            m_dataSource->disconnect(this);
        m_dataSource = newDataSource;
        if(m_dataSource != nullptr)
        {
            connect(m_dataSource, &ProjectDataSource::preItemAppended, this, [this]()
            {
                const int index = m_dataSource->getAll().count();
                beginInsertRows(QModelIndex(), index, index);
            });
            connect(m_dataSource, &ProjectDataSource::postItemAppended, this, [this]()
            {
                endInsertRows();
            });
            connect(m_dataSource, &ProjectDataSource::preItemRemoved, this, [this](int index)
            {
                beginRemoveRows(QModelIndex(), index, index);
            });
            connect(m_dataSource, &ProjectDataSource::postItemRemoved, this, [this]()
            {
                endRemoveRows();
            });
            connect(m_dataSource, &ProjectDataSource::preItemsReset, this, [this]()
            {
                beginResetModel();
            });
            connect(m_dataSource, &ProjectDataSource::postItemsReset, this, [this]()
            {
                endResetModel();
            });
        }
        endResetModel();
    }

} // namespace Synoptic
