#include "Workspace.h"

namespace Synoptic {
    Workspace::Workspace(QObject *parent)
        : QObject(parent)
    {}

    int Workspace::getId() const
    {
        return m_id;
    }

    void Workspace::setId(int newId)
    {
        if(m_id == newId)
            return;
        m_id = newId;
        emit idChanged();
    }

    QString Workspace::getTitle() const
    {
        return m_title;
    }

    void Workspace::setTitle(const QString &newTitle)
    {
        if(m_title == newTitle)
            return;
        m_title = newTitle;
        emit titleChanged();
    }

    int Workspace::getSprintDuration() const
    {
        return m_sprintDuration;
    }

    void Workspace::setSprintDuration(int newSprintDuration)
    {
        if(m_sprintDuration == newSprintDuration)
            return;

        m_sprintDuration = newSprintDuration;
        emit sprintDurationChanged();
    }

    int Workspace::getStoryPointEstimation() const
    {
        return m_storyPointEstimation;
    }

    void Workspace::setStoryPointEstimation(int newStoryPointEstimation)
    {
        if(m_storyPointEstimation == newStoryPointEstimation)
            return;

        m_storyPointEstimation = newStoryPointEstimation;
        emit storyPointEstimationChanged();
    }
}
