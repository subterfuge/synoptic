#pragma once

#include <QObject>
#include <QtQml/qqmlregistration.h>
#include "Sprint.h"
#include "WorkspaceDataSource.h"

namespace Synoptic
{
    class SprintDataSource : public QObject
    {
        Q_OBJECT

        QML_ELEMENT
        QML_SINGLETON
    public:
        explicit SprintDataSource(QObject *parent = nullptr);

        Q_INVOKABLE Sprint* get(int id);

        QList<Sprint *> getAll(int workspaceId, bool onlyOpen = false);

        Q_INVOKABLE int insert(const QString &sprintGoal, const QDateTime &startDate, const QDateTime &endDate,
                const QString &productGoal, int workspaceId);

        bool update(int id, bool closed, const QString &sprintGoal, const QDateTime &startDate, const QDateTime &endDate,
                const QString &productGoal, int workspaceId);

        Q_INVOKABLE bool closeSprint(int id);

    signals:
        void preItemAppended();
        void postItemAppended();
        void preItemRemoved(int index);
        void postItemRemoved();

    private:
        static WorkspaceDataSource& getWorkspaceDataSource();
    };
}