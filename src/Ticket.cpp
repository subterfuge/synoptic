#include "Ticket.h"

namespace Synoptic {

    Ticket::Ticket(QObject *parent)
        : QObject{parent}
    {}

    int Ticket::getId() const
    {
        return m_id;
    }

    void Ticket::setId(int newId)
    {
        if (m_id == newId)
            return;
        m_id = newId;
        emit idChanged();
    }

    Project *Ticket::getProject() const
    {
        return m_project;
    }

    void Ticket::setProject(Project *newProject)
    {
        if (m_project == newProject)
            return;
        m_project = newProject;
        emit projectChanged();
    }

    QString Ticket::getTitle() const
    {
        return m_title;
    }

    void Ticket::setTitle(const QString &newTitle)
    {
        if (m_title == newTitle)
            return;
        m_title = newTitle;
        emit titleChanged();
    }

    int Ticket::getPosition() const
    {
        return m_position;
    }

    void Ticket::setPosition(int newPosition)
    {
        if (m_position == newPosition)
            return;
        m_position = newPosition;
        emit positionChanged();
    }

    QString Ticket::getDescription() const
    {
        return m_description;
    }

    void Ticket::setDescription(const QString &newDescription)
    {
        if (m_description == newDescription)
            return;
        m_description = newDescription;
        emit descriptionChanged();
    }

    Ticket::KanoFeel Ticket::getPresentKanoFeel() const
    {
        return m_presentKanoFeel;
    }

    void Ticket::setPresentKanoFeel(KanoFeel newPresentKanoFeel)
    {
        if (m_presentKanoFeel == newPresentKanoFeel)
            return;
        m_presentKanoFeel = newPresentKanoFeel;
        emit presentKanoFeelChanged();
    }

    Ticket::KanoFeel Ticket::getAbsentKanoFeel() const
    {
        return m_absentKanoFeel;
    }

    void Ticket::setAbsentKanoFeel(KanoFeel newAbsentKanoFeel)
    {
        if (m_absentKanoFeel == newAbsentKanoFeel)
            return;
        m_absentKanoFeel = newAbsentKanoFeel;
        emit absentKanoFeelChanged();
    }

    float Ticket::getStoryPoints() const
    {
        return m_storyPoints;
    }

    void Ticket::setStoryPoints(float newStoryPoints)
    {
        if (m_storyPoints == newStoryPoints)
            return;

        m_storyPoints = newStoryPoints;
        emit storyPointsChanged();
    }

    QDateTime Ticket::getDueDate() const
    {
        return m_dueDate;
    }

    void Ticket::setDueDate(const QDateTime &newDueDate)
    {
        if(m_dueDate == newDueDate)
            return;

        m_dueDate = newDueDate;
        emit dueDateChanged();
    }

    Ticket::Priority Ticket::getPriority() const
    {
        return m_priority;
    }

    void Ticket::setPriority(Ticket::Priority newPriority)
    {
        if(m_priority == newPriority)
            return;

        m_priority = newPriority;
        emit priorityChanged();
    }

    int Ticket::getKanbanColumn() const
    {
        return m_kanbanColumn;
    }

    void Ticket::setKanbanColumn(int newKanbanColumn)
    {
        if(m_kanbanColumn == newKanbanColumn)
            return;

        m_kanbanColumn = newKanbanColumn;
        emit kanbanColumnChanged();
    }

    int Ticket::getKanbanPosition() const
    {
        return m_kanbanPosition;
    }

    void Ticket::setKanbanPosition(int newKanbanPosition)
    {
        if(m_kanbanPosition == newKanbanPosition)
            return;

        m_kanbanPosition = newKanbanPosition;
        emit kanbanPositionChanged();
    }

    bool Ticket::getInNextSprint() const
    {
        return m_inNextSprint;
    }

    void Ticket::setInNextSprint(bool newInNextSprint)
    {
        if(m_inNextSprint == newInNextSprint)
            return;

        m_inNextSprint = newInNextSprint;
        emit inNextSprintChanged();
    }

    Sprint *Ticket::getSprint() const
    {
        return m_sprint;
    }

    void Ticket::setSprint(Sprint *newSprint)
    {
        if (m_sprint == newSprint)
            return;
        m_sprint = newSprint;
        emit sprintChanged();
    }

    Ticket *Ticket::getParent() const
    {
        return m_parent;
    }

    void Ticket::setParent(Ticket *newParent)
    {
        if(m_parent == newParent)
            return;
        m_parent = newParent;
        emit parentChanged();
    }

} // Synoptic
