#include "Sprint.h"
#include "Workspace.h"

namespace Synoptic {
    Sprint::Sprint(QObject *parent)
        : QObject{parent}
    {}

    int Sprint::getId() const
    {
        return m_id;
    }

    void Sprint::setId(int newId)
    {
        if (m_id == newId)
            return;
        m_id = newId;
        emit idChanged();
    }

    bool Sprint::getClosed() const
    {
        return m_closed;
    }

    void Sprint::setClosed(bool newClosed)
    {
        if (m_closed == newClosed)
            return;
        m_closed = newClosed;
        emit closedChanged();
    }

    QString Sprint::getSprintGoal() const
    {
        return m_sprintGoal;
    }

    void Sprint::setSprintGoal(const QString &newSprintGoal)
    {
        if (m_sprintGoal == newSprintGoal)
            return;
        m_sprintGoal = newSprintGoal;
        emit sprintGoalChanged();
    }

    QDateTime Sprint::getStartDate() const
    {
        return m_startDate;
    }

    void Sprint::setStartDate(const QDateTime &newStartDate)
    {
        if (m_startDate == newStartDate)
            return;
        m_startDate = newStartDate;
        emit startDateChanged();
    }

    QDateTime Sprint::getEndDate() const
    {
        return m_endDate;
    }

    void Sprint::setEndDate(const QDateTime &newEndDate)
    {
        if (m_endDate == newEndDate)
            return;
        m_endDate = newEndDate;
        emit endDateChanged();
    }

    QString Sprint::getProductGoal() const
    {
        return m_productGoal;
    }

    void Sprint::setProductGoal(const QString &newProductGoal)
    {
        if (m_productGoal == newProductGoal)
            return;
        m_productGoal = newProductGoal;
        emit productGoalChanged();
    }

    Workspace* Sprint::getWorkspace() const
    {
        return m_workspace;
    }

    void Sprint::setWorkspace(Workspace *newWorkspace)
    {
        if(m_workspace == newWorkspace)
            return;

        m_workspace = newWorkspace;
        emit workspaceChanged();
    }

}
