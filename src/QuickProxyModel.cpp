#include "QuickProxyModel.h"

namespace Synoptic
{

    RoleFilter::RoleFilter(QObject *parent)
    : QObject(parent), m_role()
    {}

    int RoleFilter::getRole() const
    {
        return m_role;
    }

    void RoleFilter::setRole(int newRole)
    {
        if(m_role == newRole)
            return;

        m_role = newRole;
        emit roleChanged();
    }

    QVariant RoleFilter::getFilter() const
    {
        return m_filter;
    }

    void RoleFilter::setFilter(const QVariant &newFilter)
    {
        if(m_filter == newFilter)
            return;

        m_filter = newFilter;
        emit filterChanged();
    }

    QuickProxyModel::QuickProxyModel(QObject *parent)
        : QSortFilterProxyModel(parent), m_sortOrder(QuickProxyModel::AscendingOrder)
    {}

    bool QuickProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
    {
        if(sourceModel() == nullptr)
            return false;

        bool result = true;
        for(auto *filter : m_roleFilters)
        {
            const QVariant &data = sourceModel()->data(sourceModel()->index(sourceRow, 0), filter->getRole());
            switch(static_cast<QMetaType::Type>(filter->getFilter().typeId()))
            {
                case QMetaType::Bool:
                    result &= data.toBool() == filter->getFilter().toBool();
                    break;
                case QMetaType::Int:
                    result &= data.toInt() == filter->getFilter().toInt();
                    break;
                case QMetaType::QString:
                    result &= data.toString().contains(filter->getFilter().toString(), Qt::CaseInsensitive);
                    break;
                default:
                    if(filter->getFilter().canConvert<QVariantList>())
                    {
                        bool intermediateResult = false;
                        for(auto &f: filter->getFilter().toList())
                            intermediateResult |= data.toInt() == f.toInt();
                        result &= intermediateResult;
                    }
                    else
                        qCritical() << "Unhandled type:" << filter->getFilter().metaType().name() <<
                                "(" << static_cast<QMetaType::Type>(filter->getFilter().typeId()) << ")";
                    break;
            }
        }
        return result;
    }

    bool QuickProxyModel::lessThan(const QModelIndex &sourceLeft, const QModelIndex &sourceRight) const
    {
        if(sourceModel() == nullptr)
            return false;
        if(m_sortRole.typeId() != QMetaType::Int)
            return false;

        bool result = false;
        const QVariant &dataLeft = sourceModel()->data(sourceLeft, m_sortRole.toInt());
        const QVariant &dataRight = sourceModel()->data(sourceRight, m_sortRole.toInt());
        switch(static_cast<QMetaType::Type>(dataLeft.typeId()))
        {
        case QMetaType::Int:
            result = dataLeft.toInt() < dataRight.toInt();
            break;
        default:
            qCritical() << "Unhandled type:" << dataLeft.metaType().name();
            break;
        }
        return result;
    }

    void QuickProxyModel::addRoleFilter(QQmlListProperty<Synoptic::RoleFilter> *list, Synoptic::RoleFilter *filter)
    {
        auto *proxyModel = qobject_cast<Synoptic::QuickProxyModel*>(list->object);
        if(proxyModel == nullptr)
            return;

        proxyModel->m_roleFilters.append(filter);
        connect(filter, &RoleFilter::filterChanged, proxyModel, [=]() {
            proxyModel->update();
        });
        connect(filter, &RoleFilter::roleChanged, proxyModel, [=]() {
            proxyModel->update();
        });
    }

    QQmlListProperty<Synoptic::RoleFilter> QuickProxyModel::getRoleFilters()
    {
        return QQmlListProperty<Synoptic::RoleFilter>(this, nullptr, &QuickProxyModel::addRoleFilter,
                                                      nullptr, nullptr, nullptr);
    }

    QAbstractItemModel *QuickProxyModel::getQuickSourceModel() const
    {
        return sourceModel();
    }

    void QuickProxyModel::setQuickSourceModel(QAbstractItemModel *newSourceModel)
    {
        setSourceModel(newSourceModel);
        emit quickSourceModelChanged();
    }

    QVariant QuickProxyModel::getQuickSortRole() const
    {
        return m_sortRole;
    }

    void QuickProxyModel::setQuickSortRole(const QVariant &newSortOnRole)
    {
        if(newSortOnRole == m_sortRole)
            return;

        m_sortRole = newSortOnRole;
        if(newSortOnRole != QVariant())
            sort(0, static_cast<Qt::SortOrder>(m_sortOrder));
        emit quickSortRoleChanged();
    }

    QuickProxyModel::SortOrder QuickProxyModel::getSortOrder() const
    {
        return m_sortOrder;
    }

    void QuickProxyModel::setSortOrder(SortOrder newSortOrder)
    {
        if(newSortOrder == m_sortOrder)
            return;

        m_sortOrder = newSortOrder;
        if(m_sortRole != QVariant())
            sort(0, static_cast<Qt::SortOrder>(m_sortOrder));
        emit sortOrderChanged();
    }

    void QuickProxyModel::update()
    {
        invalidateFilter();
    }

} // namespace Synoptic
