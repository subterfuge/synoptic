#pragma once

#include <QObject>
#include <QDateTime>
#include <QtQml>
#include "TicketDataSource.h"

namespace Synoptic
{
    struct CalendarEvent
    {
        Q_GADGET
        QML_ELEMENT
        QML_UNCREATABLE("CalendarEvent should not be created in qml")

    public:
        enum class Type : short
        {
            GoogleType,
            TicketDueType,
        };
        Q_ENUM(Type)

        QString name;
        Type type;
        QString description;
        QDateTime startDate;
        QDateTime endDate;
    };

    class CalendarEventDataSource : public QObject
    {
        Q_OBJECT

    public:
        explicit CalendarEventDataSource(QObject *parent = nullptr);

        QList<CalendarEvent> get(const QDate &date);

        void init(TicketDataSource *ticketDataSource);

        Q_INVOKABLE void reset();

    signals:
        void preItemsReset();
        void postItemsReset();
        void resetItems();

    private:
        TicketDataSource *m_ticketDataSource;
    };
} // Synoptic
