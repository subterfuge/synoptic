#pragma once

#include <QString>
#include <stdexcept>

namespace Synoptic
{

    class BaseException : public std::exception
    {
    public:
        explicit BaseException(std::string message);
        const char *what() const noexcept override;

    private:
        std::string m_message;
    };

    class ArgumentException : public BaseException
    {
    public:
        explicit ArgumentException(std::string message);
    };

} // namespace Synoptic
