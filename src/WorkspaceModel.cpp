#include "WorkspaceModel.h"

namespace Synoptic
{

    WorkspaceModel::WorkspaceModel(QObject *parent)
            : QAbstractListModel(parent), m_dataSource(nullptr)
    {}

    int WorkspaceModel::rowCount(const QModelIndex &parent) const
    {
        if(parent.isValid() || m_dataSource == nullptr)
            return 0;

        return Synoptic::WorkspaceDataSource::getAll().count();
    }

    QVariant WorkspaceModel::data(const QModelIndex &index, int role) const
    {
        if(!index.isValid())
            return QVariant();

        Workspace *workspace = Synoptic::WorkspaceDataSource::getAll()[index.row()];
        switch(role)
        {
            case IdRole:
                return QVariant(workspace->getId());
            case TitleRole:
                return QVariant(workspace->getTitle());
            case SprintDurationRole:
                return QVariant(workspace->getSprintDuration());
            case StoryPointEstimationRole:
                return QVariant(workspace->getStoryPointEstimation());
            default:
                qWarning() << "Trying to get data from non-existent role " << role;
                return QVariant();
        }
    }

    bool WorkspaceModel::setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if(m_dataSource == nullptr)
            return false;

        if(data(index, role) == value)
            return false;

        bool changed = false;
        Workspace *workspace = Synoptic::WorkspaceDataSource::getAll()[index.row()];
        switch(role)
        {
            case IdRole:
                changed = workspace->getId() != value.toInt();
                workspace->setId(value.toInt());
                break;
            case TitleRole:
                changed = workspace->getTitle() != value.toString();
                workspace->setTitle(value.toString());
                break;
            case SprintDurationRole:
                changed = workspace->getSprintDuration() != value.toInt();
                workspace->setSprintDuration(value.toInt());
                break;
            case StoryPointEstimationRole:
                changed = workspace->getStoryPointEstimation() != value.toInt();
                workspace->setStoryPointEstimation(value.toInt());
                break;
            default:
                break;
        }
        if(changed && m_dataSource->update(index.row(), workspace).type() == QSqlError::NoError)
        {
            emit dataChanged(index, index, {role});
            return true;
        }
        return false;
    }

    Qt::ItemFlags WorkspaceModel::flags(const QModelIndex &index) const
    {
        if(!index.isValid())
            return Qt::NoItemFlags;

        return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
    }

    QHash<int, QByteArray> WorkspaceModel::roleNames() const
    {
        QHash<int, QByteArray> roles;
        roles[IdRole] = "id";
        roles[TitleRole] = "title";
        roles[SprintDurationRole] = "sprintDuration";
        roles[StoryPointEstimationRole] = "storyPointEstimation";
        return roles;
    }

    WorkspaceDataSource *WorkspaceModel::getDataSource() const
    {
        return m_dataSource;
    }

    void WorkspaceModel::setDataSource(WorkspaceDataSource *newDataSource)
    {
        beginResetModel();
        if(m_dataSource != nullptr)
            m_dataSource->disconnect(this);
        m_dataSource = newDataSource;
        if(m_dataSource != nullptr)
        {
            connect(m_dataSource, &WorkspaceDataSource::preItemAppended, this, [this]()
            {
                const int index = m_dataSource->getAll().count();
                beginInsertRows(QModelIndex(), index, index);
            });
            connect(m_dataSource, &WorkspaceDataSource::postItemAppended, this, [this]()
            {
                endInsertRows();
            });
            connect(m_dataSource, &WorkspaceDataSource::preItemRemoved, this, [this](int index)
            {
                beginRemoveRows(QModelIndex(), index, index);
            });
            connect(m_dataSource, &WorkspaceDataSource::postItemRemoved, this, [this]()
            {
                endRemoveRows();
            });
            connect(m_dataSource, &WorkspaceDataSource::itemChanged, this, [this]() {
                beginResetModel();
                endResetModel();
            });
        }
        endResetModel();
    }

} // namespace Synoptic
