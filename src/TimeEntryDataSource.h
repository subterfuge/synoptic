#pragma once

#include <QObject>
#include <QDateTime>
#include <QSqlError>
#include <QtQml/qqmlregistration.h>

namespace Synoptic
{
    struct TimeEntry
    {
        Q_GADGET
        Q_PROPERTY(int id MEMBER id)
        Q_PROPERTY(int ticketId MEMBER ticketId)
        Q_PROPERTY(QString ticketProjectId MEMBER ticketProjectId)
        Q_PROPERTY(QDateTime beginDate MEMBER beginDate)
        Q_PROPERTY(int duration MEMBER duration)
        QML_NAMED_ELEMENT(timeEntry)
    public:
        int id;
        int ticketId;
        QString ticketProjectId;
        QDateTime beginDate;
        int duration; ///< Duration expressed in seconds
    };

    class TimeEntrySignals : public QObject
    {
        Q_OBJECT
    signals:
        void preReset();
        void postReset();
    };

    class TimeEntryDataSource : public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        QML_SINGLETON
    public:
        explicit TimeEntryDataSource(QObject *parent = nullptr);

        Q_INVOKABLE QList<TimeEntry> get(int ticketId, const QString &projectId);

        Q_INVOKABLE QList<TimeEntry> getByDay(QDateTime date);

        Q_INVOKABLE QSqlError insert(int ticketId, const QString &ticketProjectId,
                                     const QDateTime &beginDate, int duration);

        static TimeEntrySignals *getSignals();
    };
} // Synoptic
