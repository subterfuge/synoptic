#pragma once

#include <QObject>
#include "DefinitionOfDoneDataSource.h"

namespace Synoptic
{

    class TicketDefinitionOfDoneModel : public QAbstractListModel
    {
        Q_OBJECT
        QML_ELEMENT
        Q_PROPERTY(int ticketId READ getTicketId WRITE setTicketId NOTIFY ticketIdChanged)
        Q_PROPERTY(QString projectId READ getProjectId WRITE setProjectId NOTIFY projectIdChanged)
        Q_PROPERTY(DefinitionOfDoneDataSource *dataSource READ getDataSource WRITE setDataSource
                NOTIFY dataSourceChanged)

    public:
        enum DefinitionOfDoneRoles
        {
            IdRole = Qt::UserRole,
            DescriptionRole,
            CheckedRole,
        };
        Q_ENUM(DefinitionOfDoneRoles)

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        bool setData(const QModelIndex &index, const QVariant &value,
                int role = Qt::EditRole) override;

        //Qt::ItemFlags flags(const QModelIndex& index) const override;

        QHash<int, QByteArray> roleNames() const override;

        Q_INVOKABLE bool anyChecked() const;

        void update();

        int getTicketId() const;
        void setTicketId(int newTicketId);

        const QString& getProjectId() const;
        void setProjectId(const QString &newProjectId);

        DefinitionOfDoneDataSource *getDataSource() const;
        void setDataSource(DefinitionOfDoneDataSource *newDataSource);

    signals:
        void ticketIdChanged();
        void projectIdChanged();
        void dataSourceChanged();

    private:
        QVector<DefinitionOfDoneItem> m_items;
        int m_ticketId = -1;
        QString m_projectId;
        DefinitionOfDoneDataSource *m_dataSource = nullptr;
    };

} // Synoptic
