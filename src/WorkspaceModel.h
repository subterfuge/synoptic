#pragma once

#include <QAbstractListModel>
#include "WorkspaceDataSource.h"

namespace Synoptic
{
    class WorkspaceModel : public QAbstractListModel
    {
        Q_OBJECT

        QML_ELEMENT
        Q_PROPERTY(WorkspaceDataSource* dataSource READ getDataSource WRITE setDataSource CONSTANT)

        enum WorkspaceRole
        {
            IdRole = Qt::UserRole,
            TitleRole,
            SprintDurationRole,
            StoryPointEstimationRole,
        };

    public:
        explicit WorkspaceModel(QObject *parent = nullptr);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        bool setData(const QModelIndex &index, const QVariant &value,
                     int role = Qt::EditRole) override;

        Qt::ItemFlags flags(const QModelIndex& index) const override;

        QHash<int, QByteArray> roleNames() const override;

        WorkspaceDataSource *getDataSource() const;
        void setDataSource(WorkspaceDataSource *newDataSource);

    private:
        WorkspaceDataSource *m_dataSource;
    };
} // namespace Synoptic
