#include "CalendarEventDataSource.h"

namespace Synoptic
{
    CalendarEventDataSource::CalendarEventDataSource(QObject *parent)
            : QObject{parent}, m_ticketDataSource(nullptr)
    {}

    QList<CalendarEvent> CalendarEventDataSource::get(const QDate &date)
    {
        QList<CalendarEvent> result;
        for(auto ticket : m_ticketDataSource->getAll())
        {
            if(date == ticket->getDueDate().date())
            {
                CalendarEvent event;
                event.name = ticket->getTitle();
                event.type = CalendarEvent::Type::TicketDueType;
                event.description = ticket->getDescription();
                event.startDate = ticket->getDueDate();
                event.endDate = event.startDate;
                result.push_back(event);
            }
        }
        std::sort(result.begin(), result.end(), [](CalendarEvent a, CalendarEvent b) {
            return a.startDate < b.startDate;
        });
        return result;
    }

    void CalendarEventDataSource::init(TicketDataSource *ticketDataSource)
    {
        emit preItemsReset();
        m_ticketDataSource = ticketDataSource;
        emit postItemsReset();
    }

    void CalendarEventDataSource::reset()
    {
        emit resetItems();
    }

} // Synoptic