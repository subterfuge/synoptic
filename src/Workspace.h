#pragma once

#include <QObject>
#include <QtQml/qqmlregistration.h>

namespace Synoptic
{
    class Workspace : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(int id READ getId WRITE setId NOTIFY idChanged FINAL)
        Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged FINAL)
        Q_PROPERTY(int sprintDuration READ getSprintDuration WRITE setSprintDuration NOTIFY sprintDurationChanged FINAL)
        Q_PROPERTY(int storyPointEstimation READ getStoryPointEstimation WRITE setStoryPointEstimation NOTIFY
            storyPointEstimationChanged FINAL)
        QML_ELEMENT

    public:
        explicit Workspace(QObject *parent = nullptr);

        int getId() const;
        void setId(int newId);

        QString getTitle() const;
        void setTitle(const QString &newTitle);

        int getSprintDuration() const;
        void setSprintDuration(int newSprintDuration);

        int getStoryPointEstimation() const;
        void setStoryPointEstimation(int newStoryPointEstimation);

    signals:
        void idChanged();
        void titleChanged();
        void sprintDurationChanged();
        void storyPointEstimationChanged();

    private:
        int m_id = 0;
        QString m_title;
        int m_sprintDuration = 0;
        int m_storyPointEstimation = 0;
    };
}
