#include "WorkspaceDataSource.h"

#include <QSqlQuery>
#include "DbManager.h"

namespace Synoptic
{
    bool WorkspaceDataSource::m_initialized = false;

    WorkspaceDataSource::WorkspaceDataSource(QObject *parent)
        : QObject(parent)
    {
        if(!m_initialized)
            init();
    }

    QList<Workspace*>& WorkspaceDataSource::getAll()
    {
        static QList<Workspace*> list;
        return list;
    }

    QSqlError WorkspaceDataSource::update(const int index, Workspace *workspace)
    {
        Q_ASSERT(!getAll().empty());
        QSqlQuery query;
        query.prepare(DbManager::UPDATE_WORKSPACE_SQL);
        query.bindValue(":id", workspace->getId());
        query.bindValue(":title", workspace->getTitle());
        query.bindValue(":sprintDuration", workspace->getSprintDuration());
        query.bindValue(":storyPointEstimation", workspace->getStoryPointEstimation());
        query.exec();
        auto result = query.lastError();
        if(result.type() == QSqlError::NoError)
        {
            qDebug() << "Updated Workspace with id =" << workspace->getId() << "in database (new data: title = \"" <<
                workspace->getTitle() << "\")";
            getAll()[index] = workspace;
            emit itemChanged(index);
        }
        else
            qCritical() << "Error updating workspace with id =" << workspace->getId() << ":" << result.text();
        return query.lastError();
    }

    int WorkspaceDataSource::getNextId()
    {
        int result = 1;
        for(auto *workspace : getAll())
        {
            if(workspace->getId() >= result)
                result = workspace->getId() + 1;
        }
        return result;
    }

    Workspace *WorkspaceDataSource::get(int id) const
    {
        auto isWorkspacePredicate = [=](Workspace *w) {
            return w->getId() == id;
        };
        auto it = std::find_if(getAll().begin(), getAll().end(), isWorkspacePredicate);
        if(it != getAll().end())
            return *it;
        return nullptr;
    }

    bool WorkspaceDataSource::any() const
    {
        return !getAll().empty();
    }

    QSqlError WorkspaceDataSource::insert(Synoptic::Workspace *newWorkspace)
    {
        QSqlQuery query;
        query.prepare(DbManager::INSERT_WORKSPACE_SQL);
        newWorkspace->setId(getNextId());
        query.bindValue(":id", newWorkspace->getId());
        query.bindValue(":title", newWorkspace->getTitle());
        query.bindValue(":sprintDuration", newWorkspace->getSprintDuration());
        query.bindValue(":storyPointEstimation", newWorkspace->getStoryPointEstimation());
        query.exec();
        auto result = query.lastError();
        if(result.type() == QSqlError::NoError)
        {
            qDebug() << "Inserted workspace with id:" << newWorkspace->getId();
            emit preItemAppended();
            getAll().append(newWorkspace);
            emit postItemAppended();
        }
        else
            qCritical() << "Error inserting workspace:" << result;
        return result;
    }

    QSqlError WorkspaceDataSource::insert(const QString &title, int sprintDuration, int storyPointEstimation)
    {
        auto *workspace = new Workspace(this);
        workspace->setTitle(title);
        workspace->setSprintDuration(sprintDuration);
        workspace->setStoryPointEstimation(storyPointEstimation);
        auto result = insert(workspace);
        return result;
    }

    void WorkspaceDataSource::init()
    {
        getAll().clear();
        auto query = QSqlQuery("SELECT id, title, sprintDuration, storyPointEstimation FROM Workspace ORDER BY id");
        while(query.next())
        {
            int id = query.value(0).toInt();
            QString title = query.value(1).toString();
            int sprintDuration = query.value(2).toInt();
            int storyPointEstimation = query.value(3).toInt();
            Workspace *workspace = new Workspace(this);
            workspace->setId(id);
            workspace->setTitle(title);
            workspace->setSprintDuration(sprintDuration);
            workspace->setStoryPointEstimation(storyPointEstimation);
            getAll().push_back(workspace);
        }
        qDebug() << "Initialized WorkspaceDataSource with" << getAll().count() << "items";
        m_initialized = true;
    }
} // namespace Synoptic
