#pragma once

#include <QObject>
#include <QtQmlIntegration>

namespace Synoptic
{
    struct DefinitionOfDoneItem
    {
        Q_GADGET
        Q_PROPERTY(QString description MEMBER description)
    public:
        int id;
        QString description;
        bool checked = false;
    };

    class DefinitionOfDoneSignals : public QObject
    {
        Q_OBJECT
    signals:
        void projectDodItemAppended();
        void ticketDodItemAppended(int ticketId, const QString& projectId);
        void projectDodItemChanged(int id);
        void ticketDodItemChanged(int id);
        void ticketDodUpdated(int ticketId, const QString& projectId);
    };

    class DefinitionOfDoneDataSource : public QObject
    {
        Q_OBJECT
        QML_ELEMENT
        QML_SINGLETON

    public:
        enum Type
        {
            Ticket,
            Project,
        };

        /// Get the DefinitionOfDoneItem list defined as default for the given project.
        ///
        /// \note Corresponds to a list of DefinitionOfDoneItem rows in the database. For a list of
        /// DefinitionOfDoneTicketRow rows, use get(const QString &projectId, int ticketId) instead.
        Q_INVOKABLE QVector<DefinitionOfDoneItem> get(const QString &projectId);

        /// Get the DefinitionOfDoneItem list defined for a ticket.
        ///
        /// \note Correspond to a list of DefinitionOfDoneTicketRow rows in the database. For a list of
        /// DefinitionOfDoneItem rows, use get(const QString &projectId) instead.
        Q_INVOKABLE QVector<DefinitionOfDoneItem> get(const QString &projectId, int ticketId);

        static DefinitionOfDoneItem getItem(int id, DefinitionOfDoneDataSource::Type type = Type::Ticket);

        Q_INVOKABLE bool insert(const QString &description, const QString &projectId);
        Q_INVOKABLE bool insert(const QString &description, int ticketId, const QString &projectId);

        bool update(const int id, const QString &description);
        bool update(const int id, const QString &description, bool checked);

        Q_INVOKABLE bool deleteProjectDefaultItem(int id);
        Q_INVOKABLE bool clear(int ticketId, const QString &projectId);
        Q_INVOKABLE bool uncheckAll(int ticketId, const QString &projectId);

        DefinitionOfDoneSignals *getSignals();
    };
} // Synoptic
