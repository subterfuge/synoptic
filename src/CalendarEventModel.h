#pragma once

#include <QAbstractListModel>
#include <QtQml>
#include "CalendarEventDataSource.h"

namespace Synoptic
{
    class CalendarEventModel : public QAbstractListModel
    {
        Q_OBJECT

        Q_PROPERTY(QDate date READ getDate WRITE setDate NOTIFY dateChanged)
        Q_PROPERTY(CalendarEventDataSource *dataSource READ getDataSource WRITE setDataSource NOTIFY dataSourceChanged)
        QML_ELEMENT

    public:
        enum CalendarRoles
        {
            NameRole = Qt::UserRole,
            TypeRole,
            DescriptionRole,
            StartDateRole,
            EndDateRole
        };

        explicit CalendarEventModel(QObject *parent = nullptr);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        QHash<int, QByteArray> roleNames() const override;

        Q_INVOKABLE void update();

        QDate getDate() const;
        void setDate(const QDate &newDate);

        CalendarEventDataSource* getDataSource() const;
        void setDataSource(CalendarEventDataSource *newDataSource);

    signals:
        void dateChanged();
        void dataSourceChanged();

    private:
        QVector<CalendarEvent> m_events;
        QDate m_date;
        CalendarEventDataSource *m_dataSource;
    };
}
