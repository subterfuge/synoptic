#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <QAbstractListModel>
#include <QtQml/qqmlregistration.h>
#include "ProjectDataSource.h"
#include "WorkspaceDataSource.h"

namespace Synoptic
{
    class ProjectModel : public QAbstractListModel
    {
        Q_OBJECT

        Q_PROPERTY(ProjectDataSource* dataSource READ getDataSource WRITE setDataSource CONSTANT)
        QML_ELEMENT

        enum ProjectRole
        {
            IdRole = Qt::UserRole,
            TitleRole,
            CreationDateRole,
            LastTimeUsedRole,
            DescriptionRole,
            WorkspaceIdRole,
        };

    public:
        explicit ProjectModel(QObject *parent = nullptr);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        bool setData(const QModelIndex &index, const QVariant &value,
                     int role = Qt::EditRole) override;

        Qt::ItemFlags flags(const QModelIndex& index) const override;

        QHash<int, QByteArray> roleNames() const override;

        ProjectDataSource *getDataSource() const;
        void setDataSource(ProjectDataSource *newDataSource);

    private:
        ProjectDataSource *m_dataSource;
    };
} // namespace Synoptic

#endif // PROJECTMODEL_H
