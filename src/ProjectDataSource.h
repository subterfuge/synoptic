#ifndef PROJECTDATASOURCE_H
#define PROJECTDATASOURCE_H

#include <QObject>
#include <QSqlError>
#include <QtQml/qqmlregistration.h>
#include "Project.h"
#include "WorkspaceDataSource.h"

namespace Synoptic
{
    /*struct Project
    {
        QString id;
        QString title;
        int sprintDuration; //!< Duration of one sprint in weeks
        QString description;
        QString productGoal; //!< Current product goal of the project
    };*/

    class ProjectDataSource : public QObject
    {
        Q_OBJECT

        QML_ELEMENT
        QML_SINGLETON
    public:
        explicit ProjectDataSource(QObject *parent = nullptr);

        enum class ProjectInsertError
        {
            Ok = 0,
            IdAlreadyExists,
            IdMalformed,
            DbError,
        };
        Q_ENUM(ProjectInsertError)

        void init();

        void resetFromDb(int workspaceId = -1);

        QList<Project*>& getAll();

        Q_INVOKABLE Project* get(const QString &id);

        QSqlError update(const int index, Project *project);

        Q_INVOKABLE Synoptic::ProjectDataSource::ProjectInsertError insert(const QString &id, const QString &title,
               const QString &description, int workspaceId);

    signals:
        void preItemAppended();
        void postItemAppended();
        void preItemRemoved(int index);
        void postItemRemoved();
        void preItemsReset();
        void postItemsReset();

    private:
        inline static QList<Project*> m_list;
        inline static bool m_init = false;
    };
} // namespace Synoptic

#endif // PROJECTDATASOURCE_H
