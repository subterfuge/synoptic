#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QString>
#include <QSqlDatabase>
#include <QSqlError>
#include <QStandardPaths>
#include <QDir>
#include <QCoreApplication>

namespace Synoptic
{
    inline QString dbName;

    constexpr auto WORKSPACE_TABLE_NAME = QLatin1StringView("Workspace");
    constexpr auto SPRINT_TABLE_NAME = QLatin1StringView("Sprint");
    constexpr auto PROJECT_TABLE_NAME = QLatin1StringView("Project");
    constexpr auto TIME_ENTRY_TABLE_NAME = QLatin1StringView("TimeEntry");
    constexpr auto TICKET_TABLE_NAME = QLatin1StringView("Ticket");
    constexpr auto HISTORY_TABLE_NAME = QLatin1StringView("History");
    constexpr auto PROJECT_DEFINITION_OF_DONE_TABLE_NAME = QLatin1StringView("ProjectDefinitionOfDone");
    constexpr auto TICKET_DEFINITION_OF_DONE_TABLE_NAME = QLatin1StringView("TicketDefinitionOfDone");

    class DbManager
    {
    public:
        inline static const auto CREATE_WORKSPACE_SQL = R"(
            CREATE TABLE )" + WORKSPACE_TABLE_NAME + R"((id INTEGER PRIMARY KEY, title TEXT NOT NULL UNIQUE,
            sprintDuration INTEGER NOT NULL, storyPointEstimation INTEGER NOT NULL);
        )";

        inline static const auto INSERT_DEFAULT_WORKSPACE_SQL =
                "INSERT INTO " + WORKSPACE_TABLE_NAME + "(id, title, sprintDuration, storyPointEstimation)"
                " VALUES(1, 'Workspace', 14, 300);";

        inline static const auto CREATE_SPRINT_SQL = R"(
            CREATE TABLE )" + SPRINT_TABLE_NAME + R"((id INTEGER PRIMARY KEY, closed INTEGER NOT NULL DEFAULT 0,
            sprintGoal TEXT, startDate INTEGER, endDate INTEGER, productGoal TEXT,
            workspaceId INTEGER NOT NULL REFERENCES Workspace(id));
        )";

        inline static const auto CREATE_PROJECT_SQL = R"(
            CREATE TABLE )" + PROJECT_TABLE_NAME + R"((id TEXT PRIMARY KEY, title TEXT NOT NULL,
            creationDate INTEGER NOT NULL, lastTimeUsed INTEGER NOT NULL, productGoal TEXT, description TEXT,
            workspaceId INTEGER NOT NULL REFERENCES Workspace(id));
        )";

        inline static const auto CREATE_TICKET_SQL = R"(
            CREATE TABLE )" + TICKET_TABLE_NAME + R"((id INTEGER NOT NULL,
            projectId TEXT NOT NULL REFERENCES Project(id),
            title TEXT NOT NULL,
            position INTEGER NOT NULL,
            description TEXT,
            presentKanoFeel INTEGER NOT NULL DEFAULT 0,
            absentKanoFeel INTEGER NOT NULL DEFAULT 0,
            storyPoints REAL,
            dueDate INTEGER,
            priority INTEGER NOT NULL DEFAULT 1,
            kanbanColumn INTEGER NOT NULL DEFAULT 0,
            kanbanPosition INTEGER NOT NULL DEFAULT 0,
            inNextSprint INTEGER NOT NULL DEFAULT 0,
            archived INTEGER NOT NULL DEFAULT 0,
            sprintId INTEGER REFERENCES Sprint(id),
            parentTicketId INTEGER REFERENCES Ticket(id),
            PRIMARY KEY (id, projectId));
        )";

        inline static const auto CREATE_TIME_ENTRY_SQL = R"(
            CREATE TABLE )" + TIME_ENTRY_TABLE_NAME + R"((id INTEGER PRIMARY KEY,
            ticketId INTEGER NOT NULL REFERENCES Ticket(id),
            ticketProjectId TEXT NOT NULL REFERENCES Ticket(projectId),
            beginDate INTEGER NOT NULL,
            duration INTEGER NOT NULL);
        )";

        inline static const auto CREATE_HISTORY_SQL = R"(
            CREATE TABLE )" + HISTORY_TABLE_NAME + R"((id INTEGER NOT NULL,
            ticketId INTEGER NOT NULL REFERENCES Ticket(id),
            ticketProjectId TEXT NOT NULL REFERENCES Ticket(projectId),
            date INTEGER NOT NULL, data BLOB NOT NULL,
            action INTEGER NOT NULL,
            PRIMARY KEY (id, ticketId, ticketProjectId));
        )";

        inline static const auto CREATE_PROJECT_DEFINITION_OF_DONE_SQL =
            "CREATE TABLE " + PROJECT_DEFINITION_OF_DONE_TABLE_NAME + "("
                R"("id" INTEGER, "description" TEXT NOT NULL, "projectId" TEXT, )"
                R"(PRIMARY KEY("id"), FOREIGN KEY("projectId") REFERENCES "Project"("id"))"
            ");";

        inline static const auto CREATE_TICKET_DEFINITION_OF_DONE_SQL =
            "CREATE TABLE " + TICKET_DEFINITION_OF_DONE_TABLE_NAME + "("
                R"("id" INTEGER,)"
                R"("description" TEXT,)"
                R"("checked" INTEGER DEFAULT 0,)"
                R"("ticketId" INTEGER,)"
                R"("projectId" TEXT,)"
                R"(PRIMARY KEY("id"),)"
                R"(FOREIGN KEY("ticketId") REFERENCES "Ticket"("id"),)"
                R"(FOREIGN KEY("projectId") REFERENCES "Ticket"("projectId"))"
            ");";

        inline static const auto INSERT_WORKSPACE_SQL =
            R"(INSERT INTO )" + WORKSPACE_TABLE_NAME + R"((id, title, sprintDuration, storyPointEstimation)
            VALUES(:id, :title, :sprintDuration, :storyPointEstimation);)";

        inline static const auto INSERT_SPRINT_SQL = R"(
            INSERT INTO )" + SPRINT_TABLE_NAME + R"((closed, sprintGoal, startDate, endDate, productGoal, workspaceId)
            VALUES(:closed, :sprintGoal, :startDate, :endDate, :productGoal, :workspaceId);
        )";

        inline static const auto INSERT_PROJECT_SQL = R"(
            INSERT INTO )" + PROJECT_TABLE_NAME + R"((id, title, creationDate, lastTimeUsed,
            productGoal, description, workspaceId) VALUES(:id, :title, :creationDate, :lastTimeUsed,
            :productGoal, :description, :workspaceId);
        )";

        inline static const auto INSERT_TICKET_SQL = R"(
            INSERT INTO )" + TICKET_TABLE_NAME + R"((id, projectId, title, position, description, presentKanoFeel,
            absentKanoFeel, storyPoints, dueDate, priority, kanbanColumn, kanbanPosition, inNextSprint, sprintId,
            parentTicketId)
            VALUES(:id, :projectId, :title, :position, :description, :presentKanoFeel, :absentKanoFeel, :storyPoints,
            :dueDate, :priority, :kanbanColumn, :kanbanPosition, :inNextSprint, :sprintId, :parentTicketId);
        )";

        inline static const auto INSERT_HISTORY_SQL = R"(
            INSERT INTO )" + HISTORY_TABLE_NAME + R"((id, ticketId, ticketProjectId, date, data, action)
            VALUES(?, ?, ?, ?, ?, ?);
        )";

        inline static const auto INSERT_TIME_ENTRY_SQL = R"(
            INSERT INTO )" + TIME_ENTRY_TABLE_NAME + R"((ticketId, ticketProjectId, beginDate, duration)
            VALUES(:ticketId, :ticketProjectId, :beginDate, :duration);
        )";

        inline static const auto INSERT_PROJECT_DOD_SQL =
                "INSERT INTO " + PROJECT_DEFINITION_OF_DONE_TABLE_NAME + "(description, projectId) "
                "VALUES(:description, :projectId)";

        inline static const auto INSERT_TICKET_DOD_SQL =
                "INSERT INTO " + TICKET_DEFINITION_OF_DONE_TABLE_NAME + "(description, checked, ticketId, projectId) "
                " VALUES(:description, :checked, :ticketId, :projectId)";

        inline static const auto UPDATE_WORKSPACE_SQL = R"(
            UPDATE )" + WORKSPACE_TABLE_NAME + R"( SET title = :title, sprintDuration = :sprintDuration,
            storyPointEstimation = :storyPointEstimation WHERE id = :id;
        )";

        inline static const auto UPDATE_TICKET_SQL = R"(
            UPDATE )" + TICKET_TABLE_NAME + R"( SET title = :title, position = :position, description = :description,
            presentKanoFeel = :presentKanoFeel, absentKanoFeel = :absentKanoFeel, storyPoints = :storyPoints,
            dueDate = :dueDate, priority = :priority, kanbanColumn = :kanbanColumn, kanbanPosition = :kanbanPosition,
            inNextSprint = :inNextSprint, sprintId = :sprintId, parentTicketId = :parentTicketId
            WHERE id = :id AND projectId = :projectId;
        )";

        inline static const auto UPDATE_PROJECT_SQL = R"(
            UPDATE )" + PROJECT_TABLE_NAME + R"( SET title = :title, lastTimeUsed = :lastTimeUsed, productGoal =
            :productGoal, description = :description, workspaceId = :workspaceId WHERE id = :id;
        )";

        inline static const auto UPDATE_SPRINT_SQL = R"(
            UPDATE )" + SPRINT_TABLE_NAME + R"( SET closed = :closed, sprintGoal = :sprintGoal,
            startDate = :startDate, endDate = :endDate, productGoal = :productGoal, workspaceId = :workspaceId
            WHERE id = :id;
        )";

        inline static const auto UPDATE_PROJECT_DOD_SQL =
                "UPDATE " + PROJECT_DEFINITION_OF_DONE_TABLE_NAME + " SET description = :description"
                " WHERE id = :id;";

        inline static const auto UPDATE_TICKET_DOD_SQL =
                "UPDATE " + TICKET_DEFINITION_OF_DONE_TABLE_NAME + " SET description = :description,"
                " checked = :checked WHERE id = :id;";

        inline static const auto CLOSE_SPRINT_SQL = R"(
            UPDATE )" + SPRINT_TABLE_NAME + R"( SET closed = 1 WHERE id = :id;
        )";

        inline static const auto ARCHIVE_COMPLETED_TICKETS_SQL = R"(
            UPDATE )" + TICKET_TABLE_NAME + R"( SET archived = 1 WHERE inNextSprint = 1 AND kanbanColumn = 3;
        )";

        inline static const auto SELECT_TIME_ENTRY_SQL = R"(
            SELECT id, ticketId, ticketProjectId, beginDate, duration FROM )" + TIME_ENTRY_TABLE_NAME +
            R"( WHERE ticketId = :ticketId AND ticketProjectId = :ticketProjectId ORDER BY beginDate DESC;
        )";

        inline static const auto SELECT_TIME_ENTRY_BY_DATE_SQL =
                "SELECT id, ticketId, ticketProjectId, beginDate, duration FROM " + TIME_ENTRY_TABLE_NAME +
                " WHERE date(beginDate, 'auto') = date(:date)";

        inline static const auto SELECT_SPRINT_SQL = R"(
            SELECT id, closed, sprintGoal, startDate, endDate, productGoal, workspaceId FROM )" + SPRINT_TABLE_NAME +
            R"( WHERE id = :sprintId;
        )";

        inline static const auto SELECT_ALL_SPRINT_SQL = R"(
            SELECT id, closed, sprintGoal, startDate, endDate, productGoal, workspaceId FROM )" + SPRINT_TABLE_NAME +
            R"( WHERE workspaceId = :workspaceId ORDER BY startDate;
        )";

        inline static const auto SELECT_ALL_OPEN_SPRINT_SQL = R"(
            SELECT id, closed, sprintGoal, startDate, endDate, productGoal, workspaceId FROM )" + SPRINT_TABLE_NAME +
            R"( WHERE workspaceId = :workspaceId AND closed = 0 ORDER BY startDate;
        )";

        inline static const auto SELECT_PROJECT_DOD_SQL =
                "SELECT id, description FROM " + PROJECT_DEFINITION_OF_DONE_TABLE_NAME +
                " WHERE projectId = :projectId";

        inline static const auto SELECT_TICKET_DOD_SQL =
                "SELECT id, description, checked FROM " + TICKET_DEFINITION_OF_DONE_TABLE_NAME +
                " WHERE ticketId = :ticketId AND projectId = :projectId";

        inline static const auto ARCHIVE_TICKET_SQL =
            R"(UPDATE )" + TICKET_TABLE_NAME + R"( SET archived = :archived WHERE id = :ticketId
            AND projectId = :projectId;
        )";

        inline static const auto DELETE_TICKET_SQL =
            R"(DELETE FROM )" + TICKET_TABLE_NAME + R"( WHERE id = :ticketId AND projectId = :projectId;)";

        inline static const auto CLEAR_TICKET_DOD_SQL =
                "DELETE FROM " + TICKET_DEFINITION_OF_DONE_TABLE_NAME + " WHERE ticketId = :ticketId AND "
                "projectId = :projectId;";

        inline static const auto UNCHECK_ALL_TICKET_DOD_SQL =
                "UPDATE " + TICKET_DEFINITION_OF_DONE_TABLE_NAME + " SET checked = false"
                " WHERE ticketId = :ticketId AND projectId = :projectId";

        DbManager();

        QSqlError init();

    private:
        QSqlDatabase m_db;
    };
} // namespace Synoptic

#endif // DBMANAGER_H
