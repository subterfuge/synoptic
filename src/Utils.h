#pragma once

#include <QObject>
#include <QSortFilterProxyModel>
#include <QQmlApplicationEngine>
#include "TicketModel.h"
#include "ProjectModel.h"
#include "WorkspaceModel.h"
#include "Settings.h"
#include "CalendarEventDataSource.h"

namespace Synoptic
{

    class Utils : public QObject
    {
    Q_OBJECT

    public:
        static Utils& getInstance();

        WorkspaceModel &getWorkspaceModel();

        ProjectModel& getProjectModel();

        CalendarEventDataSource& getCalendarEventDataSource();

        Q_INVOKABLE void resetDataSources();

        Settings& getSettings();

        QQmlApplicationEngine* getEngine() const;
        void setEngine(QQmlApplicationEngine *engine);

    private:
        explicit Utils(QObject *parent = nullptr);

        WorkspaceModel m_workspaceModel;
        ProjectModel m_projectModel;
        CalendarEventDataSource m_calendarEventDataSource;
        Synoptic::Settings m_settings;
        QQmlApplicationEngine* m_engine;
    };

} // Synoptic
