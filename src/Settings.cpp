#include "Settings.h"

#include <QCoreApplication>
#include "Utils.h"

namespace Synoptic
{
    QTranslator Settings::m_translator;

    Settings::Settings(QObject *parent)
            : QObject(parent)
    {}

    bool Settings::setWorkspace(int workspaceId) const
    {
        QSettings settings;
        auto previousValue = -1;
#ifdef QT_DEBUG
        const auto *key = "workspace_debug";
#else
        const auto *key = "workspace";
#endif
        if(settings.contains(key))
            previousValue = settings.value(key).toInt();
        if(previousValue == workspaceId)
        {
            qDebug() << "Settings: workspace value unchanged (" << workspaceId << ")";
            return false;
        }

        settings.setValue(key, workspaceId);
        qDebug() << "Settings: workspace value change (old:" << previousValue << "; new:" << workspaceId << ")";
        return true;
    }

    int Settings::getWorkspace() const
    {
        QSettings settings;
#ifdef QT_DEBUG
        const auto *key = "workspace_debug";
#else
        cosnt auto *key = "workspace";
#endif
        if(settings.contains(key))
            return settings.value(key).toInt();

        return -1;
    }

    bool Settings::setLocale(const QString &locale)
    {
        QSettings settings;
        QString previousValue;
        if(settings.contains("i16n/locale"))
            previousValue = settings.value("i16n/locale").toString();
        if(previousValue == locale)
        {
            qDebug() << "Settings: i16n/locale value unchanged (" << locale << ")";
            return false;
        }

        settings.setValue("i16n/locale", locale);
        qDebug() << "Settings: i16n/locale value changed (old:" << previousValue << "; new:" << locale << ")";
        return true;
    }

    QString Settings::getLocale() const
    {
        QSettings settings;
        if(settings.contains("i16n/locale"))
            return settings.value("i16n/locale").toString();

        return QString();
    }

    bool Settings::reloadTranslation(QString locale)
    {
        if(!m_translator.isEmpty())
            QCoreApplication::removeTranslator(&m_translator);
        if(locale.isEmpty())
            locale = "en";
        if(!m_translator.load("synoptic_" + locale))
        {
            qWarning() << "Unable to load" << locale << "translation file";
            return false;
        }

        QCoreApplication::installTranslator(&m_translator);
        Utils::getInstance().getEngine()->retranslate();
        return true;
    }

    bool Settings::setCurrentSprint(int sprintId)
    {
#ifdef QT_DEBUG
        const auto *key = "sprint_debug";
#else
        const auto *key = "sprint";
#endif
        auto result = setSetting(key, sprintId);
        if(result)
            emit currentSprintChanged();
        return result;
    }

    int Settings::getCurrentSprint() const
    {
        QSettings settings;
#ifdef QT_DEBUG
        const auto *key = "sprint_debug";
#else
        const auto *key = "sprint";
#endif
        if(settings.contains(key))
            return settings.value(key).toInt();

        return -1;
    }

    Synoptic::Settings* QmlSettingsFactory::create(QQmlEngine*_, QJSEngine *engine)
    {
        auto& utils = Synoptic::Utils::getInstance();
        auto* settings = &(utils.getSettings());
        Q_ASSERT(settings);
        Q_ASSERT(engine->thread() == settings->thread());

        if(m_engine)
            Q_ASSERT(m_engine == engine);
        else
            m_engine = engine;
        QJSEngine::setObjectOwnership(settings, QJSEngine::CppOwnership);
        return settings;
    }

} // namespace Synoptic
