#ifndef TICKETDATASOURCE_H
#define TICKETDATASOURCE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QModelIndex>
#include "ProjectDataSource.h"
#include "Ticket.h"

namespace Synoptic
{
    /*struct Ticket
    {
        int id;
        class Project* project;
        QString title;
        int position;
        QString description;
    };*/

    class TicketInfo: public QObject
    {
    Q_OBJECT
        Q_PROPERTY(int sprintReadyTicketsCount READ getSprintReadyTicketsCount NOTIFY sprintReadyTicketsCountChanged)
        Q_PROPERTY(float sprintReadyEstimate READ getSprintReadyEstimate NOTIFY sprintReadyEstimateChanged)
        Q_PROPERTY(int sprintReadyDoneCount READ getSprintReadyDoneCount NOTIFY sprintReadyDoneCountChanged)
        Q_PROPERTY(float sprintReadyDoneEstimate READ getSprintReadyDoneEstimate NOTIFY sprintReadyDoneEstimateChanged)
        QML_ELEMENT

    public:
        explicit TicketInfo(QObject* parent = nullptr) {}

        int getSprintReadyTicketsCount() const;
        void setSprintReadyTicketsCount(int newSprintReadyTicketsCount);

        float getSprintReadyEstimate() const;
        void setSprintReadyEstimate(float newSprintReadyEstimate);

        int getSprintReadyDoneCount() const;
        void setSprintReadyDoneCount(int newSprintReadyDoneCount);

        float getSprintReadyDoneEstimate() const;
        void setSprintReadyDoneEstimate(float newSprintReadyDoneEstimate);

    signals:
        void sprintReadyTicketsCountChanged();
        void sprintReadyEstimateChanged();
        void sprintReadyDoneCountChanged();
        void sprintReadyDoneEstimateChanged();
        void infoChanged();

    private:
        int m_sprintReadyTicketsCount = 0;
        float m_sprintReadyEstimate = 0.f;
        int m_sprintReadyDoneCount = 0;
        float m_sprintReadyDoneEstimate = 0.f;
    };

    class TicketDataSourceSignals : public QObject
    {
    Q_OBJECT
    public:
        explicit TicketDataSourceSignals(QObject* parent = nullptr) noexcept {}
    signals:
        void preItemAppended();
        void postItemAppended();
        void preItemRemoved(int index);
        void postItemRemoved();
        void preItemsReset();
        void postItemsReset();
        void itemChanged(int index);
    };

    class TicketDataSource : public QObject
    {
        Q_OBJECT

        QML_ELEMENT
        QML_SINGLETON

    public:
        explicit TicketDataSource(QObject *parent = nullptr);

        QList<Ticket*>& getAll();

        void move(int from, int to, Project *project);

        int getNextId(Project *project);

        Q_INVOKABLE Ticket* get(int id, const QString& projectId) const;

        Q_INVOKABLE TicketInfo* getInfo();

        void updateInfo();

        Q_INVOKABLE TicketDataSourceSignals& getSignals();

        /// Get the index of the ticket in the datasource
        /// \param ticket the ticket to search for
        /// \return The index of the ticket, starting at 0 or -1 if the ticket can't be found
        int getIndex(Synoptic::Ticket* ticket) const;

        Ticket* getSelected() const;
        QModelIndex getSelectedIndex() const;
        void setSelected(Ticket* ticket, const QModelIndex& index);

        /// Insert a new ticket in database and in datasource's list.
        /// \param newTicket the new ticket to push in database. newTicket's id is discarded and the next database id
        ///                  is used instead. newTicket's id gets updated accordingly
        /// \return -1 if something went wrong, the id of the new ticket otherwise
        Q_INVOKABLE int insert(const QString &projectId, const QString &title, int position,
                const QString &description, Ticket::KanoFeel presentKanoFeel, Ticket::KanoFeel absentKanoFeel,
                float storyPoints, const QDateTime &dueDate, Ticket::Priority priority,
                int kanbanColumn = 0, int kanbanPosition = 0, bool inNextSprint = false);

        Q_INVOKABLE bool update(Synoptic::Ticket *ticket);

        Q_INVOKABLE QSqlError archive(Synoptic::Ticket *ticket);

        Q_INVOKABLE QSqlError deleteTicket(Synoptic::Ticket *ticket);

        Q_INVOKABLE bool setKanbanPosition(const QString &projectId, int ticketId, int position);

        int removeCompletedFromList();

        Q_INVOKABLE void beginTransaction();
        Q_INVOKABLE void endTransaction();
        Q_INVOKABLE void rollback();

        /// Clears #m_list and repopulate it using database.
        /// \param workspaceId id of workspace to load. If negative, will load all the tickets.
        void resetFromDb(int workspaceId = -1);

    private:
        inline static bool m_init = false;
        inline static QList<Ticket*> m_list;
        inline static Ticket* m_selectedTicket = nullptr;
        inline static QModelIndex m_selectedTicketIndex {};
    };
} // namespace Synoptic

Q_DECLARE_METATYPE(Synoptic::TicketInfo)

#endif // TICKETDATASOURCE_H
