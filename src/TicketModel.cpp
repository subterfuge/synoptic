#include "TicketModel.h"

namespace Synoptic
{

    TicketModel::TicketModel(QObject *parent)
        : QAbstractListModel(parent), m_dataSource(nullptr)
    {}

    int TicketModel::rowCount(const QModelIndex &parent) const
    {
        // For list models only the root node (an invalid parent) should return the list's size. For all
        // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
        if (parent.isValid() || (m_dataSource == nullptr))
            return 0;

        return m_dataSource->getAll().count();
    }

    QVariant TicketModel::data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();

        Ticket *ticket = m_dataSource->getAll()[index.row()];
        switch(role)
        {
            case IdRole:
                return QVariant(ticket->getId());
            case ProjectIdRole:
                if(ticket->getProject() == nullptr)
                    return QVariant(QMetaType::fromType<QString>());
                return QVariant(ticket->getProject()->getId());
            case TitleRole:
                return QVariant(ticket->getTitle());
            case DescriptionRole:
                return QVariant(ticket->getDescription());
            case PositionRole:
                return QVariant(ticket->getPosition());
            case PresentKanoFeelRole:
                return QVariant(static_cast<int>(ticket->getPresentKanoFeel()));
            case AbsentKanoFeelRole:
                return QVariant(static_cast<int>(ticket->getAbsentKanoFeel()));
            case StoryPointsRole:
                return QVariant(ticket->getStoryPoints());
            case DueDateRole:
                return QVariant(ticket->getDueDate());
            case PriorityRole:
                return QVariant(static_cast<int>(ticket->getPriority()));
            case KanbanColumnRole:
                return QVariant(ticket->getKanbanColumn());
            case KanbanPositionRole:
                return QVariant(ticket->getKanbanPosition());
            case InNextSprintRole:
                return QVariant(ticket->getInNextSprint());
            case SprintIdRole:
                if(ticket->getSprint() == nullptr)
                    return QVariant(QMetaType::fromType<int>());
                return QVariant(ticket->getSprint()->getId());
            case ParentIdRole:
                if(ticket->getParent() == nullptr)
                    return QVariant(QMetaType::fromType<int>());
                return QVariant(ticket->getParent()->getId());
            case SelectedRole:
                bool result = (ticket == m_dataSource->getSelected());
                return result;
        }

        return QVariant();
    }

    bool TicketModel::setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if(m_dataSource == nullptr)
            return false;

        if (data(index, role) != value)
        {
            bool changed = false;
            Ticket *ticket = m_dataSource->getAll()[index.row()];
            switch(role)
            {
                case IdRole:
                    qWarning() << "Changing id is not supported.";
                    return false;
                case ProjectIdRole:
                    qWarning() << "Changing project is not supported.";
                    return false;
                case TitleRole:
                    if (ticket->getTitle() != value.toString())
                    {
                        ticket->setTitle(value.toString());
                        changed = true;
                    }
                    break;
                case DescriptionRole:
                    if (ticket->getDescription() != value.toString())
                    {
                        ticket->setDescription(value.toString());
                        changed = true;
                    }
                    break;
                case PositionRole:
                    if (ticket->getPosition() != value.toInt())
                    {
                        ticket->setPosition(value.toInt());
                        changed = true;
                    }
                    break;
                case PresentKanoFeelRole:
                    if (static_cast<int>(ticket->getPresentKanoFeel()) != value.toInt())
                    {
                        ticket->setPresentKanoFeel(static_cast<Ticket::KanoFeel>(value.toInt()));
                        changed = true;
                    }
                    break;
                case AbsentKanoFeelRole:
                    if (static_cast<int>(ticket->getAbsentKanoFeel()) != value.toInt())
                    {
                        ticket->setAbsentKanoFeel(static_cast<Ticket::KanoFeel>(value.toInt()));
                        changed = true;
                    }
                    break;
                case StoryPointsRole:
                    if (ticket->getStoryPoints() != value.toFloat())
                    {
                        ticket->setStoryPoints(value.toFloat());
                        changed = true;
                    }
                    break;
                case DueDateRole:
                    if (ticket->getDueDate() != value.toDateTime())
                    {
                        ticket->setDueDate((value.toDateTime()));
                        changed = true;
                    }
                    break;
                case PriorityRole:
                    if (static_cast<int>(ticket->getPriority()) != value.toInt())
                    {
                        ticket->setPriority(static_cast<Ticket::Priority>(value.toInt()));
                        changed = true;
                    }
                    break;
                case KanbanColumnRole:
                    if(ticket->getKanbanColumn() != value.toInt())
                    {
                        ticket->setKanbanColumn(value.toInt());
                        changed = true;
                    }
                    break;
                case KanbanPositionRole:
                    if(ticket->getKanbanPosition() != value.toInt())
                    {
                        ticket->setKanbanPosition(value.toInt());
                        changed = true;
                    }
                    break;
                case InNextSprintRole:
                    if(ticket->getInNextSprint() != value.toBool())
                    {
                        ticket->setInNextSprint(value.toBool());
                        changed = true;
                    }
                    break;
                case SprintIdRole:
                    qCritical() << "Not yet implemented";
                    break;
                case ParentIdRole:
                    qCritical() << "Not yet implemented";
                    break;
                case SelectedRole:
                    if(!value.toBool() && m_dataSource->getSelected() == ticket)
                    {
                        m_dataSource->setSelected(nullptr, QModelIndex());
                        emit dataChanged(index, index, {role});
                        return true;
                    }
                    else if(value.toBool() && m_dataSource->getSelected() != ticket)
                    {
                        auto previousTicketIndex = m_dataSource->getSelectedIndex();
                        m_dataSource->setSelected(ticket, index);
                        emit dataChanged(index, index, {role});
                        emit dataChanged(previousTicketIndex, previousTicketIndex, {role});
                        return true;
                    }
                    break;
            }

            if(changed)
            {
                m_dataSource->update(ticket);
                return true;
            }
        }
        return false;
    }

    Qt::ItemFlags TicketModel::flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return Qt::NoItemFlags;

        return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
    }

    QHash<int, QByteArray> TicketModel::roleNames() const
    {
        QHash<int, QByteArray> roles;
        roles[IdRole] = "id";
        roles[ProjectIdRole] = "projectId";
        roles[TitleRole] = "title";
        roles[DescriptionRole] = "description";
        roles[PositionRole] = "position";
        roles[PresentKanoFeelRole] = "presentKanoFeel";
        roles[AbsentKanoFeelRole] = "absentKanoFeel";
        roles[StoryPointsRole] = "storyPoints";
        roles[DueDateRole] = "dueDate";
        roles[PriorityRole] = "priority";
        roles[KanbanColumnRole] = "kanbanColumn";
        roles[KanbanPositionRole] = "kanbanPosition";
        roles[InNextSprintRole] = "inNextSprint";
        roles[SprintIdRole] = "sprintId";
        roles[ParentIdRole] = "parentId";
        roles[SelectedRole] = "selected";
        return roles;
    }

    void TicketModel::move(int from, int to, Project *project)
    {
        Q_ASSERT(project != nullptr);

        if(from < 0 || from >= rowCount() || to < 0 || to >= rowCount() || from == to)
            return;

        if(from == to - 1)// Allow item moving to the bottom
            to = from++;
        beginMoveRows(QModelIndex(), from, from, QModelIndex(), to);
        m_dataSource->move(from, to, project);
        endMoveRows();
    }

    void TicketModel::resetSelected() {
        auto previousSelected = m_dataSource->getSelectedIndex();
        m_dataSource->setSelected(nullptr, QModelIndex());
        emit dataChanged(previousSelected, previousSelected, {SelectedRole});
    }

    TicketDataSource *TicketModel::getDataSource() const
    {
        return m_dataSource;
    }

    void TicketModel::setDataSource(TicketDataSource *newDataSource)
    {
        beginResetModel();
        if(m_dataSource != nullptr)
            m_dataSource->disconnect(this);
        m_dataSource = newDataSource;
        auto& sig = m_dataSource->getSignals();
        if(m_dataSource != nullptr)
        {
            connect(&sig, &TicketDataSourceSignals::preItemAppended, this, [this]()
            {
                const int index = m_dataSource->getAll().count();
                beginInsertRows(QModelIndex(), index, index);
            });
            connect(&sig, &TicketDataSourceSignals::postItemAppended, this, [this]()
            {
                endInsertRows();
            });
            connect(&sig, &TicketDataSourceSignals::preItemRemoved, this, [this](int index)
            {
                beginRemoveRows(QModelIndex(), index, index);
            });
            connect(&sig, &TicketDataSourceSignals::postItemRemoved, this, [this]()
            {
                endRemoveRows();
            });
            connect(&sig, &TicketDataSourceSignals::preItemsReset, this, [this]()
            {
               beginResetModel();
            });
            connect(&sig, &TicketDataSourceSignals::postItemsReset, this, [this]()
            {
                endResetModel();
            });
            connect(&sig, &TicketDataSourceSignals::itemChanged, this, [this](int id)
            {
                emit dataChanged(index(id, 0), index(id, 0));
            });
        }
        endResetModel();
    }

} // namespace Synoptic
