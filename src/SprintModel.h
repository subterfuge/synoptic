#pragma once

#include <QAbstractListModel>
#include <QtQml/qqmlregistration.h>

namespace Synoptic
{
    class SprintDataSource;

    class SprintModel : public QAbstractListModel
    {
        Q_OBJECT

        Q_PROPERTY(SprintDataSource *dataSource READ getDataSource WRITE setDataSource NOTIFY dataSourceChanged)
        QML_ELEMENT
    public:
        enum SprintRoles
        {
            IdRole = Qt::UserRole,
            ClosedRole,
            SprintGoalRole,
            StartDateRole,
            EndDateRole,
            ProductGoalRole,
            WorkspaceIdRole,
        };
        Q_ENUM(SprintRoles)

        explicit SprintModel(QObject *parent = nullptr);

        int rowCount(const QModelIndex &parent = QModelIndex()) const override;
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
        QHash<int, QByteArray> roleNames() const override;

        Q_INVOKABLE void update();

        SprintDataSource* getDataSource() const;
        void setDataSource(SprintDataSource *newDataSource);

    signals:
        void dataSourceChanged();

    private:
        SprintDataSource *m_dataSource;
    };
}
