#include <QSqlQuery>
#include "DefinitionOfDoneDataSource.h"

#include <QSqlQuery>
#include "DbManager.h"

namespace Synoptic
{
    QVector<DefinitionOfDoneItem> DefinitionOfDoneDataSource::get(const QString &projectId)
    {
        QVector<DefinitionOfDoneItem> result;
        QSqlQuery query;
        query.prepare(DbManager::SELECT_PROJECT_DOD_SQL);
        query.bindValue(":projectId", projectId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error querying" << Synoptic::PROJECT_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            return result;
        }

        while(query.next())
        {
            DefinitionOfDoneItem item;
            item.id = query.value(0).toInt();
            item.description = query.value(1).toString();
            item.checked = false;
            result.append(item);
        }
        return result;
    }

    QVector<DefinitionOfDoneItem> DefinitionOfDoneDataSource::get(const QString &projectId, int ticketId)
    {
        QVector<DefinitionOfDoneItem> result;
        QSqlQuery query;
        query.prepare(DbManager::SELECT_TICKET_DOD_SQL);
        query.bindValue(":projectId", projectId);
        query.bindValue(":ticketId", ticketId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error querying" << Synoptic::TICKET_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            return result;
        }

        while(query.next())
        {
            DefinitionOfDoneItem item;
            item.id = query.value(0).toInt();
            item.description = query.value(1).toString();
            item.checked = query.value(2).toBool();
            result.append(item);
        }
        return result;
    }

    bool DefinitionOfDoneDataSource::insert(const QString &description, const QString &projectId)
    {
        QSqlQuery query;
        query.prepare(DbManager::INSERT_PROJECT_DOD_SQL);
        query.bindValue(":description", description);
        query.bindValue(":projectId", projectId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error inserting" << Synoptic::PROJECT_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            return false;
        }

        emit getSignals()->projectDodItemAppended();
        return true;
    }

    bool DefinitionOfDoneDataSource::insert(const QString &description, int ticketId, const QString &projectId)
    {
        QSqlQuery query;
        query.prepare(DbManager::INSERT_TICKET_DOD_SQL);
        query.bindValue(":description", description);
        query.bindValue(":checked", false);
        query.bindValue(":ticketId", ticketId);
        query.bindValue(":projectId", projectId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error inserting" << Synoptic::TICKET_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            return false;
        }

        emit getSignals()->ticketDodItemAppended(ticketId, projectId);
        return true;
    }

    bool DefinitionOfDoneDataSource::update(const int id, const QString &description)
    {
        return false;
    }

    bool DefinitionOfDoneDataSource::update(const int id, const QString &description, bool checked)
    {
        QSqlQuery query;
        query.prepare(DbManager::UPDATE_TICKET_DOD_SQL);
        query.bindValue(":id", id);
        query.bindValue(":description", description);
        query.bindValue(":checked", checked);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error updating" << Synoptic::TICKET_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            return false;
        }
        emit getSignals()->ticketDodItemChanged(id);
        return true;
    }

    bool DefinitionOfDoneDataSource::deleteProjectDefaultItem(int id)
    {
        return false;
    }

    bool DefinitionOfDoneDataSource::clear(int ticketId, const QString &projectId)
    {
        QSqlQuery query;
        query.prepare(DbManager::CLEAR_TICKET_DOD_SQL);
        query.bindValue(":ticketId", ticketId);
        query.bindValue(":projectId", projectId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error deleting" << TICKET_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            return false;
        }

        return true;
    }

    bool DefinitionOfDoneDataSource::uncheckAll(int ticketId, const QString &projectId)
    {
        QSqlQuery query;
        query.prepare(DbManager::UNCHECK_ALL_TICKET_DOD_SQL);
        query.bindValue(":ticketId", ticketId);
        query.bindValue(":projectId", projectId);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            qCritical() << "Error unchecking" << TICKET_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            return false;
        }

        emit getSignals()->ticketDodUpdated(ticketId, projectId);
        return true;
    }

    DefinitionOfDoneSignals *DefinitionOfDoneDataSource::getSignals()
    {
        static DefinitionOfDoneSignals definitionOfDoneSignals {};
        return &definitionOfDoneSignals;
    }

    DefinitionOfDoneItem DefinitionOfDoneDataSource::getItem(int id, DefinitionOfDoneDataSource::Type type)
    {
        QSqlQuery query;
        if(type == Type::Ticket)
            query.prepare("SELECT id, description, checked FROM " + Synoptic::TICKET_DEFINITION_OF_DONE_TABLE_NAME +
                    " WHERE id = :id;");
        else
            query.prepare("SELECT id, description FROM " + Synoptic::PROJECT_DEFINITION_OF_DONE_TABLE_NAME +
                    " WHERE id = :id;");
        query.bindValue(":id", id);
        query.exec();
        auto error = query.lastError();
        if(error.type() != QSqlError::NoError)
        {
            if(type == Type::Ticket)
                qCritical() << "Error fetching" << Synoptic::TICKET_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
            else
                qCritical() << "Error fetching" << Synoptic::PROJECT_DEFINITION_OF_DONE_TABLE_NAME << ":" << error;
        }
        query.first();
        if(type == Type::Ticket)
            return DefinitionOfDoneItem { id, query.value(1).toString(), query.value(2).toBool() };
        else
            return DefinitionOfDoneItem { id, query.value(1).toString(), false };
    }
} // Synoptic
