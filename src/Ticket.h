#ifndef TICKET_H
#define TICKET_H

#include <QObject>
#include <QtQml/qqmlregistration.h>
#include "Project.h"
#include "Sprint.h"

namespace Synoptic
{
    class Ticket : public QObject
    {
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(int id READ getId WRITE setId NOTIFY idChanged FINAL)
    Q_PROPERTY(Project* project READ getProject WRITE setProject NOTIFY projectChanged FINAL)
    Q_PROPERTY(QString title READ getTitle WRITE setTitle NOTIFY titleChanged FINAL)
    Q_PROPERTY(int position READ getPosition WRITE setPosition NOTIFY positionChanged FINAL)
    Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged FINAL)
    Q_PROPERTY(KanoFeel presentKanoFeel READ getPresentKanoFeel WRITE setPresentKanoFeel NOTIFY presentKanoFeelChanged FINAL)
    Q_PROPERTY(KanoFeel absentKanoFeel READ getAbsentKanoFeel WRITE setAbsentKanoFeel NOTIFY absentKanoFeelChanged FINAL)
    Q_PROPERTY(float storyPoints READ getStoryPoints WRITE setStoryPoints NOTIFY storyPointsChanged FINAL)
    Q_PROPERTY(QDateTime dueDate READ getDueDate WRITE setDueDate NOTIFY dueDateChanged FINAL)
    Q_PROPERTY(Priority priority READ getPriority WRITE setPriority NOTIFY priorityChanged FINAL)
    Q_PROPERTY(int kanbanColumn READ getKanbanColumn WRITE setKanbanColumn NOTIFY kanbanColumnChanged FINAL)
    Q_PROPERTY(int kanbanPosition READ getKanbanPosition WRITE setKanbanPosition NOTIFY kanbanPositionChanged FINAL)
    Q_PROPERTY(bool inNextSprint READ getInNextSprint WRITE setInNextSprint NOTIFY inNextSprintChanged FINAL)
    Q_PROPERTY(Sprint* sprint READ getSprint WRITE setSprint NOTIFY sprintChanged FINAL)
    Q_PROPERTY(Ticket* parent READ getParent WRITE setParent NOTIFY parentChanged FINAL)

    public:
        explicit Ticket(QObject *parent = nullptr);

        enum class KanoFeel
        {
            None = 0,
            Disliked,
            Accepted,
            Neutral,
            Expected,
            Liked
        };
        Q_ENUM(KanoFeel)

        enum class KanoCategory
        {
            None = 0,
            Questionable,
            Reversed,
            Indifferent,
            Attractive,
            Performance,
            MustHave
        };
        Q_ENUM(KanoCategory)

        enum class Priority
        {
            Low = 0,
            Medium,
            High,
            Urgent
        };
        Q_ENUM(Priority)

        int getId() const;
        void setId(int newId);

        Project *getProject() const;
        void setProject(Project *newProject);

        QString getTitle() const;
        void setTitle(const QString &newTitle);

        int getPosition() const;
        void setPosition(int newPosition);

        QString getDescription() const;
        void setDescription(const QString &newDescription);

        KanoFeel getPresentKanoFeel() const;
        void setPresentKanoFeel(KanoFeel newPresentKanoFeel);

        KanoFeel getAbsentKanoFeel() const;
        void setAbsentKanoFeel(KanoFeel newAbsentKanoFeel);

        float getStoryPoints() const;
        void setStoryPoints(float newStoryPoints);

        QDateTime getDueDate() const;
        void setDueDate(const QDateTime &newDueDate);

        Priority getPriority() const;
        void setPriority(Priority newPriority);

        int getKanbanColumn() const;
        void setKanbanColumn(int newKanbanColumn);

        int getKanbanPosition() const;
        void setKanbanPosition(int newKanbanPosition);

        bool getInNextSprint() const;
        void setInNextSprint(bool newInNextSprint);

        Sprint *getSprint() const;
        void setSprint(Sprint *newSprint);

        Ticket *getParent() const;
        void setParent(Ticket *newParent);

    signals:
        void idChanged();
        void projectChanged();
        void titleChanged();
        void positionChanged();
        void descriptionChanged();
        void presentKanoFeelChanged();
        void absentKanoFeelChanged();
        void storyPointsChanged();
        void dueDateChanged();
        void priorityChanged();
        void kanbanColumnChanged();
        void kanbanPositionChanged();
        void inNextSprintChanged();
        void sprintChanged();
        void parentChanged();

    private:
        int m_id = 0;
        Project *m_project = nullptr;
        QString m_title;
        int m_position = 0;
        QString m_description;
        KanoFeel m_presentKanoFeel = KanoFeel::None;
        KanoFeel m_absentKanoFeel = KanoFeel::None;
        float m_storyPoints = 0.F;
        QDateTime m_dueDate;
        Priority m_priority = Priority::Medium;
        int m_kanbanColumn = 0;
        int m_kanbanPosition = 0;
        bool m_inNextSprint = false;
        Sprint *m_sprint = nullptr;
        Ticket *m_parent = nullptr;
    };
} // namespace Synoptic

#endif // TICKET_H
