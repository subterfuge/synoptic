#include "CalendarEventModel.h"

Synoptic::CalendarEventModel::CalendarEventModel(QObject *parent)
    : QAbstractListModel(parent), m_dataSource(nullptr)
{}

int Synoptic::CalendarEventModel::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid() || (m_dataSource == nullptr))
        return 0;

    return m_events.count();
}

QVariant Synoptic::CalendarEventModel::data(const QModelIndex &index, int role) const
{
    if(!checkIndex(index, CheckIndexOption::IndexIsValid))
        return QVariant();

    switch(role)
    {
        case NameRole:
            return m_events[index.row()].name;
        case TypeRole:
            return static_cast<int>(m_events[index.row()].type);
        case DescriptionRole:
            return m_events[index.row()].description;
        case StartDateRole:
            return m_events[index.row()].startDate;
        case EndDateRole:
            return m_events[index.row()].endDate;
        default:
            return QVariant();
    }
}

QHash<int, QByteArray> Synoptic::CalendarEventModel::roleNames() const
{
    static const QHash<int, QByteArray> roles {
            { NameRole, "name" },
            { TypeRole, "type" },
            { DescriptionRole, "description" },
            { StartDateRole, "startDate" },
            { EndDateRole, "endDate" },
    };
    return roles;
}

void Synoptic::CalendarEventModel::update()
{
    if(m_dataSource == nullptr || m_date.isNull())
    {
        m_events.clear();
        return;
    }

    m_events = m_dataSource->get(m_date);
}

QDate Synoptic::CalendarEventModel::getDate() const
{
    return m_date;
}

void Synoptic::CalendarEventModel::setDate(const QDate &newDate)
{
    if(m_date == newDate)
        return;

    m_date = newDate;
    update();
    emit dateChanged();
}

Synoptic::CalendarEventDataSource* Synoptic::CalendarEventModel::getDataSource() const
{
    return m_dataSource;
}

void Synoptic::CalendarEventModel::setDataSource(Synoptic::CalendarEventDataSource *newDataSource)
{
    if(m_dataSource == newDataSource)
        return;

    beginResetModel();
    m_dataSource = newDataSource;
    update();
    connect(m_dataSource, &CalendarEventDataSource::resetItems, this, [this]()
    {
       beginResetModel();
       update();
       endResetModel();
    });
    emit dataSourceChanged();
    endResetModel();
}
