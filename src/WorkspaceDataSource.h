#pragma once

#include <QObject>
#include <QSqlError>
#include <QtQml/qqmlregistration.h>
#include "Workspace.h"

namespace Synoptic
{
    class WorkspaceDataSource : public QObject
    {
        Q_OBJECT

        QML_ELEMENT
        QML_SINGLETON
    public:
        explicit WorkspaceDataSource(QObject *parent = nullptr);

        static QList<Workspace*>& getAll();

        Q_INVOKABLE QSqlError update(int index, Workspace *workspace);

        static int getNextId();

        Q_INVOKABLE Workspace* get(int id) const;

        Q_INVOKABLE bool any() const;

        Q_INVOKABLE QSqlError insert(Synoptic::Workspace *newWorkspace);
        Q_INVOKABLE QSqlError insert(const QString &title, int sprintDuration,
                int storyPointEstimation);

        void init();

    signals:
        void preItemAppended();
        void postItemAppended();
        void preItemRemoved(int index);
        void postItemRemoved();
        void itemChanged(int index);

    private:
        static bool m_initialized;
    };
} // namespace Synoptic
