import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import style.SynopticStyle
import Components as C

Popup {
    id: newProjectPopup

    anchors.centerIn: parent
    width: 370
    height: 230
    padding: 0

    Rectangle {
        anchors.fill: parent
        color: C.Palette.smokeGreen

        C.CrossButton {
            id: closeRect
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 5
            anchors.topMargin: 5
            onClicked: newProjectPopup.close()
        }

        Rectangle {
            anchors.top: parent.top
            anchors.topMargin: -30
            width: title.width
            height: title.height
            color: C.Palette.background

            Text {
                id: title
                text: qsTr("New Workspace")
                color: C.Palette.front
                font { family: C.CustomFont.mordak; pixelSize: 26 }
            }
        }

        ColumnLayout {
            id: controlsLayout
            x: 10
            y: 10
            width: parent.width - 20

            TextField {
                id: titleField
                Layout.preferredWidth: 280
                Layout.preferredHeight: 40
                placeholderText: qsTr("Title")
                onTextEdited: {
                    color = C.Palette.front
                }

                C.Alert {
                    id: titleFieldAlert
                    anchors.top: parent.bottom
                    anchors.topMargin: 5
                    visible: false
                    text: ""
                    type: C.Alert.Danger
                }
            }

            Text {
                Layout.topMargin: 40
                text: qsTr("Defaut sprint duration")
                font: C.CustomFont.defaultTextFont
                color: C.Palette.front
            }

            SpinBox {
                id: sprintDurationField
                from: 0
                to: 200
                value: 14
                editable: true

                Text {
                    anchors.left: parent.right
                    anchors.leftMargin: 5
                    y: 2
                    text: qsTr("days")
                    font: C.CustomFont.defaultTextFont
                    color: C.Palette.front
                }
            }

            Text {
                Layout.topMargin: 40
                text: qsTr("Story point estimation")
                font: C.CustomFont.defaultTextFont
                color: C.Palette.front
            }

            C.TimePicker {
                id: storyPointEstimation
                model: [ "0:30", "1:00", "2:30", "5:00", "7:00" ]
            }

            C.Button {
                id: submitBtn
                Layout.alignment: Qt.AlignHCenter
                Layout.topMargin: 30
                text: qsTr("Submit")
                onClicked: {
                    if(titleField.text.trim() === "")
                    {
                        titleAlert(qsTr("Title shouldn't be empty"))
                        titleField.color = C.Palette.red
                        return
                    }

                    let storyPointEstimationResult = storyPointEstimation.checkInput()
                    if(storyPointEstimationResult.result) {
                        let result = WorkspaceDataSource.insert(titleField.text, sprintDurationField.value,
                            (storyPointEstimationResult.hours * 60)
                            + parseInt(storyPointEstimationResult.minutes))
                    }
                    newProjectPopup.close()
                }
            }
        }
    }

    function titleAlert(message) {
        titleFieldAlert.text = message
        titleFieldAlert.type = C.Alert.Danger
        titleFieldAlert.visible = true
    }

    function reset() {
        titleField.text =  ""
        titleFieldAlert.text = ""
        titleFieldAlert.visible = false
        sprintDurationField.value = 14
    }

    onClosed: {
        reset()
    }
}
