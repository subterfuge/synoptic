import QtQuick 2.15
import QtQuick.Layouts
import Synoptic
import style.SynopticStyle
import Components as C

Item {
    id: root
    implicitWidth: 350
    implicitHeight: 350

    signal close
    property Workspace workspace: null
    property int index: 0

    function titleAlert(message) {
        titleFieldAlert.text = message
        titleFieldAlert.type = C.Alert.Danger
        titleFieldAlert.visible = true
    }

    function reset() {
        titleFieldAlert.text = ""
        titleFieldAlert.visible = false
        root.workspace = null
    }

    onClose: {
        reset()
    }

    onWorkspaceChanged: {
        if(root.workspace === null)
            return

        let mins = root.workspace.storyPointEstimation
        let time = new Date()
        let hours = Math.floor(mins / 60)
        time.setHours(hours)
        mins = mins - hours * 60
        time.setMinutes(mins)
        storyPointEstimation.setTime(time)
    }

    Text {
        anchors.top: parent.top
        anchors.topMargin: -30
        text: qsTr("Edit workspace")
        color: C.Palette.front
        font { family: C.CustomFont.mordak; pixelSize: 26 }
    }

    Rectangle {
        id: background
        anchors.fill: parent
        color: C.Palette.smokeGreen
    }

    C.CrossButton {
        id: closeBtn
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 5
        anchors.topMargin: 5
        onClicked: root.close()
    }

    ColumnLayout {
        id: controlsLayout
        x: 10
        y: 10
        width: parent.width - 20

        TextField {
            id: titleField
            Layout.preferredWidth: 280
            Layout.preferredHeight: 40
            placeholderText: qsTr("Title")
            text: root.workspace === null ? "" : root.workspace.title
            onTextEdited: {
                color = C.Palette.front
            }

            C.Alert {
                id: titleFieldAlert
                anchors.top: parent.bottom
                anchors.topMargin: 5
                visible: false
                text: ""
                type: C.Alert.Danger
            }
        }

        Text {
            Layout.topMargin: 40
            text: qsTr("Defaut sprint duration")
            font: C.CustomFont.defaultTextFont
            color: C.Palette.front
        }

        SpinBox {
            id: sprintDurationField
            from: 0
            to: 200
            value: root.workspace === null ? 0 : root.workspace.sprintDuration
            editable: true

            Text {
                anchors.left: parent.right
                anchors.leftMargin: 5
                y: 2
                text: qsTr("days")
                font: C.CustomFont.defaultTextFont
                color: C.Palette.front
            }
        }

        Text {
            Layout.topMargin: 40
            text: qsTr("Story point estimation")
            font: C.CustomFont.defaultTextFont
            color: C.Palette.front
        }

        C.TimePicker {
            id: storyPointEstimation
            model: [ "0:30", "1:00", "2:30", "5:00", "7:00" ]
        }

        C.Button {
            id: submitBtn
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: 30
            text: qsTr("Submit")
            onClicked: {
                if(titleField.text.trim() === "")
                {
                    titleAlert(qsTr("Title shouldn't be empty"))
                    titleField.color = C.Palette.red
                    return
                }

                let storyPointEstimationResult = storyPointEstimation.checkInput()
                if(storyPointEstimationResult.result) {
                    root.workspace.title = titleField.text
                    root.workspace.sprintDuration = sprintDurationField.value
                    root.workspace.storyPointEstimation = (storyPointEstimationResult.hours * 60)
                            + parseInt(storyPointEstimationResult.minutes)
                    let result = WorkspaceDataSource.update(root.index, root.workspace)
                }
                root.close()
            }
        }
    }
}
