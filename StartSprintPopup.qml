import QtQuick
import QtQuick.Layouts
import Synoptic
import style.SynopticStyle
import Components as C

Popup {
    id: root

    anchors.centerIn: Overlay.overlay
    padding: 0
    Overlay.modal: AppModalRectangle {}
    implicitWidth: 500
    implicitHeight: 270

    onClosePolicyChanged: {
        if(closePolicy === Popup.NoAutoClose)
            closeBtn.visible = false
        else
            closeBtn.visible = true
    }

    Rectangle {
        anchors.centerIn: parent
        width: parent.width
        height: parent.height
        color: C.Palette.smokeGreen

        Text {
            anchors.top: parent.top
            anchors.topMargin: -30
            text: qsTr("Start sprint")
            color: C.Palette.front
            font { family: C.CustomFont.mordak; pixelSize: 26 }
        }

        C.CrossButton {
            id: closeBtn
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 5
            onClicked: {
                root.close()
            }
        }

        ColumnLayout {
            anchors.fill: parent
            anchors.margins: 10

            Text {
                verticalAlignment: Text.AlignVCenter
                text: qsTr("End of sprint")
                color: C.Palette.front
                font: C.CustomFont.defaultTextFont
            }

            C.DatePicker {
                id: endDatePicker
                Layout.topMargin: 5
                selectedDate: new Date((new Date()).setHours(0, 0, 0) +
                                       WorkspaceDataSource.get(Settings.getWorkspace()).sprintDuration *
                                       86400000 /*ms per day*/)

                Text {
                    anchors.left: parent.right
                    anchors.leftMargin: 5
                    height: parent.height
                    text: qsTr("(%1 day(s) from now)").arg(
                              Math.round((endDatePicker.selectedDate - new Date().setHours(0, 0, 0)) / 86400000))
                    verticalAlignment: Text.AlignVCenter
                    font: C.CustomFont.defaultTextFont
                    color: C.Palette.front
                }
            }

            Text {
                Layout.topMargin: 20
                verticalAlignment: Text.AlignVCenter
                text: qsTr("Sprint goal")
                color: C.Palette.front
                font: C.CustomFont.defaultTextFont
            }

            TextArea {
                id: sprintGoalField
                Layout.topMargin: 5
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            C.Button {
                Layout.topMargin: 20
                Layout.alignment: Qt.AlignHCenter
                text: qsTr("Start sprint")
                onClicked: {
                    let newSprintId = SprintDataSource.insert(sprintGoalField.text, new Date(),
                                                     endDatePicker.selectedDate, "", Settings.getWorkspace())
                    if(newSprintId > 0) {
                        Settings.currentSprint = newSprintId
                        root.close()
                    }
                }
            }
        }
    }
}
