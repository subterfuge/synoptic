import QtQuick
import Components as C

Rectangle {
    id: root
    color: C.Palette.grey

    signal projectClicked()
    signal boardClicked()
    signal kanbanClicked()
    signal calendarClicked()
    signal settingsClicked()

    Column {
        id: topColum
        anchors.top: parent.top

        SideButton {
            icon: String.fromCodePoint(0xeb9b) // Material icon "Rocket Launch"
            onClicked: projectClicked()
        }

        SideButton {
            icon: String.fromCodePoint(0xe9b9) // Material icon "Lists"
            onClicked: boardClicked()
        }

        SideButton {
            icon: String.fromCodePoint(0xeb7f)
            onClicked: kanbanClicked()
        }

        SideButton {
            icon: String.fromCodePoint(0xebcc)
            onClicked: calendarClicked()
        }
    }

    Column {
        id: bottomColumn
        anchors.bottom: parent.bottom

        SideButton {
            icon: String.fromCodePoint(0xe8b8) // Material icon "Settings"
            onClicked: settingsClicked()
        }
    }

    component SideButton: MouseArea {
        property alias icon: sideBtnImg.text

        width: root.width
        height: root.width
        cursorShape: Qt.PointingHandCursor
        hoverEnabled: true

        onEntered: {
            sideBtnBg.color = C.Palette.greenLight
            sideBtnImg.color = C.Palette.background
        }
        onExited: {
            sideBtnBg.color = "transparent"
            sideBtnImg.color = C.Palette.front
        }

        Rectangle {
            id: sideBtnBg
            anchors.fill: parent
            color: "transparent"
            border.color: "transparent"
        }

        Text {
            id: sideBtnImg
            anchors.centerIn: parent
            color: C.Palette.front
            font { family: C.CustomFont.material; pixelSize: parent.width - 10 }
        }
    }
}
