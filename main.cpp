#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QFileInfo>
#include <QSortFilterProxyModel>
#include <QIcon>
#include "src/Utils.h"
#include "src/DbManager.h"
#include "src/ProjectModel.h"
#include "src/TimeEntryDataSource.h"

void logfileOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    const char *file = context.file ? context.file : "";
    const char *function = context.function ? context.function : "";
    QList<QString> additionalInfo;
    if(context.file != nullptr)
        additionalInfo.push_back("in " + QString(context.file));
    if(context.function != nullptr)
        additionalInfo.push_back("function: " + QString(context.function));
    static QFile logfile = QFile(QDate::currentDate().toString("yyyy_MM_dd") + ".log");
    static bool fileOpened = false;
    if(!fileOpened)
    {
        if(!logfile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
            return;

        fileOpened = true;
    }
    QTextStream fileStream = QTextStream(&logfile);
    QTextStream consoleStream = QTextStream(stdout);
    QTextStream errorStream = QTextStream(stderr);
    const QString defaultColor = "\033[0m";
    const QString green = "\033[0;32m";
    const QString blue = "\033[0;34m";
    const QString yellow = "\033[1;33m";
    const QString red = "\033[0;31m";
    const QString dbgTagC = "[" + blue + "DBG" + defaultColor + "]: ";
    const QString infTagC = "[" + green + "INF" + defaultColor + "]: ";
    const QString infTagF = "[INF]:\t";
    const QString wrnTagC = "[" + yellow + "WRN" + defaultColor + "]: ";
    const QString wrnTagF = "[WRN]:\t";
    const QString errTagC = "[" + red + "ERR" + defaultColor + "]: ";
    const QString errTagF = "[ERR]:\t";
    const QString ftlTagC = "[" + red + "FTL" + defaultColor + "]: ";
    const QString ftlTagF = "[FTL]:\t";
    switch (type) {
        case QtDebugMsg:
            consoleStream << dbgTagC << localMsg.constData() << "\n";
            break;
        case QtInfoMsg:
            fileStream << infTagF << localMsg.constData() << "\n";
            consoleStream << infTagC << localMsg.constData() << "\n";
            break;
        case QtWarningMsg:
            fileStream << wrnTagF << localMsg.constData() << "\n";
            consoleStream << wrnTagC<< localMsg.constData() << "\n";
            break;
        case QtCriticalMsg:
            fileStream << errTagF << localMsg.constData() << "\n";
            errorStream << errTagC << localMsg.constData() << "\n";
            break;
        case QtFatalMsg:
            fileStream << ftlTagF << localMsg.constData() << "\n";
            errorStream << ftlTagC << localMsg.constData() << "\n";
            abort();
    }
}

int main(int argc, char *argv[])
{
    qInstallMessageHandler(logfileOutput);
    QCoreApplication::setOrganizationName("Subterfuge");
    QCoreApplication::setApplicationName("Synoptic");
    auto appDataDir = QDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    appDataDir.mkpath(".");
#ifdef QT_DEBUG
    Synoptic::dbName = appDataDir.filePath("database_debug.sqlite");
#else
    Synoptic::dbName = appDataDir.filePath("database.sqlite");
#endif
    QGuiApplication app(argc, argv);

    QIcon::setThemeName("synoptic");

    Synoptic::DbManager dbManager;
    dbManager.init();

    Synoptic::Utils &utils = Synoptic::Utils::getInstance();
    Synoptic::WorkspaceDataSource workspaceDataSource;
    Synoptic::ProjectDataSource projectDataSource;
    Synoptic::TimeEntryDataSource timeEntryDataSource;

    Synoptic::WorkspaceModel &workspaceModel = utils.getWorkspaceModel();
    workspaceModel.setDataSource(&workspaceDataSource);
    Synoptic::ProjectModel &projectModel = utils.getProjectModel();
    projectModel.setDataSource(&projectDataSource);

    QQmlApplicationEngine engine;
    utils.setEngine(&engine);

    auto &settings = utils.getSettings();
    settings.reloadTranslation(settings.getLocale());

    engine.addImportPath(":/");
    engine.addImportPath("style");
    engine.rootContext()->setContextProperty("utils", &utils);
    engine.rootContext()->setContextProperty("projectDataSource", &projectDataSource);
    engine.rootContext()->setContextProperty("projectModel", &projectModel);
    engine.rootContext()->setContextProperty("calendarEventDataSource", &utils.getCalendarEventDataSource());
    engine.rootContext()->setContextProperty("timeEntryDataSource", &timeEntryDataSource);
    const QUrl url(u"qrc:/qt/qml/Synoptic/Main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreationFailed,
        &app, []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.load(url);

    return QGuiApplication::exec();
}
