import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Components as C

Item {
    id: stopWatch

    property alias running: timer.running
    property date startTime: new Date()
    property int minutes: 0
    property int seconds: 0
    property int elapsedTimeMs: 0
    property color color: baseColor
    property color baseColor: Qt.rgba(1, 0.471, 0.212, 0.631)
    property color runningColor: "#fe3550"
    property color pausedColor: "#00c0a4"
    property real glowBlur: 0.0
    property real glowSpeed: 0.0

    signal clicked()

    QtObject {
        id: internal
        property date previousTime
        property bool alertIfRunningForAWhile: true
        readonly property double alertAfterMinutes: 60
        property int lastAlertAt: 0
    }

    function reset() {
        timer.running = false
        stopWatch.minutes = 0
        stopWatch.seconds = 0
        stopWatch.elapsedTimeMs = 0
        internal.lastAlertAt = 0
        stopWatch.startTime = new Date()
        pauseIcon.visible = false
        stopIcon.visible = false
        playIcon.visible = true
        stopWatch.color = stopWatch.baseColor
        stopWatch.glowBlur = 0.0
    }

    function updateTime() {
        let now = new Date()
        let delta = now - internal.previousTime
        stopWatch.elapsedTimeMs += delta
        if(internal.alertIfRunningForAWhile === true)
            stopWatch.updateAlert(delta)
        internal.previousTime = now
        stopWatch.minutes = Math.floor(elapsedTimeMs / 60000)
        stopWatch.seconds = Math.floor(elapsedTimeMs / 1000) % 60
    }

    function updateAlert(delta) {
        internal.lastAlertAt += delta
        if((internal.lastAlertAt / 60000) < internal.alertAfterMinutes)
            return

        C.NotificationSystem.push(qsTr("Timer running"),
                                  qsTr("The timer is running for a while. Don't forget to take a break."),
                                  C.NotificationSystem.TypeBasic)
        internal.alertIfRunningForAWhile = false
    }

    function start() {
        if(timer.running)
            return

        internal.previousTime = new Date()
        timer.running = true
        pauseIcon.visible = false
        stopIcon.visible = true
        playIcon.visible = false
        stopWatch.color = stopWatch.runningColor
        stopWatch.glowBlur = 0.25
        internal.alertIfRunningForAWhile = true
    }

    function pause() {
        timer.running = false
        pauseIcon.visible = false
        stopIcon.visible = false
        playIcon.visible = true
        stopWatch.color = stopWatch.pausedColor
        stopWatch.glowBlur = 0.2
        internal.lastAlertAt = 0
    }

    implicitWidth: 300
    implicitHeight: imageLeft.height + 20

    layer.enabled: true

    layer.effect: ShaderEffect {
        readonly property Item iSource: stopWatch
        property real iTime: frameAnimation.elapsedTime
        readonly property Item iSourceBlur1: blurHelper.blurSrc1
        readonly property Item iSourceBlur2: blurHelper.blurSrc2
        readonly property Item iSourceBlur3: blurHelper.blurSrc3
        readonly property Item iSourceBlur4: blurHelper.blurSrc4
        readonly property Item iSourceBlur5: blurHelper.blurSrc5
        readonly property real blurMultiplier: 0.0
        property real fastBlurAmount: stopWatch.glowBlur
        property real speed: stopWatch.glowSpeed
        property color colorFilter: stopWatch.color

        onSpeedChanged: {
            frameAnimation.running = speed !== 0.0;
        }

        vertexShader: '/shaders/glow.vert.qsb'
        fragmentShader: '/shaders/glow.frag.qsb'
        implicitHeight: stopWatch.implicitHeight
        implicitWidth: stopWatch.implicitWidth

        FrameAnimation {
            id: frameAnimation
            running: true
        }
    }

    BlurHelper {
        id: blurHelper
        anchors.fill: parent
        source: stopWatch
        property int blurMax: 64
        property real blurMultiplier: 0.0
    }

    Timer {
        id: timer
        interval: 100
        repeat: true
        running: false
        onTriggered: stopWatch.updateTime()
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: {
            if(stopWatch.running)
                stopWatch.pause()
            else
                stopWatch.start()
            stopWatch.clicked()
        }
    }

    Image {
        id: imageLeft
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        source: "/image/stopwatch-side.png"
    }

    Image {
        id: imageRight
        y: height + 10
        anchors.right: parent.right
        anchors.rightMargin: -width + 10
        source: "/image/stopwatch-side.png"
        transform: Rotation { angle: 180 }
    }

    Rectangle {
        y: 10
        anchors.left: imageLeft.right
        anchors.right: imageRight.left
        height: 1
        color: "white"

        Rectangle {
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: 5
            height: 7
            color: "white"
        }
    }

    Rectangle {
        anchors.left: imageLeft.right
        anchors.right: imageRight.left
        anchors.bottom: imageLeft.bottom
        height: 1
        color: "white"

        Rectangle {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: 5
            height: 7
            color: "white"
        }
    }

    // Pause icon
    Rectangle {
        id: pauseIcon
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: imageLeft.right
        anchors.leftMargin: 12
        width: 8
        height: 40
        visible: false
        color: "white"

        Rectangle {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.right
            anchors.leftMargin: 8
            width: 8
            height: 40
            color: "white"
        }
    }

    // Play icon
    Text {
        id: playIcon
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: imageLeft.right
        anchors.leftMargin: -12
        text: String.fromCodePoint(0xe037)
        color: "white"
        font { family: C.CustomFont.material; pixelSize: 80 }
    }

    // Stop icon
    Rectangle {
        id: stopIcon
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: imageLeft.right
        anchors.leftMargin: 12
        visible: false
        width: 40
        height: 40
        color: "white"
    }

    Text {
        id: label
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 70
        font: Qt.font({family: C.CustomFont.mordakMono1, pixelSize: 35})
        color: "white"
        opacity: 0.8
        text: String(stopWatch.minutes).padStart(3, "0") + ":" + String(stopWatch.seconds).padStart(2, "0")
    }
}
