import QtQuick
import Components as C

C.ComboSlider {
    id: root

    property int functionnalValue: Ticket.None
    property int dysfunctionnalValue: Ticket.None

    function update() {
        switch(root.functionnalValue) {
            case Ticket.Disliked:
                if(root.dysfunctionnalValue === Ticket.Disliked)
                    root.value = Ticket.Reversed
                else
                    root.value = Ticket.Questionable
                break
            case Ticket.Accepted:
            case Ticket.Neutral:
            case Ticket.Expected:
                if(root.dysfunctionnalValue === Ticket.Disliked)
                    root.value = Ticket.MustHave
                else if(root.dysfunctionnalValue === Ticket.Liked)
                    root.value = Ticket.Questionable
                else
                    root.value = Ticket.Indifferent
                break
            case Ticket.Liked:
                if(root.dysfunctionnalValue === Ticket.Disliked)
                    root.value = Ticket.Performance
                else if(root.dysfunctionnalValue === Ticket.Liked)
                    root.value = Ticket.Reversed
                else
                    root.value = Ticket.Attractive
                break
        }
    }

    onFunctionnalValueChanged: update()
    onDysfunctionnalValueChanged: update()

    readOnly: true
    font { family: C.CustomFont.proggy; pixelSize: 18; letterSpacing: -1.5 }
    startValue: 1
    labelWidth: 130
    valueList: [
        qsTr("Questionnable"),
        qsTr("Reversed"),
        qsTr("Indiferent"),
        qsTr("Attractive"),
        qsTr("Performance"),
        qsTr("Must-have")
    ]

    Component.onCompleted: update()
}
