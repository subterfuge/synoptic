pragma ComponentBehavior: Bound

import QtQuick
import QtQuick.Controls.impl
import QtQuick.Templates as T
import Components as C

T.ComboBox {
    id: control

    width: 140
    height: 40

    leftPadding: padding + (!control.mirrored || !indicator || !indicator.visible ? 0 : indicator.width + spacing)
    rightPadding: padding + (control.mirrored || !indicator || !indicator.visible ? 0 : indicator.width + spacing)

    font: C.CustomFont.smallTextFont

    delegate: ItemDelegate {
        required property var model
        required property int index

        width: ListView.view.width
        text: model[control.textRole]
        palette.text: C.Palette.front
        palette.highlightedText: C.Palette.front
        highlighted: control.highlightedIndex === index
        hoverEnabled: control.hoverEnabled
        font: control.font
    }

    indicator: Text {
        x: control.mirrored ? control.padding : control.width - width - control.padding
        y: control.topPadding + (control.availableHeight - height) / 2
        color: C.Palette.front
        text: String.fromCodePoint(0xe5c5)
        font { family: C.CustomFont.material; pixelSize: 20 }
        opacity: enabled ? 1 : 0.3
    }

    contentItem: T.TextField {
        id: textField
        leftPadding: !control.mirrored ? 12 : control.editable && activeFocus ? 3 : 1
        rightPadding: control.mirrored ? 12 : control.editable && activeFocus ? 3 : 1
        topPadding: 6 - control.padding
        bottomPadding: 6 - control.padding

        text: control.editable ? control.editText : control.displayText

        enabled: control.editable
        autoScroll: control.editable
        readOnly: control.down
        inputMethodHints: control.inputMethodHints
        validator: control.validator
        selectByMouse: control.selectTextByMouse

        color: C.Palette.front
        selectionColor: C.Palette.smokeGreenLight
        selectedTextColor: C.Palette.greenLight
        verticalAlignment: Text.AlignVCenter

        background: Rectangle {
            visible: control.enabled && control.editable && !control.flat
            color: "transparent"
        }
    }

    background: Rectangle {
        width: control.width
        height: control.height

        color: C.Palette.background
        border.color: control.down || (control.editable && control.activeFocus) ? C.Palette.greenLight : C.Palette.front
        border.width: 1
        visible: !control.flat || control.down
    }

    popup: T.Popup {
        id: popup
        x: 5
        y: control.height + 5
        width: control.width
        height: Math.min(contentItem.implicitHeight, control.Window.height - topMargin - bottomMargin)
        topMargin: 6
        bottomMargin: 6

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: control.delegateModel
            currentIndex: control.highlightedIndex
            highlightMoveDuration: 0
            boundsBehavior: Flickable.StopAtBounds

            Rectangle {
                z: 10
                width: parent.width
                height: parent.height
                color: "transparent"
                border.color: C.Palette.front
            }

            T.ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {
            color: C.Palette.background
        }
    }
}
