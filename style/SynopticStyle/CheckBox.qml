import QtQuick
import QtQuick.Templates as T
import QtQuick.Controls.impl
import Components as C

T.CheckBox {
    id: control

    implicitWidth: 20
    implicitHeight: 20

    padding: 6
    spacing: 6

    indicator: Rectangle {
        implicitWidth: control.width
        implicitHeight: control.height

        x: control.text ? (control.mirrored ? control.width - width - control.rightPadding : control.leftPadding) : control.leftPadding + (control.availableWidth - width) / 2
        y: control.topPadding + (control.availableHeight - height) / 2

        color: control.visualFocus ? C.Palette.grey : C.Palette.background
        border.color: C.Palette.front

        Text {
            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            font { family: C.CustomFont.material; pixelSize: parent.height }
            text: String.fromCodePoint(0xe5ca) // material icon "Check"
            color: C.Palette.front
            visible: control.checkState === Qt.Checked
        }

        Rectangle {
            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            width: 16
            height: 3
            color: C.Palette.greenLight
            visible: control.checkState === Qt.PartiallyChecked
        }

        MouseArea {
            anchors.fill: parent
            enabled: false
            cursorShape: Qt.PointingHandCursor
        }
    }

    contentItem: CheckLabel {
        leftPadding: control.indicator && !control.mirrored ? control.indicator.width + control.spacing : 0
        rightPadding: control.indicator && control.mirrored ? control.indicator.width + control.spacing : 0

        text: control.text
        font: control.font
        color: control.palette.windowText
    }
}
