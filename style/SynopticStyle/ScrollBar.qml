// Copyright (C) 2017 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

import QtQuick
import QtQuick.Controls.impl
import QtQuick.Templates as T
import Components as C

T.ScrollBar {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
        implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
        implicitContentHeight + topPadding + bottomPadding)

    //padding: 2
    visible: control.policy !== T.ScrollBar.AlwaysOff
    minimumSize: orientation === Qt.Horizontal ? height / width : width / height

    contentItem: Rectangle {
        implicitWidth: control.interactive ? 9 : 2
        implicitHeight: control.interactive ? 9 : 2

        color: control.pressed ? C.Palette.white : C.Palette.greyLight
        opacity: 0.0

        states: State {
            name: "active"
            when: control.policy === T.ScrollBar.AlwaysOn || control.size < 1.0
            PropertyChanges { control.contentItem.opacity: 1.0 }
        }
    }

    Rectangle {
        x: control.orientation === Qt.Horizontal ? 0 : control.width / 2
        y: control.orientation === Qt.Horizontal ? control.height / 2 : 0
        z: -1
        width: control.orientation === Qt.Horizontal ? control.width : 1
        height: control.orientation === Qt.Horizontal ? 1 : control.height
        color: C.Palette.greyLight
        visible: control.policy === T.ScrollBar.AlwaysOn || control.size < 1.0
    }
}
