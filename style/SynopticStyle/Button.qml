import QtQuick 2.15
import QtQuick.Templates as T
import Components as C

T.Button {
    id: control

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding)

    padding: 6
    horizontalPadding: padding + 2
    spacing: 6

    icon.width: 24
    icon.height: 24
    icon.color: control.enabled ? C.Palette.front : C.Palette.greyLight
    text: "Button"
    font: C.CustomFont.defaultTextFont

    contentItem: IconLabel {
        id: label
        spacing: control.spacing
        mirrored: control.mirrored
        display: control.display

        icon: control.icon
        text: control.text
        font: control.font
        color: control.enabled ? C.Palette.front : C.Palette.greyLight
    }

    background: Rectangle {
        id: background
        implicitWidth: 100
        implicitHeight: 40
        visible: !control.flat || control.down || control.checked || control.highlighted
        color: control.checked || control.highlighted ? C.Palette.front : C.Palette.background
        border.color: control.enabled ? C.Palette.front : C.Palette.greyLight
        border.width: 1

        states: [
            State {
                name: ""
                PropertyChanges {
                    target: backgroundRectangleEffect
                    width: 0
                    height: 0
                }
                PropertyChanges {
                    label.color: control.enabled ? C.Palette.front : C.Palette.greyLight
                    label.icon.color: control.enabled ? C.Palette.front : C.Palette.greyLight
                }
            },

            State {
                name: "hovered"
                PropertyChanges {
                    target: backgroundRectangleEffect
                    width: background.width - 4
                    height: background.height - 4
                }
                PropertyChanges {
                    label.color: C.Palette.background
                    label.icon.color: C.Palette.background
                }
            }
        ]

        Rectangle {
            id: backgroundRectangleEffect
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            color: background.border.color
            width: 0
            height: 0
            Behavior on width {
                NumberAnimation { duration: 50 }
            }
            Behavior on height {
                NumberAnimation { duration: 50 }
            }
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: control.enabled
            cursorShape: Qt.PointingHandCursor
            onEntered: background.state = "hovered"
            onExited: background.state = ""
        }
    }
}
