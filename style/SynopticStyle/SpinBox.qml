// Copyright (C) 2017 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

import QtQuick
import QtQuick.Templates as T
import QtQuick.Controls.impl
import Components as C

T.SpinBox {
    id: control

    // Note: the width of the indicators are calculated into the padding
    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
        contentItem.implicitWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
        implicitContentHeight + topPadding + bottomPadding,
        up.implicitIndicatorHeight, down.implicitIndicatorHeight)

    spacing: 6
    leftPadding: control.mirrored ? (up.indicator ? up.indicator.width : 0) : (down.indicator ? down.indicator.width : 0)
    rightPadding: control.mirrored ? (down.indicator ? down.indicator.width : 0) : (up.indicator ? up.indicator.width : 0)

    validator: IntValidator {
        locale: control.locale.name
        bottom: Math.min(control.from, control.to)
        top: Math.max(control.from, control.to)
    }

    contentItem: TextInput {
        text: control.displayText
        padding: 6
        width: 12

        font: control.font
        color: enabled ? C.Palette.front : C.Palette.greyLight
        selectionColor: Qt.alpha(C.Palette.greenLight, 0.30)
        selectedTextColor: C.Palette.front
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

        readOnly: !control.editable
        validator: control.validator
        inputMethodHints: control.inputMethodHints
        clip: width < implicitWidth

        Rectangle {
            width: parent.width
            height: parent.height
            color: "transparent"
            border.color: control.activeFocus ? C.Palette.greenLight : C.Palette.black
            border.width: 1
        }
    }

    up.indicator: Rectangle {
        x: control.mirrored ? 0 : control.width - width
        //implicitWidth: control.Material.touchTarget
        //implicitHeight: control.Material.touchTarget
        height: control.height
        width: height
        border.color: C.Palette.front
        border.width: 1
        color: control.up.hovered ? C.Palette.front : "transparent"

        Rectangle {
            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            width: Math.min(parent.width / 3, parent.height / 3)
            height: 2
            color: {
                if(enabled)
                    control.up.hovered ? C.Palette.background : C.Palette.front
                else
                    C.Palette.greyLight
            }
        }
        Rectangle {
            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            width: 2
            height: Math.min(parent.width / 3, parent.height / 3)
            color: {
                if(enabled)
                    control.up.hovered ? C.Palette.background : C.Palette.front
                else
                    C.Palette.greyLight
            }
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            enabled: false
        }
    }

    down.indicator: Rectangle {
        x: control.mirrored ? control.width - width : 0
        /*implicitWidth: control.Material.touchTarget
        implicitHeight: control.Material.touchTarget*/
        height: control.height
        width: height
        border.color: C.Palette.front
        border.width: 1
        color: control.down.hovered ? C.Palette.front : "transparent"

        Rectangle {
            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            width: parent.width / 3
            height: 2
            color: {
                if(enabled)
                    control.down.hovered ? C.Palette.background : C.Palette.front
                else
                    C.Palette.greyLight
            }
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            enabled: false
        }
    }

    background: Rectangle {
        implicitWidth: 140
        //implicitHeight: control.Material.textFieldHeight
        color: "transparent"
        //border.color: C.Palette.black
    }
}