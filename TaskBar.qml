import QtQuick
import QtQuick.Controls
import QtQuick.Shapes
import Components as C

Rectangle {
    property string text
    property int fontSize : 11
    property Window target

    id: taskBar
    height: 22
    width: Window.width
    color: "transparent"

    Label {
        id: title
        x: 2
        text: taskBar.text
        font.pointSize: taskBar.fontSize
        verticalAlignment: Text.AlignVCenter
        color: C.Palette.front
    }

    Row {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 4
        anchors.topMargin: 4
        spacing: 4

        MouseArea {
            id: reduce
            width: taskBar.height - 8
            height: taskBar.height - 8
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            onClicked: {
                target.visibility = Window.Minimized
            }
            onEntered: {
                reduceBg.color = C.Palette.redLight
            }
            onExited: {
                reduceBg.color = C.Palette.red
            }

            Rectangle {
                id: reduceBg
                anchors.fill: parent
                color: C.Palette.red
            }

            Shape {
                ShapePath {
                    startX: 3; startY: reduce.height - 3
                    strokeColor: C.Palette.background
                    strokeWidth: 1

                    PathLine { x: 12; y: reduce.height - 3 }
                }
            }
        }

        MouseArea {
            id: maximize
            width: taskBar.height - 8
            height: taskBar.height - 8
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            onClicked: {
                if(target.visibility === Window.Windowed)
                    target.visibility = Window.Maximized
                else
                    target.visibility = Window.Windowed
            }
            onEntered: {
                maximizeBg.color = C.Palette.redLight
            }
            onExited: {
                maximizeBg.color = C.Palette.red
            }

            Rectangle {
                id: maximizeBg
                anchors.fill: parent
                color: C.Palette.red
            }

            Rectangle {
                x: 3
                y: parent.height - 13
                width: 10
                height: 10
                color: "transparent"
                border.color: C.Palette.background

            }
        }

        MouseArea {
            id: close
            width: taskBar.height - 8
            height: taskBar.height - 8
            cursorShape: Qt.PointingHandCursor
            hoverEnabled: true
            onClicked: {
                target.close()
            }
            onEntered: {
                closeBg.color = C.Palette.redLight
            }
            onExited: {
                closeBg.color = C.Palette.red
            }

            Rectangle {
                id: closeBg
                anchors.fill: parent
                color: C.Palette.red
            }

            Shape {
                ShapePath {
                    startX: 3; startY: close.height - 12
                    strokeColor: C.Palette.background
                    strokeWidth: 1

                    PathLine { x: close.height - 10; y: close.height - 3; }
                }

                ShapePath {
                    startX: 3; startY: close.height - 3
                    strokeColor: C.Palette.background
                    strokeWidth: 1

                    PathLine { x: close.height - 10; y: close.height - 12; }
                }
            }
        }
    }
}
