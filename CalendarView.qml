import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import style.SynopticStyle
import Components as C

Item {
    id: calendar

    property date now: new Date()

    onVisibleChanged: {
        if(visible) {
            calendarEventDataSource.reset()
        }
    }

    GridLayout {
        anchors.fill: parent
        columns: 2

        DayOfWeekRow {
            id: dayOfWeekRow
            Layout.column: 1
            Layout.fillWidth: true
            Layout.preferredHeight: 50

            delegate: Text {
                text: model.longName
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: C.Palette.front
                font: C.CustomFont.defaultTextFont
            }
        }

        WeekNumberColumn {
            id: weekColumn

            Layout.fillHeight: true
            Layout.preferredWidth: 50

            month: grid.month
            year: grid.year
            locale: grid.locale
            delegate: Text {
                text: model.weekNumber
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                color: C.Palette.front
                font: C.CustomFont.defaultTextFont
            }
        }

        MonthGrid {
            id: grid

            Layout.fillWidth: true
            Layout.fillHeight: true

            month: calendar.now.getMonth()
            year: calendar.now.getFullYear()
            locale: Qt.locale("fr_FR")

            delegate: calendarDelegate
        }
    }

    Component {
        id: calendarDelegate
        Item {
            opacity: model.month === grid.month ? 1.0 : 0.5

            Rectangle {
                anchors.margins: -1
                anchors.fill: parent

                color: C.Palette.background
                border.color: C.Palette.front

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                }

                ListView {
                    id: dayEventList
                    anchors.fill: parent
                    anchors.margins: 5
                    anchors.rightMargin: 30
                    boundsBehavior: Flickable.StopAtBounds // TODO: change for mobile
                    clip: true

                    model: CalendarEventModel {
                        dataSource: calendarEventDataSource
                        date: model.date
                    }
                    delegate: Rectangle {
                        height: 30
                        width: dayEventList.width
                        color: "transparent"
                        border.color: model.type === CalendarEvent.TicketDueType ? C.Palette.yellow : C.Palette.greenLight

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            x: 5
                            verticalAlignment: Text.AlignVCenter
                            text: model.name
                            font: C.CustomFont.smallTextFont
                            color: parent.border.color
                        }
                    }
                }
            }

            Rectangle {
                id: dayRect
                width: 25
                height: 25
                anchors.right: parent.right
                color: C.Palette.front

                Text {
                    id: dayText
                    anchors.centerIn: parent
                    text: model.day
                    color: C.Palette.background
                    font: C.CustomFont.smallTextFont
                }
            }
        }
    }
}
