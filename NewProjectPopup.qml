import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Shapes
import Synoptic
import style.SynopticStyle
import Components as C

Popup {
    id: newProjectPopup

    anchors.centerIn: parent
    width: 500
    height: 540
    padding: 0
    closePolicy: Popup.NoAutoClose

    Rectangle {
        anchors.fill: parent
        color: C.Palette.smokeGreen

        C.CrossButton {
            id: closeRect
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 5
            anchors.topMargin: 5
            onClicked: newProjectPopup.close()
        }

        Text {
            anchors.top: parent.top
            anchors.topMargin: -30
            text: qsTr("New Project")
            color: C.Palette.front
            font { family: C.CustomFont.mordak; pixelSize: 26 }
        }

        ColumnLayout {
            x: 10
            y: 10
            width: parent.width - 20
            height: parent.height - 20

            Item {
                Layout.preferredHeight: idTextField.height

                Row {
                    spacing: 5

                    TextField {
                        id: idTextField
                        width: 60
                        height: 40
                        placeholderText: qsTr("Id")
                        validator: RegularExpressionValidator { regularExpression: /^[a-zA-Z0-9][a-zA-Z0-9_]{0,3}$/ }
                        onTextEdited: {
                            text = text.toUpperCase()
                            color = C.Palette.front
                        }
                    }

                    TextField {
                        id: titleTextField
                        width: 350
                        height: 40
                        placeholderText: qsTr("Title")
                        onTextEdited: {
                            color = C.Palette.front
                        }
                    }
                }

                C.Alert {
                    id: idTextFieldAlert
                    anchors.top: parent.bottom
                    anchors.topMargin: 5
                    visible: false
                    text: ""
                    type: C.Alert.Danger
                }
            }

            Text {
                Layout.topMargin: 27
                text: qsTr("Description")
                font: C.CustomFont.defaultTextFont
                color: C.Palette.front
            }

            TextArea {
                id: descriptionTextArea
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.verticalStretchFactor: 1
            }

            GroupBox {
                Layout.topMargin: 15
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.verticalStretchFactor: 2
                title: qsTr("Default definition of done")
                font: C.CustomFont.defaultTextFont

                ListView {
                    id: dodList
                    anchors.top: parent.top
                    anchors.bottom: addDodBtn.top
                    anchors.bottomMargin: 2
                    width: parent.width
                    spacing: 2
                    boundsBehavior: Flickable.StopAtBounds
                    clip: true
                    ScrollBar.vertical: ScrollBar {}
                    model: dodModel
                    delegate: Row {
                        width: dodList.width - 10
                        height: dodItemField.height
                        spacing: 2

                        Button {
                            id: removeDodItemBtn
                            height: parent.height
                            width: height
                            text: ""
                            hoverEnabled: true
                            onClicked: dodModel.remove(model.index)
                            onHoveredChanged: {
                                if(hovered)
                                    crossShape.color = C.Palette.background
                                else
                                    crossShape.color = C.Palette.front
                            }

                            Shape {
                                id: crossShape
                                property color color: C.Palette.front
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.horizontalCenter: parent.horizontalCenter
                                ShapePath {
                                    startX: 0; startY: 0
                                    strokeColor: crossShape.color
                                    strokeWidth: 1

                                    PathLine { x: 15; y: 15; }
                                }

                                ShapePath {
                                    startX: 15; startY: 0
                                    strokeColor: crossShape.color
                                    strokeWidth: 1

                                    PathLine { x: 0; y: 15; }
                                }
                            }
                        }

                        TextField {
                            id: dodItemField
                            text: model.description
                            width: parent.width - removeDodItemBtn.width - 20
                            onTextChanged: model.description = text
                        }
                    }
                }

                Button {
                    id: addDodBtn
                    anchors.bottom: parent.bottom
                    text: qsTr("Add item")
                    icon.name: "add"
                    onClicked: dodModel.append({"description": ""})
                }

                ListModel {
                    id: dodModel
                    ListElement {
                        description: ""
                    }
                }
            }

            C.Button {
                id: submitBtn
                Layout.topMargin: 15
                Layout.alignment: Qt.AlignHCenter
                text: qsTr("Submit")
                onClicked: {
                    let projectId = idTextField.text
                    if (projectId === undefined || projectId.trim() === "")
                    {
                        idTitleAlert(qsTr("Id shouldn't be empty"))
                        idTextField.color = C.Palette.red
                        return
                    }
                    if(titleTextField.text.trim() === "")
                    {
                        idTitleAlert(qsTr("Title shouldn't be empty"))
                        titleTextField.color = C.Palette.red
                        return
                    }
                    let result = projectDataSource.insert(projectId, titleTextField.text,
                            descriptionTextArea.text, Settings.getWorkspace())
                    if(result === ProjectDataSource.IdAlreadyExists)
                    {
                        idTitleAlert(qsTr("Id already exists"))
                        idTextField.color = C.Palette.red
                    }
                    else
                    {
                        for(let i = 0; i < dodModel.count; i++) {
                            let dodItemDescription = dodModel.get(i).description
                            if(dodItemDescription.trim === undefined || dodItemDescription.trim() === "")
                                continue

                            DefinitionOfDoneDataSource.insert(dodItemDescription, projectId)
                        }
                        idTextFieldAlert.visible = false
                        newProjectPopup.close()
                    }
                }
            }
        }
    }

    function idTitleAlert(message) {
        idTextFieldAlert.text = message
        idTextFieldAlert.type = C.Alert.Danger
        idTextFieldAlert.visible = true
    }

    function reset() {
        idTextField.text = ""
        titleTextField.text = ""
        idTextFieldAlert.visible = false
        descriptionTextArea.text = ""
        submitBtn.error = false
    }

    onClosed: {
        reset()
    }
}
