import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Shapes
import Synoptic
import style.SynopticStyle
import Components as C

Rectangle {
    id: ticketView
    property Ticket ticket : Ticket {}
    property bool useKanoModel: true

    enum Error {
        NoError = 0,
        NoTitleError = 1,
        DueDateWrongFormatError = 2,
        DueTimeWrongFormatError = 4
    }

    QtObject {
        id: internal
        property bool isNew: true

        function checkFields() {
            let result = TicketView.NoError
            if(title.text === "")
                result |= TicketView.NoTitleError
            if(!dueDateField.inputToDate().result)
                result |= TicketView.DueDateWrongFormatError
            if(!dueTimeField.checkInput().result)
                result |= TicketView.DueTimeWrongFormatError
            return result;
        }

        function updateAlert(error) {
            if((error & TicketView.NoTitleError) === TicketView.NoTitleError) {
                titleAlert.text = qsTr("Title shouldn't be empty");
                titleAlert.visible = true
            }
            else
                titleAlert.visible = false
            if((error & TicketView.DueDateWrongFormatError) === TicketView.DueDateWrongFormatError) {
                dueDateAlert.visible = true
                if((error & TicketView.DueTimeWrongFormatError) === TicketView.DueTimeWrongFormatError)
                    dueDateAlert.text = qsTr("Wrong date and time format")
                else
                    dueDateAlert.text = qsTr("Wrong date format")
            }
            else if((error & TicketView.DueTimeWrongFormatError) === TicketView.DueTimeWrongFormatError) {
                dueDateAlert.visible = true
                dueDateAlert.text = qsTr("Wrong time format")
            }
            else
                dueDateAlert.visible = false
        }

        function tryInsert() {
            let err = checkFields()
            updateAlert(err)
            if(err === TicketView.NoError) {
                insert();
            }
        }

        function tryUpdate() {
            let err = checkFields()
            updateAlert(err)
            if(err === TicketView.NoError)
                TicketDataSource.update(ticketView.ticket)
        }

        function insert(): int {
            let result = TicketDataSource.insert(ticketView.ticket.project.dbId, ticketView.ticket.title,
                ticketView.ticket.position, ticketView.ticket.description, ticketView.ticket.presentKanoFeel,
                ticketView.ticket.absentKanoFeel, ticketView.ticket.storyPoints, ticketView.ticket.dueDate,
                ticketView.ticket.priority)
            if(result !== -1) {
                ticketView.ticket.id = result
                ticketModel.setData(ticketModel.index(ticketModel.rowCount() - 1, 0), true, TicketModel.SelectedRole) // A bit clunky for now
                isNew = false
            }
            return result
        }

        function resetDodToDefault() {
            dodModel.clear()
            let items = DefinitionOfDoneDataSource.get(ticketView.ticket.project.dbId)
            for(let i = 0; i < items.length; i++) {
                dodModel.append({"description": items[i].description})
            }
        }

        function pullDod() {
            dodModel.clear()
            let items = DefinitionOfDoneDataSource.get(ticketView.ticket.project.dbId, ticketView.ticket.id)
            for(let i = 0; i < items.length; i++) {
                dodModel.append({"description": items[i].description})
            }
        }

        function updateDod() {
            DefinitionOfDoneDataSource.clear(ticketView.ticket.id, ticketView.ticket.project.dbId)
            for(let i = 0; i < dodModel.count; i++) {
                if(dodModel.get(i).description === undefined || dodModel.get(i).description === "")
                    continue

                DefinitionOfDoneDataSource.insert(dodModel.get(i).description, ticketView.ticket.id,
                    ticketView.ticket.project.dbId)
            }
        }
    }

    Component { // Used to create new instances of Ticket object
        id: newTicket
        Ticket { presentKanoFeel: Ticket.Disliked; absentKanoFeel: Ticket.Disliked }
    }

    function setTicket(id, projectId) {
        internal.isNew = false
        ticket = TicketDataSource.get(id, projectId)
        title.text = ticket.title
        dueDateField.selectedDate = ticket.dueDate
        dueTimeField.setTime(ticket.dueDate)
        descriptionField.text = ticket.description
        storyPoints.value = ticket.storyPoints
        prioritySlider.value = ticket.priority
        functionnalSlider.value = ticket.presentKanoFeel
        disfunctionnalSlider.value = ticket.absentKanoFeel
        timeEntryModel.ticketId = id
        timeEntryModel.ticketProjectId = projectId
        timeEntryModel.update()
        internal.pullDod();
    }

    function setTicketNew(project, position) {
        internal.isNew = true
        ticket = newTicket.createObject()
        ticket.project = project
        ticket.position = position
        title.text = ticket.title
        dueDateField.selectedDate = undefined
        dueTimeField.text = ""
        descriptionField.text = ticket.description
        storyPoints.value = ticket.storyPoints
        prioritySlider.value = Ticket.Medium
        functionnalSlider.value = ticket.presentKanoFeel
        disfunctionnalSlider.value = ticket.absentKanoFeel
        timeEntryModel.ticketId = 0
        timeEntryModel.ticketProjectId = project.dbId
        timeEntryModel.update()
        internal.resetDodToDefault()
    }

    color: C.Palette.smokeGreen

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            ///////////////////////////////////////////////////////////////////////
            // LEFT PANEL
            ///////////////////////////////////////////////////////////////////////
            ColumnLayout {
                id: leftPanel
                property int contentItemsWidth: width - 30
                Layout.margins: 5
                Layout.fillHeight: true
                Layout.fillWidth: true
                spacing: 5

                Text {
                    id: titleLabel
                    font: C.CustomFont.subTitleFont
                    text: qsTr("Title")
                    color: C.Palette.front
                }

                TextField {
                    id: title
                    Layout.preferredHeight: 36
                    Layout.fillWidth: true
                    text: ticketView.ticket.title
                    font: C.CustomFont.defaultTextFont
                    onTextChanged: ticketView.ticket.title = text

                    C.Alert {
                        id: titleAlert
                        anchors.top: parent.bottom
                        anchors.topMargin: 5
                        visible: false
                        type: C.Alert.Danger
                        text: qsTr("Title shouldn't be empty")
                    }
                }

                Text {
                    id: descriptionLabel
                    Layout.topMargin: 25
                    font: C.CustomFont.subTitleFont
                    text: qsTr("Description")
                    color: C.Palette.front

                    Text {
                        id: descriptionDecoration

                        function updateText() {
                            text = "002 - DES\t" + descriptionField.text.length.toString().padStart(3, '0') + "_"
                                + descriptionField.text.trim().split(/\s+/).length + " : "
                                + descriptionField.cursorPosition + "°";
                        }

                        y: -10
                        font { family: C.CustomFont.mordak; pixelSize: 8 }
                        color: C.Palette.smokeGreenLight
                    }

                    Rectangle {
                        id: line
                        height: 2
                        width: 150
                        color: C.Palette.smokeGreenLight
                    }
                }

                Flickable {
                    id: description
                    Layout.fillWidth: true
                    Layout.preferredHeight: 110
                    flickableDirection: Flickable.VerticalFlick
                    boundsBehavior: Flickable.StopAtBounds

                    ScrollBar.vertical: ScrollBar {}

                    TextArea.flickable: TextArea {
                        id: descriptionField
                        text: ticketView.ticket.description
                        font: C.CustomFont.defaultTextFont
                        wrapMode: TextEdit.Wrap
                        background: Rectangle {
                            border.width: 1
                            border.color: descriptionField.focus ? C.Palette.greenLight : "transparent"
                            color: C.Palette.background
                        }

                        Component.onCompleted: descriptionDecoration.updateText()
                        onCursorPositionChanged: descriptionDecoration.updateText()
                        onTextChanged: {
                            ticketView.ticket.description = text
                            descriptionDecoration.updateText()
                        }
                    }
                }

                GroupBox {
                    id: dodGb
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.topMargin: 25
                    title: qsTr("Definition of done")

                    ListView {
                        id: dodList
                        anchors.top: parent.top
                        anchors.bottom: dodBtns.top
                        anchors.bottomMargin: 5
                        width: parent.width
                        clip: true
                        spacing: 2
                        boundsBehavior: Flickable.StopAtBounds
                        ScrollBar.vertical: ScrollBar {}
                        model: ListModel {
                            id: dodModel
                        }

                        delegate: Row {
                            width: dodList.width - 10
                            height: dodItemField.height
                            spacing: 2

                            Button {
                                id: removeDodItemBtn
                                height: parent.height
                                width: height
                                text: ""
                                hoverEnabled: true
                                onClicked: dodModel.remove(model.index)
                                onHoveredChanged: {
                                    if(hovered)
                                        crossShape.color = C.Palette.background
                                    else
                                        crossShape.color = C.Palette.front
                                }

                                Shape {
                                    id: crossShape
                                    property color color: C.Palette.front
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    ShapePath {
                                        startX: 0; startY: 0
                                        strokeColor: crossShape.color
                                        strokeWidth: 1

                                        PathLine { x: 15; y: 15; }
                                    }

                                    ShapePath {
                                        startX: 15; startY: 0
                                        strokeColor: crossShape.color
                                        strokeWidth: 1

                                        PathLine { x: 0; y: 15; }
                                    }
                                }
                            }

                            TextField {
                                id: dodItemField
                                text: model.description
                                width: parent.width - removeDodItemBtn.width - 20
                                onTextEdited: model.description = text
                            }
                        }
                    }

                    Row {
                        id: dodBtns
                        anchors.bottom: parent.bottom
                        spacing: 2
                        Button {
                            text: qsTr("Add item")
                            icon.name: "add"
                            font: Qt.font({ family: C.Palette.proggy, pixelSize: 14 })
                            onClicked: dodModel.append({ "description": "" })
                        }

                        Button {
                            text: qsTr("Reset to project default")
                            icon.name: "undo"
                            font: Qt.font({ family: C.Palette.proggy, pixelSize: 14 })
                            onClicked: internal.resetDodToDefault()
                        }

                        Button {
                            text: qsTr("Clear")
                            icon.name: "delete"
                            font: Qt.font({ family: C.Palette.proggy, pixelSize: 14 })
                            onClicked: dodModel.clear()
                        }
                    }
                }
            }

            ///////////////////////////////////////////////////////////////////////
            // RIGHT PANEL
            ///////////////////////////////////////////////////////////////////////
            ColumnLayout {
                id: rightPanel
                Layout.margins: 5
                Layout.preferredWidth: 280
                Layout.alignment: Qt.AlignTop
                spacing: 5

                Text {
                    id: storyPointsLabel
                    font: C.CustomFont.subTitleFont
                    text: "Story points"
                    color: C.Palette.front
                }

                SpinBox {
                    id: storyPoints
                    editable: true
                    onValueChanged: ticketView.ticket.storyPoints = value
                    font: C.CustomFont.defaultTextFont
                }

                Text {
                    Layout.topMargin: 20
                    font: C.CustomFont.subTitleFont
                    text: qsTr("Due date")
                    color: C.Palette.front
                }

                Row {
                    spacing: 10
                    C.DatePicker {
                        id: dueDateField

                        C.Alert {
                            id: dueDateAlert
                            anchors.top: parent.bottom
                            anchors.topMargin: 5
                            visible: false
                            type: C.Alert.Danger
                        }
                    }

                    C.TimePicker {
                        id: dueTimeField
                    }
                }

                GroupBox {
                    title: qsTr("Priotity")
                    Layout.topMargin: 25

                    RowLayout {
                        C.ComboSlider {
                            id: prioritySlider
                            font { family: C.CustomFont.proggy; pixelSize: 18; letterSpacing: -1.5 }
                            startValue: 0
                            value: ticketView.ticket.priority
                            minColor: Qt.hsva(0.6, 1, 1)
                            maxColor: Qt.hsva(0, 1, 1)
                            valueList: [
                                qsTr("Low"),
                                qsTr("Medium"),
                                qsTr("High"),
                                qsTr("Urgent")
                            ]
                            onValueChanged: {
                                ticketView.ticket.priority = prioritySlider.value
                            }
                        }
                    }
                }

                GroupBox {
                    id: kanoFeelBox

                    function updateKanoCategory() {
                        let dv = disfunctionnalSlider.value
                        switch(functionnalSlider.value) {
                            case Ticket.Disliked:
                                if(dv === Ticket.Disliked)
                                    kanoCategorySlider.value = Ticket.Reversed
                                else
                                    kanoCategorySlider.value = Ticket.Questionable
                                break
                            case Ticket.Accepted:
                            case Ticket.Neutral:
                            case Ticket.Expected:
                                if(dv === Ticket.Disliked)
                                    kanoCategorySlider.value = Ticket.MustHave
                                else if(dv === Ticket.Liked)
                                    kanoCategorySlider.value = Ticket.Questionable
                                else
                                    kanoCategorySlider.value = Ticket.Indifferent
                                break
                            case Ticket.Liked:
                                if(dv === Ticket.Disliked)
                                    kanoCategorySlider.value = Ticket.Performance
                                else if(dv === Ticket.Liked)
                                    kanoCategorySlider.value = Ticket.Reversed
                                else
                                    kanoCategorySlider.value = Ticket.Attractive
                                break
                        }
                    }

                    Layout.preferredWidth: kanoCategorySlider.implicitWidth + 10
                    Layout.preferredHeight: 195
                    Layout.topMargin: 25
                    visible: ticketView.useKanoModel
                    title: "Kano feels"

                    Text {
                        id: functionnalLabel
                        font { family: C.CustomFont.proggy; pixelSize: 14; bold: true }
                        text: qsTr("Present")
                        color: C.Palette.front
                    }

                    C.ComboSlider {
                        id: functionnalSlider
                        anchors.top: functionnalLabel.bottom
                        font { family: C.CustomFont.proggy; pixelSize: 18; letterSpacing: -1.5 }
                        startValue: 1
                        valueList: [
                            qsTr("Dislike"),
                            qsTr("Accept"),
                            qsTr("Neutral"),
                            qsTr("Expect"),
                            qsTr("Like")
                        ]
                        onValueChanged: {
                            ticketView.ticket.presentKanoFeel = functionnalSlider.value
                            kanoFeelBox.updateKanoCategory()
                        }
                    }

                    Text {
                        id: disfunctionnalLabel
                        anchors.top: functionnalSlider.bottom
                        anchors.topMargin: 10
                        font {
                            family: C.CustomFont.proggy; pixelSize: 14; bold: true
                        }
                        text: qsTr("Absent")
                        color: C.Palette.front
                    }

                    C.ComboSlider {
                        id: disfunctionnalSlider
                        anchors.top: disfunctionnalLabel.bottom
                        font { family: C.CustomFont.proggy; pixelSize: 18; letterSpacing: -1.5 }
                        startValue: 1
                        valueList: [
                            qsTr("Dislike"),
                            qsTr("Accept"),
                            qsTr("Neutral"),
                            qsTr("Expect"),
                            qsTr("Like")
                        ]
                        onValueChanged: {
                            ticketView.ticket.absentKanoFeel = disfunctionnalSlider.value
                            kanoFeelBox.updateKanoCategory()
                        }
                    }

                    Text {
                        id: kanoCategoryLabel
                        anchors.top: disfunctionnalSlider.bottom
                        anchors.topMargin: 10
                        font { family: C.CustomFont.proggy; pixelSize: 14; bold: true }
                        text: qsTr("Category")
                        color: C.Palette.front
                    }

                    C.ComboSlider {
                        id: kanoCategorySlider
                        anchors.top: kanoCategoryLabel.bottom
                        readOnly: true
                        font { family: C.CustomFont.proggy; pixelSize: 18; letterSpacing: -1.5 }
                        startValue: 1
                        labelWidth: 130
                        valueList: [
                            qsTr("Questionnable"),
                            qsTr("Reversed"),
                            qsTr("Indiferent"),
                            qsTr("Attractive"),
                            qsTr("Performance"),
                            qsTr("Must-have")
                        ]
                    }
                }

                Text {
                    Layout.topMargin: 20
                    font: C.CustomFont.subTitleFont
                    text: qsTr("Time spent")
                    color: C.Palette.front
                }

                Text {
                    font { family: C.CustomFont.mordakMono1; pixelSize: 18 }
                    color: C.Palette.front
                    text: qsTr("%1h %2m %3s").arg(Math.floor(timeEntryModel.totalTime / 60 / 60))
                        .arg(Math.floor(timeEntryModel.totalTime / 60) % 60).arg(timeEntryModel.totalTime % 60)

                    Button {
                        y: -1
                        anchors.left: parent.right
                        anchors.leftMargin: 5
                        width: 20
                        height: 20
                        enabled: !internal.isNew
                        text: String.fromCodePoint(0xe145)
                        font { family: C.CustomFont.material; pixelSize: 15 }
                        onClicked: {
                            addTimeEntryPopup.open()
                        }

                        Popup {
                            id: addTimeEntryPopup
                            width: 260
                            height: 135
                            margins: 0

                            onClosed: {
                                addTimeEntryAlert.visible = false
                                addTimeEntryDuration.text = ""
                                addTimeEntryDate.selectedDate = undefined
                            }

                            Rectangle {
                                anchors.fill: parent
                                color: C.Palette.background
                                border.color: C.Palette.front

                                Row {
                                    x: 5
                                    spacing: 100
                                    Text {
                                        font: C.CustomFont.defaultTextFont
                                        color: C.Palette.front
                                        text: qsTr("Date")
                                        C.DatePicker {
                                            id: addTimeEntryDate
                                            anchors.top: parent.bottom
                                        }
                                    }

                                    Text {
                                        font: C.CustomFont.defaultTextFont
                                        color: C.Palette.front
                                        text: qsTr("Duration")
                                        C.TimePicker {
                                            id: addTimeEntryDuration
                                            anchors.top: parent.bottom
                                            model: [ "0:10", "0:20", "0:30", "1:00", "2:00" ]
                                        }
                                    }
                                }

                                C.Alert {
                                    id: addTimeEntryAlert
                                    x: 5
                                    anchors.bottom: addTimeEntryBtn.top
                                    anchors.bottomMargin: 5
                                    visible: false
                                    type: C.Alert.Danger
                                    text: qsTr("Wrong date or time format")
                                }

                                C.Button {
                                    id: addTimeEntryBtn
                                    width: 100
                                    anchors.bottom: parent.bottom
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    anchors.bottomMargin: 5
                                    text: qsTr("Add")
                                    onClicked: {
                                        let dateResult = addTimeEntryDate.inputToDate()
                                        let timeResult = addTimeEntryDuration.checkInput()
                                        if(dateResult.result && timeResult.result) {
                                            TimeEntryDataSource.insert(ticketView.ticket.id,
                                                ticketView.ticket.project.dbId, dateResult.date,
                                                timeResult.hours * 60 * 60 + timeResult.minutes * 60)
                                            addTimeEntryPopup.close()
                                            addTimeEntryAlert.visible = false
                                        }
                                        else
                                            addTimeEntryAlert.visible = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////
        // BOTTOM
        ///////////////////////////////////////////////////////////////////////
        RowLayout {
            Layout.preferredHeight: 45
            Layout.alignment: Qt.AlignHCenter
            Layout.bottomMargin: 5

            C.Button {
                id: saveBtn

                text: qsTr("Save")
                Layout.preferredWidth: 200
                Layout.preferredHeight: 40
                normalColor: C.Palette.white
                hoveredColor: C.Palette.greenLight
                onClicked: {
                    let err = internal.checkFields()
                    internal.updateAlert(err)
                    if(err !== TicketView.NoError)
                        return

                    let date = dueDateField.inputToDate().date
                    let timeResult = dueTimeField.checkInput()
                    date.setHours(timeResult.hours)
                    date.setMinutes(timeResult.minutes)
                    ticketView.ticket.dueDate = date
                    if(internal.isNew)
                        internal.insert()
                    else
                        TicketDataSource.update(ticketView.ticket)

                    internal.updateDod()
                }
            }

            C.Button {
                id: cancelBtn

                text: qsTr("Cancel")
                Layout.preferredWidth: 200
                Layout.preferredHeight: 40
                normalColor: C.Palette.white
                hoveredColor: C.Palette.greenLight
                onClicked: {
                    ticketModel.resetSelected()
                    ticketView.visible = false
                }
            }

            Button {
                enabled: !internal.isNew
                text: String.fromCodePoint(0xe1a1)
                Layout.preferredWidth: 40
                Layout.preferredHeight: 40
                font { family: C.CustomFont.material; pixelSize: 32 }
                onClicked : {
                    // noinspection JSDeprecatedSymbols
                    TicketDataSource.archive(ticketView.ticket)
                    ticketView.visible = false
                }
            }
        }
    }

    TimeEntryModel {
        id: timeEntryModel
        dataSource: TimeEntryDataSource
    }
}
