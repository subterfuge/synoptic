<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>BoardView</name>
    <message>
        <location filename="../BoardView.qml" line="+98"/>
        <location filename="../bin/build/debug/Synoptic/BoardView.qml" line="+98"/>
        <source>Filters</source>
        <translation>Filtres</translation>
    </message>
    <message>
        <location line="+25"/>
        <location filename="../bin/build/debug/Synoptic/BoardView.qml" line="+25"/>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <location line="+53"/>
        <location filename="../bin/build/debug/Synoptic/BoardView.qml" line="+53"/>
        <source>New ticket</source>
        <translation>Nouveau ticket</translation>
    </message>
</context>
<context>
    <name>DatePicker</name>
    <message>
        <location filename="../imports/Components/DatePicker.qml" line="+26"/>
        <location filename="../bin/build/debug/Components/DatePicker.qml" line="+26"/>
        <source>dd/mm/yyyy</source>
        <translation>jj/mm/aaaa</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../Main.qml" line="+26"/>
        <location filename="../bin/build/debug/Synoptic/Main.qml" line="+26"/>
        <source>Synoptic</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>NewProjectPopup</name>
    <message>
        <location filename="../NewProjectPopup.qml" line="+31"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+31"/>
        <source>New Project</source>
        <translation>Nouveau Projet</translation>
    </message>
    <message>
        <location line="+29"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+29"/>
        <source>Id</source>
        <translation></translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+12"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location line="+17"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+17"/>
        <source>Sprint duration</source>
        <translation>Durée du sprint</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+8"/>
        <source>Sprint duration is expressed in days (count non-working days)</source>
        <translation>La durée du sprint est exprimée en jours (comptez les jours chômés)</translation>
    </message>
    <message>
        <location line="+13"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+13"/>
        <source>Description</source>
        <translation></translation>
    </message>
    <message>
        <location line="+15"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+15"/>
        <source>Submit</source>
        <translation>Valider</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+4"/>
        <source>Id shouldn&apos;t be empty</source>
        <translation>L&apos;identifiant ne peut pas être vide</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+6"/>
        <source>Title shouldn&apos;t be empty</source>
        <translation>Le titre ne peut pas être vide</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../bin/build/debug/Synoptic/NewProjectPopup.qml" line="+8"/>
        <source>Id already exists</source>
        <translation>L&apos;identifiant existe déjà</translation>
    </message>
</context>
<context>
    <name>NewWorkspacePopup</name>
    <message>
        <location filename="../NewWorkspacePopup.qml" line="+31"/>
        <location filename="../bin/build/debug/Synoptic/NewWorkspacePopup.qml" line="+31"/>
        <source>New Workspace</source>
        <translation>Nouveau Workspace</translation>
    </message>
    <message>
        <location line="+14"/>
        <location filename="../bin/build/debug/Synoptic/NewWorkspacePopup.qml" line="+14"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location line="+17"/>
        <location filename="../bin/build/debug/Synoptic/NewWorkspacePopup.qml" line="+17"/>
        <source>Submit</source>
        <translation>Valider</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../bin/build/debug/Synoptic/NewWorkspacePopup.qml" line="+4"/>
        <source>Title shouldn&apos;t be empty</source>
        <translation>Le titre ne peut pas être vide</translation>
    </message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <location filename="../ProjectView.qml" line="+15"/>
        <location filename="../bin/build/debug/Synoptic/ProjectView.qml" line="+15"/>
        <source>Project list</source>
        <translation>Liste des Projets</translation>
    </message>
    <message>
        <location line="+26"/>
        <location filename="../bin/build/debug/Synoptic/ProjectView.qml" line="+26"/>
        <source>New project</source>
        <translation>Nouveau Projet</translation>
    </message>
    <message>
        <location line="+74"/>
        <location filename="../bin/build/debug/Synoptic/ProjectView.qml" line="+74"/>
        <source>Used last: </source>
        <translation>Dernière utilisation : </translation>
    </message>
</context>
<context>
    <name>SearchComboBox</name>
    <message>
        <location filename="../imports/Components/SearchComboBox.qml" line="+29"/>
        <location filename="../bin/build/debug/Components/SearchComboBox.qml" line="+29"/>
        <source>Filter</source>
        <translation>Filtres</translation>
    </message>
</context>
<context>
    <name>SettingsPopup</name>
    <message>
        <location filename="../SettingsPopup.qml" line="+33"/>
        <location filename="../bin/build/debug/Synoptic/SettingsPopup.qml" line="+33"/>
        <source>Settings</source>
        <translation>Parametres</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../bin/build/debug/Synoptic/SettingsPopup.qml" line="+16"/>
        <source>Workspace:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <location filename="../bin/build/debug/Synoptic/SettingsPopup.qml" line="+13"/>
        <source>Create new workspace</source>
        <translation>Créer un nouveau Workspace</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../bin/build/debug/Synoptic/SettingsPopup.qml" line="+8"/>
        <source>Language:</source>
        <translation>Langue :</translation>
    </message>
    <message>
        <location line="+102"/>
        <location filename="../bin/build/debug/Synoptic/SettingsPopup.qml" line="+102"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../bin/build/debug/Synoptic/SettingsPopup.qml" line="+8"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../bin/build/debug/Synoptic/SettingsPopup.qml" line="+5"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>TicketView</name>
    <message>
        <location filename="../TicketView.qml" line="+35"/>
        <location line="+119"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+35"/>
        <location line="+119"/>
        <source>Title shouldn&apos;t be empty</source>
        <translation>Le titre ne peut pas rester vide</translation>
    </message>
    <message>
        <location line="-19"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-19"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Project</source>
        <translation type="vanished">Projet</translation>
    </message>
    <message>
        <source>Create new project</source>
        <translation type="vanished">Créer un nouveau projet</translation>
    </message>
    <message>
        <source>You must select a project</source>
        <translation type="vanished">Vous devez selectionner un projet</translation>
    </message>
    <message>
        <location line="+27"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+27"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location line="+59"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+59"/>
        <source>Sub-tickets</source>
        <translation>Sous-tickets</translation>
    </message>
    <message>
        <location line="+20"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+20"/>
        <source>Add new</source>
        <translation>Ajouter nouveau</translation>
    </message>
    <message>
        <location line="+180"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+180"/>
        <source>Time spent</source>
        <translation>Temps écoulé</translation>
    </message>
    <message>
        <location line="+7"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+7"/>
        <source>%1h %2m %3s</source>
        <translation></translation>
    </message>
    <message>
        <location line="+39"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+39"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location line="+10"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+10"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+16"/>
        <source>Wrong date or time format</source>
        <translation>Mauvais format temps ou date</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+9"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location line="+33"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+33"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location line="-195"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-195"/>
        <source>Present</source>
        <translation>Présent</translation>
    </message>
    <message>
        <location line="-297"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-297"/>
        <source>Wrong date and time format</source>
        <translation>Mauvais format temps et date</translation>
    </message>
    <message>
        <location line="+2"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+2"/>
        <source>Wrong date format</source>
        <translation>Mauvais format date</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+4"/>
        <source>Wrong time format</source>
        <translation>Mauvais format temps</translation>
    </message>
    <message>
        <location line="+224"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+224"/>
        <source>Due date</source>
        <translation>Date d&apos;échéance</translation>
    </message>
    <message>
        <location line="+77"/>
        <location line="+29"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+77"/>
        <location line="+29"/>
        <source>Dislike</source>
        <translation></translation>
    </message>
    <message>
        <location line="-28"/>
        <location line="+29"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-28"/>
        <location line="+29"/>
        <source>Accept</source>
        <translation></translation>
    </message>
    <message>
        <location line="-28"/>
        <location line="+29"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-28"/>
        <location line="+29"/>
        <source>Neutral</source>
        <translation></translation>
    </message>
    <message>
        <location line="-28"/>
        <location line="+29"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-28"/>
        <location line="+29"/>
        <source>Expect</source>
        <translation></translation>
    </message>
    <message>
        <location line="-28"/>
        <location line="+29"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-28"/>
        <location line="+29"/>
        <source>Like</source>
        <translation></translation>
    </message>
    <message>
        <location line="-14"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="-14"/>
        <source>Absent</source>
        <translation></translation>
    </message>
    <message>
        <location line="+27"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+27"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+12"/>
        <source>Questionnable</source>
        <translation>Discutable</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+1"/>
        <source>Reversed</source>
        <translation>Inversé</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+1"/>
        <source>Indiferent</source>
        <translation>Indifférent</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+1"/>
        <source>Attractive</source>
        <translation>Attractif</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+1"/>
        <source>Performance</source>
        <translation></translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+1"/>
        <source>Must-have</source>
        <translation></translation>
    </message>
    <message>
        <location line="+148"/>
        <location filename="../bin/build/debug/Synoptic/TicketView.qml" line="+148"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>TimePicker</name>
    <message>
        <location filename="../imports/Components/TimePicker.qml" line="+53"/>
        <location filename="../bin/build/debug/Components/TimePicker.qml" line="+53"/>
        <source>hh:mm</source>
        <translation></translation>
    </message>
</context>
</TS>
